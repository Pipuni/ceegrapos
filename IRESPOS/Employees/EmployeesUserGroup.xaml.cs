﻿using BusinessLogicDataHandler.Employees;
using ConstantsDataHandler;
using System;
using System.Data;
using System.Windows;
using System.Windows.Controls;

namespace IRESPOS.Employees
{
    /// <summary>
    /// Interaction logic for EmployeesUserGroup.xaml
    /// </summary>
    public partial class EmployeesUserGroup : UserControl
    {
        public EmployeesUserGroup()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            getUsernames();
            getUserGroups();
        }

        private void getUsernames()
        {
            EmployeeDataHandler employeeDataHandler = new EmployeeDataHandler();
            DataTable dtdis = new DataTable();
            combo_username.Items.Clear();

            try
            {
                string compcode = Constants.SYS_SESSION_COMPCODE;
                string loccode = Constants.SYS_SESSION_LOCCODE;
                string restcode = Constants.SYS_SESSION_RESTCODE;
                string sts = Constants.CON_STATUS_ACTIVE;

                dtdis = employeeDataHandler.getRestaurantUsers(compcode, loccode, restcode, sts);
                combo_username.ItemsSource = dtdis.DefaultView;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
            finally
            {
                dtdis.Dispose();
                dtdis = null;
                employeeDataHandler = null;
            }
        }

        private void getUserGroups()
        {
            EmployeeDataHandler employeeDataHandler = new EmployeeDataHandler();
            DataTable dtdis = new DataTable();
            combo_usergroup.Items.Clear();

            try
            {
                string compcode = Constants.SYS_SESSION_COMPCODE;
                string loccode = Constants.SYS_SESSION_LOCCODE;
                string sts = Constants.CON_STATUS_ACTIVE;

                dtdis = employeeDataHandler.getUserGroups(compcode, loccode, sts);
                combo_usergroup.ItemsSource = dtdis.DefaultView;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
            finally
            {
                dtdis.Dispose();
                dtdis = null;
                employeeDataHandler = null;
            }
        }

        private void Combo_username_Selected(object sender, RoutedEventArgs e)
        {
            if (combo_username.SelectedIndex == -1)
            {
                return;
            }

            EmployeeDataHandler employeeDataHandler = new EmployeeDataHandler();
            DataTable dtdis = new DataTable();

            try
            {
                string compcode = Constants.SYS_SESSION_COMPCODE;
                string loccode = Constants.SYS_SESSION_LOCCODE;
                string restcode = Constants.SYS_SESSION_RESTCODE;
                string username = combo_username.SelectedValue.ToString();
                string sts = Constants.CON_STATUS_ACTIVE;

                dtdis = employeeDataHandler.getUserGroupsForUser(compcode, loccode, restcode, username, sts);
                string groupcode = dtdis.Rows[0]["USERGROUP"].ToString();
                combo_usergroup.SelectedValue = groupcode;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
            finally
            {
                dtdis.Dispose();
                dtdis = null;
                employeeDataHandler = null;
            }
        }

        private void clearAll()
        {
            combo_username.SelectedIndex = -1;
            combo_usergroup.SelectedIndex = -1;
            start_date.SelectedDate = null;
        }

        private Boolean validateFields()
        {
            if (combo_username.SelectedIndex > -1 && combo_usergroup.SelectedIndex > -1 && start_date.Text.Length > 0)
            {
                return true;
            }
            return false;
        }

        private void Btn_Confirm(object sender, RoutedEventArgs e)
        {
            EmployeeDataHandler employeeDataHandler = new EmployeeDataHandler();

            try
            {
                if (!validateFields())
                {
                    MessageBox.Show("Please fill in all the fields !", Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                    return;
                }

                string compcode = Constants.SYS_SESSION_COMPCODE;
                string loccode = Constants.SYS_SESSION_LOCCODE;
                string restcode = Constants.SYS_SESSION_RESTCODE;
                string username = combo_username.SelectedValue.ToString();
                string enuser = Constants.SYS_SESSION_USERNAME;
                string startdate = Convert.ToDateTime(start_date.Text).ToString("yyyy-MM-dd");
                string usergroup = combo_usergroup.SelectedValue.ToString();
                string locdate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
                string actsts = Constants.CON_STATUS_ACTIVE;
                string delsts = Constants.CON_STATUS_DELETED;

                Boolean iscreate = employeeDataHandler.createUserMapping(compcode, loccode, restcode, username, usergroup, enuser, startdate, locdate, actsts, delsts);
                if (iscreate)
                {
                    clearAll();
                    Constants.CON_MESSAGE_STRING = "User Mapped successfully !";
                    MessageBox.Show(Constants.CON_MESSAGE_STRING, Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                }
                else
                {
                    Constants.CON_MESSAGE_STRING = "Data update failed. Please contact sytem admin. ";
                    MessageBox.Show(Constants.CON_MESSAGE_STRING, Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
            finally
            {
                employeeDataHandler = null;
            }
        }
    }
}