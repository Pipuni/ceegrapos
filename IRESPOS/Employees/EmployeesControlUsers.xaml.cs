﻿using BusinessLogicDataHandler.Employees;
using ConstantsDataHandler;
using System;
using System.Data;
using System.Windows;
using System.Windows.Controls;

namespace IRESPOS.Employees
{
    /// <summary>
    /// Interaction logic for EmployeesControlUsers.xaml
    /// </summary>
    public partial class EmployeesControlUsers : UserControl
    {
        private string compcode;
        private string loccode;

        public EmployeesControlUsers()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            compcode = Constants.SYS_SESSION_COMPCODE;
            loccode = Constants.SYS_SESSION_LOCCODE;

            fetchUsers();
        }

        private void fetchUsers()
        {
            EmployeeDataHandler empDataHandler = new EmployeeDataHandler();
            DataTable dtdis = new DataTable();

            try
            {
                dtdis = empDataHandler.getUsers(compcode, Constants.CON_STATUS_ACTIVE);
                lvusers.ItemsSource = dtdis.DefaultView;
            }
            catch (Exception ex)
            {
                Constants.CON_MESSAGE_STRING = ex.Message;
                MessageBox.Show(Constants.CON_MESSAGE_STRING);
            }
            finally
            {
                dtdis.Dispose();
                dtdis = null;
                empDataHandler = null;
            }
        }

        private void Btn_Search(object sender, RoutedEventArgs e)
        {
            EmployeeDataHandler empDataHandler = new EmployeeDataHandler();
            DataTable dtdis = new DataTable();

            try
            {
                string keyword = txt_searchkey.Text.ToString();
                dtdis = empDataHandler.searchUsers(compcode, loccode, Constants.CON_STATUS_ACTIVE, keyword);

                lvusers.ItemsSource = dtdis.DefaultView;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
            finally
            {
                dtdis.Dispose();
                dtdis = null;
                empDataHandler = null;
            }
        }
    }
}