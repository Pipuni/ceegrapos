﻿using BusinessLogicDataHandler.Main;
using ConstantsDataHandler;
using IRESPOS.KitchenDisplay;
using IRESPOS.Sales;
using IRESPOS.Settings;
using IRESPOS.StockList;
using System;
using System.Data;
using System.Windows;

namespace IRESPOS.Main
{
    /// <summary>
    /// Interaction logic for Dashboard.xaml
    /// </summary>
    public partial class Dashboard : Window
    {
        public Dashboard()
        {
            InitializeComponent();
            dispalydata();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            DashboardDataHandler dashboardDataHandler = new DashboardDataHandler();
            DataTable dataTable = new DataTable();

            try
            {
                string compcode = Constants.SYS_SESSION_COMPCODE;
                string loccode = Constants.SYS_SESSION_LOCCODE;
                string groupcode = Constants.SYS_SESSION_USERGROUP;
                string sts = Constants.CON_STATUS_ACTIVE;

                dataTable = dashboardDataHandler.getAccessRights(compcode, loccode, groupcode, sts);

                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        string modulecode = dataTable.Rows[i]["MODULECODE"].ToString();
                        setUserPermissions(modulecode);
                    }
                }
                else
                {
                    MessageBox.Show("You do not have any access granted !", Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
        }

        private void setUserPermissions(string modulecode)
        {
            try
            {
                switch (modulecode)
                {
                    case "RMMD0001":
                        button1.IsEnabled = true;
                        break;

                    case "RMMD0002":
                        button2.IsEnabled = true;
                        break;

                    case "RMMD0003":
                        button3.IsEnabled = true;
                        break;

                    case "RMMD0004":
                        button4.IsEnabled = true;
                        break;

                    case "RMMD0005":
                        button5.IsEnabled = true;
                        break;

                    case "RMMD0006":
                        button6.IsEnabled = true;
                        break;

                    case "RMMD0007":
                        button7.IsEnabled = true;
                        break;

                    case "RMMD0008":
                        button8.IsEnabled = true;
                        break;
                        //default:
                        //    Console.WriteLine("Invalid");
                        //    break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
        }

        private void dispalydata()
        {
            txtcompany.Text = Constants.SYS_SESSION_COMPANYNAME;
            txtlocation.Text = Constants.CON_SYSTEM_NAME;
            txtlocaddress.Text = Constants.SYS_SESSION_ADDRESS;
            txtusername.Text = Constants.SYS_SESSION_USERNAME;
            txtsysdate.Text = DateTime.Today.ToString("yyyy-MM-dd");
            txttime.Text = DateTime.Now.ToLongTimeString();
            txtcopyright.Text = "Copy right @ " + Constants.CON_OWNER_NAME + DateTime.Today.Year.ToString();
            txtproduct.Text = Constants.CON_SYSTEM_NAME;

            //// temp store logged user details
            //Constants.SYS_SESSION_COMPCODE = "C190000002";
            //Constants.SYS_SESSION_USERNAME = "thanuja96@yahoo.com";
            //Constants.SYS_SESSION_LOCCODE = "LC0000002";
            //Constants.SYS_SESSION_COMPANYNAME = "Mirissa Ocean View Hotel";
        }

        private void Button1_ImageFailed(object sender, ExceptionRoutedEventArgs e)
        {
        }

        private void Button1_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                sales childWindow = new sales();
                childWindow.Owner = this;
                childWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                childWindow.WindowStyle = WindowStyle.None;
                childWindow.ResizeMode = ResizeMode.NoResize;
                childWindow.Title = Constants.CON_SYSTEM_NAME + " [" + Constants.CON_OWNER_NAME + "] " + "Sales";
                childWindow.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
        }

        private void Button4_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Reports.ReportsWindows childWindow = new Reports.ReportsWindows();
                childWindow.Owner = this;
                childWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                childWindow.WindowStyle = WindowStyle.None;
                childWindow.ResizeMode = ResizeMode.NoResize;
                childWindow.Title = Constants.CON_SYSTEM_NAME + " [" + Constants.CON_OWNER_NAME + "] " + "Reports";
                childWindow.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
        }

        private void Button5_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Employees.Employees childWindow = new Employees.Employees();
                childWindow.Owner = this;
                childWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                childWindow.WindowStyle = WindowStyle.None;
                childWindow.ResizeMode = ResizeMode.NoResize;
                childWindow.Title = Constants.CON_SYSTEM_NAME + " [" + Constants.CON_OWNER_NAME + "] " + "Employees";
                childWindow.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
        }

        private void Button6_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Customer.Customer childWindow = new Customer.Customer();
                childWindow.Owner = this;
                childWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                childWindow.WindowStyle = WindowStyle.None;
                childWindow.ResizeMode = ResizeMode.NoResize;
                childWindow.Title = Constants.CON_SYSTEM_NAME + " [" + Constants.CON_OWNER_NAME + "] " + "Customers";
                childWindow.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
        }

        private void Button8_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Returns.SalesReturns childWindow = new Returns.SalesReturns();
                childWindow.Owner = this;
                childWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                childWindow.WindowStyle = WindowStyle.None;
                childWindow.ResizeMode = ResizeMode.NoResize;
                childWindow.Title = Constants.CON_SYSTEM_NAME + " [" + Constants.CON_OWNER_NAME + "] " + "Sales Returns";
                childWindow.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
        }

        private void Btn_Click_kitchendisplay(object sender, RoutedEventArgs e)
        {
            kitchendisplay childWindow = new kitchendisplay();
            childWindow.Owner = this;
            childWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            childWindow.WindowStyle = WindowStyle.None;
            childWindow.ResizeMode = ResizeMode.NoResize;
            childWindow.Title = Constants.CON_SYSTEM_NAME + " [" + Constants.CON_OWNER_NAME + "] " + "Kitchen Display";
            childWindow.ShowDialog();
        }

        private void Btn_Click_Stocklist(object sender, RoutedEventArgs e)
        {
            stocklist childWindow = new stocklist();
            childWindow.Owner = this;
            childWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            childWindow.WindowStyle = WindowStyle.None;
            childWindow.ResizeMode = ResizeMode.NoResize;
            childWindow.Title = Constants.CON_SYSTEM_NAME + " [" + Constants.CON_OWNER_NAME + "] " + "Stock List";
            childWindow.ShowDialog();
        }

        private void Btn_Click_Settings(object sender, RoutedEventArgs e)
        {
            settings childWindow = new settings();
            childWindow.Owner = this;
            childWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            childWindow.WindowStyle = WindowStyle.None;
            childWindow.ResizeMode = ResizeMode.NoResize;
            childWindow.Title = Constants.CON_SYSTEM_NAME + " [" + Constants.CON_OWNER_NAME + "] " + "Settings";
            childWindow.ShowDialog();
        }

        private void Button9_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
        }
    }
}