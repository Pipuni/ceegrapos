﻿using BusinessLogicDataHandler.KitchenDisplay;
using ConstantsDataHandler;
using System;
using System.Data;
using System.Windows;
using System.Windows.Controls;

namespace IRESPOS.KitchenDisplay
{
    /// <summary>
    /// Interaction logic for KitchenDisplay.xaml
    /// </summary>
    public partial class kitchendisplay : Window
    {
        private string COMPCODE;
        private string LOCCODE;
        private string RESTCODE;

        public kitchendisplay()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // init variables
            COMPCODE = Constants.SYS_SESSION_COMPCODE;
            LOCCODE = Constants.SYS_SESSION_LOCCODE;
            RESTCODE = Constants.SYS_SESSION_RESTCODE;

            getKitchenDisplay();
            txtDateTime.Content = DateTime.Now.ToString();
        }

        private void getKitchenDisplay()
        {
            KitchenDisplayDataHandler kitchenDisplayDataHandler = new KitchenDisplayDataHandler();
            DataTable dtmenuCat = new DataTable();

            try
            {
                dtmenuCat = kitchenDisplayDataHandler.getKitchenItems(COMPCODE, LOCCODE, RESTCODE, Constants.CON_STATUS_PENDING);
                lvKitchenItems.ItemsSource = "";
                if (dtmenuCat.Rows.Count > 0)
                {
                    lvKitchenItems.ItemsSource = dtmenuCat.DefaultView;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
            finally
            {
                dtmenuCat.Dispose();
                dtmenuCat = null;
                kitchenDisplayDataHandler = null;
            }
        }

        private void LvKitchenItems_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            KitchenDisplayDataHandler kitchenDisplayDataHandler = new KitchenDisplayDataHandler();

            try
            {
                DataRowView kitchenmenuitem = lvKitchenItems.SelectedItem as DataRowView;
                if (kitchenmenuitem != null)
                {
                    String STS = Constants.CON_STATUS_ACTIVE;
                    String LOCDATE = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");

                    Boolean isupdate = kitchenDisplayDataHandler.setOrderReady(kitchenmenuitem["ORDERCODE"].ToString(), kitchenmenuitem["MENUCATCODE"].ToString(), kitchenmenuitem["MENUCODE"].ToString(), STS, LOCDATE);
                    if (isupdate)
                    {
                        Constants.CON_MESSAGE_STRING = "Order Notified";
                        MessageBox.Show(Constants.CON_MESSAGE_STRING, Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                        getKitchenDisplay();
                    }
                    else
                    {
                        Constants.CON_MESSAGE_STRING = "Data Notify failed. Please contact system admin. ";
                        MessageBox.Show(Constants.CON_MESSAGE_STRING, Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                    }
                    lvKitchenItems.SelectedItems.Clear();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
            finally
            {
            }
        }

        private void Button_Click_Close(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}