﻿using BusinessLogicDataHandler.Login;
using ConstantsDataHandler;
using System;
using System.Data;
using System.Windows;

namespace IRESPOS.Login
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        public LoginWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            txtcopyright.Text = "Copy right @ " + Constants.CON_OWNER_NAME + DateTime.Today.Year.ToString();
            txtproduct.Text = Constants.CON_SYSTEM_NAME;
            txt_username.Focus();
        }

        private void Btn_Login(object sender, RoutedEventArgs e)
        {
            LoginDataHandler loginDataHandler = new LoginDataHandler();

            try
            {
                string username = txt_username.Text.ToString().Trim();
                string password = txt_password.Password.ToString().Trim();

                // validate
                if (!(username.Length > 0 && password.Length > 0))
                {
                    MessageBox.Show("Please enter both username and password !", Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                    return;
                }

                string encpassword = loginDataHandler.EnryptPassString(password.ToString());
                string curDate = DateTime.UtcNow.Date.ToString("yyyy-MM-dd");
                string sts = Constants.CON_STATUS_ACTIVE;
                string dsts = Constants.CON_STATUS_DELETED;

                DataRow dtuser = loginDataHandler.validateCredentials(username, encpassword, curDate, sts);

                //bool isValidated = loginDataHandler.userLoginValidation(username, encpassword, curDate, sts, dsts);

                if (dtuser != null)
                {
                    //// set sessions
                    Constants.SYS_SESSION_COMPCODE = dtuser["COMPCODE"].ToString();
                    Constants.SYS_SESSION_USERNAME = dtuser["USERNAME"].ToString();
                    Constants.SYS_SESSION_USERGROUP = dtuser["USERGROUP"].ToString();
                    Constants.SYS_SESSION_LOCCODE = dtuser["LOCCODE"].ToString();
                    Constants.SYS_SESSION_RESTCODE = dtuser["RESTCODE"].ToString();
                    Constants.SYS_SESSION_COMPANYNAME = dtuser["RESTNAME"].ToString();
                    Constants.SYS_SESSION_ADDRESS = dtuser["ADDRESS"].ToString();

                    txt_username.Text = "";
                    txt_password.Password = "";

                    Main.Dashboard childWindow = new Main.Dashboard();
                    //childWindow.Owner = this;
                    childWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                    childWindow.WindowStyle = WindowStyle.SingleBorderWindow;
                    childWindow.ResizeMode = ResizeMode.NoResize;
                    childWindow.Title = Constants.CON_SYSTEM_NAME + " [" + Constants.CON_OWNER_NAME + "] " + "Dashboard";
                    childWindow.ShowDialog();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Incorrect username or password !", Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
        }
    }
}