﻿using BusinessLogicDataHandler.Customers;
using ConstantsDataHandler;
using System;
using System.Data;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace IRESPOS.Customer
{
    /// <summary>
    /// Interaction logic for CustomerGiftCertificatesView.xaml
    /// </summary>
    public partial class CustomerGiftCertificatesView : UserControl
    {
        public CustomerGiftCertificatesView()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            loadGiftCertificates();
        }

        private void loadGiftCertificates()
        {
            CustomerDataHandler customerDataHandler = new CustomerDataHandler();
            DataTable dtdis = new DataTable();

            try
            {
                string compcode = Constants.SYS_SESSION_COMPCODE;
                string loccode = Constants.SYS_SESSION_LOCCODE;
                dtdis = customerDataHandler.getGiftCertificates(compcode, Constants.CON_STATUS_ACTIVE);

                lvgifts.ItemsSource = dtdis.DefaultView;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
            finally
            {
                dtdis.Dispose();
                dtdis = null;
                customerDataHandler = null;
            }
        }

        private void btn_newGiftCertificate(object sender, RoutedEventArgs e)
        {
            try
            {
                GiftCertificateCreate childWindow = new GiftCertificateCreate();
                childWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                childWindow.WindowStyle = WindowStyle.SingleBorderWindow;
                childWindow.ResizeMode = ResizeMode.NoResize;
                childWindow.Title = Constants.CON_SYSTEM_NAME + " [" + Constants.CON_OWNER_NAME + "] " + "Create Gift Certificate";
                childWindow.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
        }

        private void btn_edit(object sender, RoutedEventArgs e)
        {
            lvgifts.UnselectAll();
            DependencyObject dep = (DependencyObject)e.OriginalSource;

            while ((dep != null) && !(dep is ListViewItem))
            {
                dep = VisualTreeHelper.GetParent(dep);
            }

            if (dep == null)
                return;

            int index = lvgifts.ItemContainerGenerator.IndexFromContainer(dep);
            DataRowView item = lvgifts.Items[index] as DataRowView;
            string certificatecode = item.Row["CERTIFICATECODE"].ToString();

            GiftCertificateCreate childWindow = new GiftCertificateCreate(certificatecode);
            childWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            childWindow.WindowStyle = WindowStyle.SingleBorderWindow;
            childWindow.ResizeMode = ResizeMode.NoResize;
            childWindow.Title = Constants.CON_SYSTEM_NAME + " [" + Constants.CON_OWNER_NAME + "] " + "Edit Gift Certificate";
            childWindow.ShowDialog();
        }
    }
}