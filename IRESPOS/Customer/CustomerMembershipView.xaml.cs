﻿using BusinessLogicDataHandler.Customers;
using ConstantsDataHandler;
using System;
using System.Data;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace IRESPOS.Customer
{
    /// <summary>
    /// Interaction logic for CustomerMembershipView.xaml
    /// </summary>
    public partial class CustomerMembershipView : UserControl
    {
        public CustomerMembershipView()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            loadMembershipDetails();
        }

        private void loadMembershipDetails()
        {
            CustomerDataHandler customerDataHandler = new CustomerDataHandler();
            DataTable dtdis = new DataTable();

            try
            {
                string compcode = Constants.SYS_SESSION_COMPCODE;
                string loccode = Constants.SYS_SESSION_LOCCODE;
                dtdis = customerDataHandler.getMemberships(compcode, Constants.CON_STATUS_ACTIVE);

                lvmemberships.ItemsSource = dtdis.DefaultView;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
            finally
            {
                dtdis.Dispose();
                dtdis = null;
                customerDataHandler = null;
            }
        }

        private void btn_newMembership(object sender, RoutedEventArgs e)
        {
            try
            {
                MembershipCreate childWindow = new MembershipCreate();
                childWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                childWindow.WindowStyle = WindowStyle.SingleBorderWindow;
                childWindow.ResizeMode = ResizeMode.NoResize;
                childWindow.Title = Constants.CON_SYSTEM_NAME + " [" + Constants.CON_OWNER_NAME + "] " + "Create Membership";
                childWindow.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
        }

        private void btn_edit(object sender, RoutedEventArgs e)
        {
            lvmemberships.UnselectAll();
            DependencyObject dep = (DependencyObject)e.OriginalSource;

            while ((dep != null) && !(dep is ListViewItem))
            {
                dep = VisualTreeHelper.GetParent(dep);
            }

            if (dep == null)
                return;

            int index = lvmemberships.ItemContainerGenerator.IndexFromContainer(dep);
            DataRowView item = lvmemberships.Items[index] as DataRowView;
            string membershipcode = item.Row["MEMBERSHIPCODE"].ToString();

            MembershipCreate childWindow = new MembershipCreate(membershipcode);
            childWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            childWindow.WindowStyle = WindowStyle.SingleBorderWindow;
            childWindow.ResizeMode = ResizeMode.NoResize;
            childWindow.Title = Constants.CON_SYSTEM_NAME + " [" + Constants.CON_OWNER_NAME + "] " + "Edit Membership";
            childWindow.ShowDialog();
        }
    }
}