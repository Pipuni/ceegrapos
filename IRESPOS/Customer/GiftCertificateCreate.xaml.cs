﻿using BusinessLogicDataHandler.Customers;
using ConstantsDataHandler;
using System;
using System.Data;
using System.Windows;

namespace IRESPOS.Customer
{
    /// <summary>
    /// Interaction logic for GiftCertificateCreate.xaml
    /// </summary>
    public partial class GiftCertificateCreate : Window
    {
        public GiftCertificateCreate()
        {
            InitializeComponent();
        }

        public GiftCertificateCreate(string certificatecode)
        {
            InitializeComponent();
            loadGiftCertificate(certificatecode);
        }

        private void loadGiftCertificate(String certificatecode)
        {
            CustomerDataHandler customerDataHandler = new CustomerDataHandler();
            DataTable dtdis = new DataTable();

            try
            {
                string compcode = Constants.SYS_SESSION_COMPCODE;
                string loccode = Constants.SYS_SESSION_LOCCODE;
                dtdis = customerDataHandler.getSelectedGiftCertificate(compcode, certificatecode, Constants.CON_STATUS_ACTIVE);

                if (dtdis.Rows.Count > 0)
                {
                    DataView dtview = dtdis.DefaultView;
                    lbl_certificatecode.Content = dtview[0]["CERTIFICATECODE"].ToString();
                    txt_giftname.Text = dtview[0]["GIFTNAME"].ToString();
                    txt_refcode.Text = dtview[0]["REFERENCECODE"].ToString();
                    txt_amount.Text = dtview[0]["AMOUNT"].ToString();
                    expiry_date.Text = dtview[0]["EXPIRYDATE"].ToString();
                }
                else
                {
                    MessageBox.Show("No data found !", Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
            finally
            {
                dtdis.Dispose();
                dtdis = null;
                customerDataHandler = null;
            }
        }

        private void clearAll()
        {
            lbl_certificatecode.Content = "";
            txt_refcode.Text = "";
            txt_giftname.Text = "";
            txt_amount.Text = "";
            expiry_date.SelectedDate = null;
        }

        private Boolean validateFields()
        {
            if (txt_refcode.Text.Length > 0 && txt_giftname.Text.Length > 0 && txt_amount.Text.Length > 0 && expiry_date.Text.Length > 0)
            {
                return true;
            }
            return false;
        }

        private void Btn_Confirm(object sender, RoutedEventArgs e)
        {
            CustomerDataHandler customerDataHandler = new CustomerDataHandler();

            try
            {
                if (!validateFields())
                {
                    MessageBox.Show("Please fill in all fields !", Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                    return;
                }

                string compcode = Constants.SYS_SESSION_COMPCODE;
                string loccode = Constants.SYS_SESSION_LOCCODE;
                string certificatecode = lbl_certificatecode.Content.ToString().Trim(); ;
                string referencecode = txt_refcode.Text.ToString().Trim();
                string giftname = txt_giftname.Text.ToString().Trim();
                string amount = txt_amount.Text.ToString().Trim();
                string expirydate = Convert.ToDateTime(expiry_date.Text).ToString("yyyy/MM/dd");
                string sts = Constants.CON_STATUS_ACTIVE;
                string enuser = Constants.SYS_SESSION_USERNAME;
                string locdate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");

                Boolean iscreate = customerDataHandler.createGiftCertificate(compcode, loccode, certificatecode, referencecode, giftname, amount, expirydate, sts, enuser, locdate);
                if (iscreate)
                {
                    clearAll();
                    Constants.CON_MESSAGE_STRING = "Gift certificate saved successfully !";
                    MessageBox.Show(Constants.CON_MESSAGE_STRING, Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                }
                else
                {
                    Constants.CON_MESSAGE_STRING = "Data update failed. Please contact sytem admin. ";
                    MessageBox.Show(Constants.CON_MESSAGE_STRING, Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
            finally
            {
                customerDataHandler = null;
            }
        }
    }
}