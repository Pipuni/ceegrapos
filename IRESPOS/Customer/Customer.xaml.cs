﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace IRESPOS.Customer
{
    /// <summary>
    /// Interaction logic for Customer.xaml
    /// </summary>
    public partial class Customer : Window
    {
        public Customer()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            loadFirstView();
            txtDateTime.Content = DateTime.Now.ToString();
        }

        private void loadFirstView()
        {
            UserControl userControl = new CustomerControlViewList();
            GridDisplay.Children.Add(userControl);
        }

        private void Tab_Switched(object sender, RoutedEventArgs e)
        {
            UserControl userControl = null;
            GridDisplay.Children.Clear();

            int index = int.Parse(((Button)e.Source).Uid);

            GridCursor.Margin = new Thickness(10 + (150 * index), 0, 0, 0);

            switch (index)
            {
                case 0:
                    userControl = new CustomerControlViewList();
                    GridDisplay.Children.Add(userControl);
                    break;

                case 1:
                    userControl = new CustomerControlAddNew();
                    GridDisplay.Children.Add(userControl);
                    break;

                case 2:
                    userControl = new CustomerMembershipView();
                    GridDisplay.Children.Add(userControl);
                    break;

                case 3:
                    userControl = new CustomerGiftCertificatesView();
                    GridDisplay.Children.Add(userControl);
                    break;
            }
        }

        private void Button_Click_Close(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}