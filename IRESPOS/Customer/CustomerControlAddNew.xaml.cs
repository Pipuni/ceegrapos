﻿using BusinessLogicDataHandler.Customers;
using ConstantsDataHandler;
using System;
using System.Collections.Generic;
using System.Data;
using System.Windows;
using System.Windows.Controls;

namespace IRESPOS.Customer
{
    /// <summary>
    /// Interaction logic for CustomerControlAddNew.xaml
    /// </summary>
    public partial class CustomerControlAddNew : UserControl
    {
        private string selectedCusType;
        private string compcode;
        private string enuser;
        private string loccode;

        public CustomerControlAddNew()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            selectedCusType = Constants.CON_CUS_TYP_GUEST;
            compcode = Constants.SYS_SESSION_COMPCODE;
            enuser = Constants.SYS_SESSION_USERNAME;
            loccode = Constants.SYS_SESSION_LOCCODE;

            onStart();
        }

        private void onStart()
        {
            typeGuestSelected();
            getCustomerTitles();
            getCountries();
            getClientTypes();
            getGuestTypes();
        }

        private void typeGuestSelected()
        {
            selectedCusType = Constants.CON_CUS_TYP_GUEST;
            combo_clienttype.Visibility = Visibility.Collapsed;
            lbl_clienttype.Visibility = Visibility.Collapsed;
            combo_guesttype.Visibility = Visibility.Visible;
            lbl_guesttype.Visibility = Visibility.Visible;
            txt_vatno.Visibility = Visibility.Collapsed;
            lbl_vatno.Visibility = Visibility.Collapsed;
        }

        private void typeClientSelected()
        {
            selectedCusType = Constants.CON_CUS_TYP_CLIENT;
            combo_clienttype.Visibility = Visibility.Visible;
            lbl_clienttype.Visibility = Visibility.Visible;
            combo_guesttype.Visibility = Visibility.Collapsed;
            lbl_guesttype.Visibility = Visibility.Collapsed;
            txt_vatno.Visibility = Visibility.Visible;
            lbl_vatno.Visibility = Visibility.Visible;
        }

        private void getClientTypes()
        {
            CustomerDataHandler customerDataHandler = new CustomerDataHandler();
            DataTable dtdis = new DataTable();
            combo_clienttype.Items.Clear();

            try
            {
                dtdis = customerDataHandler.getClientTypes(Constants.CON_STATUS_ACTIVE);
                combo_clienttype.ItemsSource = dtdis.DefaultView;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
            finally
            {
                dtdis.Dispose();
                dtdis = null;
                customerDataHandler = null;
            }
        }

        private void getCountries()
        {
            CustomerDataHandler customerDataHandler = new CustomerDataHandler();
            DataTable dtdis = new DataTable();
            combo_country.Items.Clear();

            try
            {
                dtdis = customerDataHandler.getCountries();
                combo_country.ItemsSource = dtdis.DefaultView;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
            finally
            {
                dtdis.Dispose();
                dtdis = null;
                customerDataHandler = null;
            }
        }

        private void getGuestTypes()
        {
            CustomerDataHandler customerDataHandler = new CustomerDataHandler();
            DataTable dtdis = new DataTable();
            combo_guesttype.Items.Clear();

            try
            {
                dtdis = customerDataHandler.getGuestTypes(Constants.CON_STATUS_ACTIVE);
                combo_guesttype.ItemsSource = dtdis.DefaultView;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
            finally
            {
                dtdis.Dispose();
                dtdis = null;
                customerDataHandler = null;
            }
        }

        private void getCustomerTitles()
        {
            CustomerDataHandler customerDataHandler = new CustomerDataHandler();

            try
            {
                Dictionary<string, string> titles = new Dictionary<string, string>();
                titles = customerDataHandler.getCustomerTitles();
                combo_title.ItemsSource = titles;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
            finally
            {
                customerDataHandler = null;
            }
        }

        private void createGuest()
        {
            CustomerDataHandler customerDataHandler = new CustomerDataHandler();

            try
            {
                string guestrefcode = Constants.CON_NULL_VALUE;
                string guesttype = combo_guesttype.SelectedValue.ToString();
                string nicpp = txt_nicpp.Text.ToString();
                string title = combo_title.SelectedValue.ToString();
                string fname = txt_fname.Text.ToString();
                string lname = txt_lname.Text.ToString();
                string email = txt_email.Text.ToString();
                string contact = txt_contactno.Text.ToString();
                string country = combo_country.SelectedValue.ToString();
                string sts = Constants.CON_STATUS_ACTIVE;
                string locdate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");

                Boolean iscreate = customerDataHandler.CreateGuest(compcode, loccode, guestrefcode, guesttype, nicpp, title, fname, lname, email, contact, country, sts, enuser, locdate);
                if (iscreate)
                {
                    clearAll();
                    Constants.CON_MESSAGE_STRING = "User created successfully !";
                    MessageBox.Show(Constants.CON_MESSAGE_STRING, Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                }
                else
                {
                    Constants.CON_MESSAGE_STRING = "Data update failed. Please contact sytem admin. ";
                    MessageBox.Show(Constants.CON_MESSAGE_STRING, Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
            finally
            {
                customerDataHandler = null;
            }
        }

        private void createClient()
        {
            CustomerDataHandler customerDataHandler = new CustomerDataHandler();

            try
            {
                string clientrefcode = Constants.CON_NULL_VALUE;
                string clienttype = combo_clienttype.SelectedValue.ToString();
                string vatno = txt_vatno.Text.ToString();
                string nicpp = txt_nicpp.Text.ToString();
                string title = combo_title.SelectedValue.ToString();
                string fname = txt_fname.Text.ToString();
                string lname = txt_lname.Text.ToString();
                string email = txt_email.Text.ToString();
                string contact = txt_contactno.Text.ToString();
                string country = combo_country.SelectedValue.ToString();
                string sts = Constants.CON_STATUS_ACTIVE;
                string locdate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");

                Boolean iscreate = customerDataHandler.createClient(clientrefcode, compcode, clienttype, vatno, nicpp, title, fname, lname, email, contact, country, sts, enuser, locdate);
                if (iscreate)
                {
                    clearAll();
                    Constants.CON_MESSAGE_STRING = "User created successfully !";
                    MessageBox.Show(Constants.CON_MESSAGE_STRING, Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                }
                else
                {
                    Constants.CON_MESSAGE_STRING = "Data update failed. Please contact sytem admin. ";
                    MessageBox.Show(Constants.CON_MESSAGE_STRING, Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
            finally
            {
                customerDataHandler = null;
            }
        }

        private void clearAll()
        {
            combo_title.SelectedIndex = -1;
            combo_country.SelectedIndex = -1;
            combo_clienttype.SelectedIndex = -1;
            combo_guesttype.SelectedIndex = -1;

            txt_contactno.Text = "";
            txt_email.Text = "";
            txt_fname.Text = "";
            txt_lname.Text = "";
            txt_nicpp.Text = "";
            txt_vatno.Text = "";
        }

        private Boolean validateFields()
        {
            if (txt_nicpp.Text.Length > 0 && combo_title.SelectedIndex > -1 && txt_fname.Text.Length > 0 && txt_lname.Text.Length > 0
                && txt_email.Text.Length > 0 && txt_contactno.Text.Length > 0 && combo_country.SelectedIndex > -1)
            {
                if ((combo_guesttype.SelectedIndex > -1) || (combo_clienttype.SelectedIndex > -1 && txt_vatno.Text.Length > 0))
                {
                    return true;
                }
            }
            return false;
        }

        private void Btn_UserTypeSelect(object sender, RoutedEventArgs e)
        {
            try
            {
                int index = int.Parse(((Button)e.Source).Uid);
                UserTypeCursor.Margin = new Thickness(10, 10 + (150 * index), 0, 0);

                switch (index)
                {
                    case 0:
                        typeGuestSelected();
                        break;

                    case 1:
                        typeClientSelected();
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
        }

        private void Btn_Confirm(object sender, RoutedEventArgs e)
        {
            try
            {
                // validation
                if (!validateFields())
                {
                    MessageBox.Show("Please fill in all fields to continue !", Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                    return;
                }
                if (selectedCusType == Constants.CON_CUS_TYP_GUEST)
                {
                    createGuest();
                }
                else if (selectedCusType == Constants.CON_CUS_TYP_CLIENT)
                {
                    createClient();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
        }

        private void txt_nicpp_TextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {

        }

        private void txt_fname_TextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(e.Text, "[^a-zA-Z]+"))
            {
                e.Handled = true;
            }
        }

        private void txt_lname_TextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(e.Text, "[^a-zA-Z]+"))
            {
                e.Handled = true;
            }
        }

        private void txt_vatno_TextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(e.Text, "[^0-9]"))
            {
                e.Handled = true;
            }
        }

        private void txt_contactno_TextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(e.Text, "[^0-9]"))
            {
                e.Handled = true;
            }
        }

        private void txt_fname_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(e.Text, "[^a-zA-Z]+"))
            {
                e.Handled = true;
            }
        }

        private void txt_lname_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(e.Text, "[^a-zA-Z]+"))
            {
                e.Handled = true;
            }
        }

        private void txt_contactno_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(e.Text, "[^0-9]"))
            {
                e.Handled = true;
            }
        }

        private void txt_vatno_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(e.Text, "[^0-9]"))
            {
                e.Handled = true;
            }
        }
    }
}