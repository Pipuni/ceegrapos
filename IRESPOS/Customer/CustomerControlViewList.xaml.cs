﻿using BusinessLogicDataHandler.Customers;
using ConstantsDataHandler;
using System;
using System.Data;
using System.Windows;
using System.Windows.Controls;

namespace IRESPOS.Customer
{
    /// <summary>
    /// Interaction logic for CustomerControlViewList.xaml
    /// </summary>
    public partial class CustomerControlViewList : UserControl
    {
        private string compcode = Constants.SYS_SESSION_COMPCODE;
        private string loccode = Constants.SYS_SESSION_LOCCODE;

        public CustomerControlViewList()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            compcode = Constants.SYS_SESSION_COMPCODE;
            loccode = Constants.SYS_SESSION_LOCCODE;

            fetchUsers();
        }

        private void fetchUsers()
        {
            CustomerDataHandler customerDataHandler = new CustomerDataHandler();
            DataTable dtdis = new DataTable();

            try
            {
                dtdis = customerDataHandler.getCustomers(compcode, loccode, Constants.CON_STATUS_ACTIVE);

                lvusers.ItemsSource = dtdis.DefaultView;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
            finally
            {
                dtdis.Dispose();
                dtdis = null;
                customerDataHandler = null;
            }
        }

        private void Btn_Search(object sender, RoutedEventArgs e)
        {
            CustomerDataHandler customerDataHandler = new CustomerDataHandler();
            DataTable dtdis = new DataTable();

            try
            {
                string keyword = txt_searchkey.Text.ToString();
                dtdis = customerDataHandler.searchCustomers(compcode, loccode, Constants.CON_STATUS_ACTIVE, keyword);

                lvusers.ItemsSource = dtdis.DefaultView;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
            finally
            {
                dtdis.Dispose();
                dtdis = null;
                customerDataHandler = null;
            }
        }
    }
}