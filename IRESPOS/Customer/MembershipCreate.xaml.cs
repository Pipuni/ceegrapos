﻿using BusinessLogicDataHandler.Customers;
using ConstantsDataHandler;
using System;
using System.Data;
using System.Windows;

namespace IRESPOS.Customer
{
    /// <summary>
    /// Interaction logic for MembershipCreate.xaml
    /// </summary>
    public partial class MembershipCreate : Window
    {
        public MembershipCreate()
        {
            InitializeComponent();
        }

        public MembershipCreate(string membershipcode)
        {
            InitializeComponent();
            loadMembershipDetails(membershipcode);
        }

        private void loadMembershipDetails(string membershipcode)
        {
            CustomerDataHandler customerDataHandler = new CustomerDataHandler();
            DataTable dtdis = new DataTable();

            try
            {
                string compcode = Constants.SYS_SESSION_COMPCODE;
                string loccode = Constants.SYS_SESSION_LOCCODE;
                dtdis = customerDataHandler.getSelectedMembership(compcode, membershipcode, Constants.CON_STATUS_ACTIVE);

                if (dtdis.Rows.Count > 0)
                {
                    DataView dtview = dtdis.DefaultView;
                    lbl_membershipcode.Content = dtview[0]["MEMBERSHIPCODE"].ToString();
                    txt_membname.Text = dtview[0]["GROUPNAME"].ToString();
                    txt_discount.Text = dtview[0]["DISCOUNT"].ToString();
                    txt_amountforpoints.Text = dtview[0]["AMOUNTFORPOINTS"].ToString();
                    txt_pointsforamount.Text = dtview[0]["POINTSFORAMOUNT"].ToString();
                    txt_pointstoamount.Text = dtview[0]["POINTSTOAMOUNT"].ToString();
                    txt_amounttopoints.Text = dtview[0]["AMOUNTTOPOINTS"].ToString();
                }
                else
                {
                    MessageBox.Show("No data found !", Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
            finally
            {
                dtdis.Dispose();
                dtdis = null;
                customerDataHandler = null;
            }
        }

        private void clearAll()
        {
            lbl_membershipcode.Content = "";
            txt_membname.Text = "";
            txt_discount.Text = "";
            txt_pointsforamount.Text = "";
            txt_amountforpoints.Text = "";
            txt_pointstoamount.Text = "";
            txt_amounttopoints.Text = "";
        }

        private Boolean validateFieldss()
        {
            if (txt_membname.Text.Length > 0 && txt_discount.Text.Length > 0 && txt_amountforpoints.Text.Length > 0 && txt_pointsforamount.Text.Length > 0
                && txt_pointstoamount.Text.Length > 0 && txt_amounttopoints.Text.Length > 0)
            {
                return true;
            }
            return false;
        }

        private void Btn_Confirm(object sender, RoutedEventArgs e)
        {
            CustomerDataHandler customerDataHandler = new CustomerDataHandler();

            try
            {
                if (!validateFieldss())
                {
                    MessageBox.Show("Please fill in all fields !", Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                    return;
                }
                string compcode = Constants.SYS_SESSION_COMPCODE;
                string loccode = Constants.SYS_SESSION_LOCCODE;
                string membershipcode = lbl_membershipcode.Content.ToString().Trim(); ;
                string groupname = txt_membname.Text.ToString().Trim();
                string discount = txt_discount.Text.ToString().Trim();

                string amountforpoints = txt_amountforpoints.Text.ToString().Trim();
                string pointsforamount = txt_pointsforamount.Text.ToString().Trim();
                string pointstoamount = txt_pointstoamount.Text.ToString().Trim();
                string amounttopoints = txt_amounttopoints.Text.ToString().Trim();

                string sts = Constants.CON_STATUS_ACTIVE;
                string enuser = Constants.SYS_SESSION_USERNAME;
                string locdate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");

                Boolean iscreate = customerDataHandler.createMembershipGroup(compcode, loccode, membershipcode, groupname, discount, amountforpoints, pointsforamount, pointstoamount, amounttopoints, sts, enuser, locdate);
                if (iscreate)
                {
                    clearAll();
                    Constants.CON_MESSAGE_STRING = "Membership created successfully !";
                    MessageBox.Show(Constants.CON_MESSAGE_STRING, Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                }
                else
                {
                    Constants.CON_MESSAGE_STRING = "Data update failed. Please contact sytem admin. ";
                    MessageBox.Show(Constants.CON_MESSAGE_STRING, Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
            finally
            {
                customerDataHandler = null;
            }
        }
    }
}