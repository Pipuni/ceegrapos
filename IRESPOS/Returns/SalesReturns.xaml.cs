﻿using BusinessLogicDataHandler.Returns;
using BusinessLogicDataHandler.Tax;
using ConstantsDataHandler;
using System;
using System.Data;
using System.Linq;
using System.Windows;

namespace IRESPOS.Returns
{
    /// <summary>
    /// Interaction logic for SalesReturns.xaml
    /// </summary>
    public partial class SalesReturns : Window
    {
        private DataTable taxTb;
        private DataTable orderDetailsTb;
        private DataTable removedDataTable;

        public SalesReturns()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            txtDateTime.Content = DateTime.Now.ToString();
            initRemoveDataTable();
        }

        private void initRemoveDataTable()
        {
            removedDataTable = new DataTable();
            removedDataTable.Clear();
            removedDataTable.Columns.Add("MENUCATCODE");
            removedDataTable.Columns.Add("MENUCODE");
            removedDataTable.Columns.Add("STS");
        }

        private void clearAll()
        {
            dtdet.ItemsSource = null;
            orderDetailsTb = null;
            txtbarcode.Text = "";
            lblordercode.Text = "";
            lblorderdate.Text = "";
            lbltablename.Text = "";
            lblordertotal.Text = "";
            initRemoveDataTable();
        }

        private void calcOrderTotal()
        {
            if (orderDetailsTb.Rows.Count > 0)
            {
                decimal totAmntwithtax = orderDetailsTb.AsEnumerable()
                                             .Where(y => y.Field<string>("ISTAXAPPLY") == Constants.CON_STATUS_TXNOTAPPLIED)
                                             .Sum(x => x.Field<decimal>("AMOUNT"));

                decimal totAmntwithouttax = orderDetailsTb.AsEnumerable()
                    .Where(y => y.Field<string>("ISTAXAPPLY") == Constants.CON_STATUS_TXAPPLIED)
                                   .Sum(x => x.Field<decimal>("AMOUNT"));

                GLTaxCalculator gLTaxCalculator = new GLTaxCalculator();

                taxTb = gLTaxCalculator.calculateTaxAmount(Constants.SYS_SESSION_COMPCODE, Constants.SYS_SESSION_LOCCODE, Constants.CON_TAX_RM, Convert.ToDouble(totAmntwithouttax), Convert.ToDouble(totAmntwithtax));

                string netamount = taxTb.Select("TAXTYPE = '" + Constants.CON_TAX_NETT_RW + "'")[0]["TAXAMOUNT"].ToString();
                lblordertotal.Text = netamount.ToString();
            }
        }

        private void getOrderDetails()
        {
            dtdet.ItemsSource = null;
            lblorderdate.Text = "";
            lbltablename.Text = "";
            lblordertotal.Text = "0";
            initRemoveDataTable();

            if (txtbarcode.Text.Length < 1)
            {
                MessageBox.Show("Please enter order code !", Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                return;
            }
            ReturnsDataHandler returnsDataHandler = new ReturnsDataHandler();

            string compcode = Constants.SYS_SESSION_COMPCODE;
            string loccode = Constants.SYS_SESSION_LOCCODE;
            string orderCode = txtbarcode.Text.ToString().Trim();
            string orderdate = DateTime.UtcNow.Date.ToString("yyyy-MM-dd");
            string sts = Constants.CON_STATUS_DELETED;
            orderDetailsTb = returnsDataHandler.getOrderDetails(compcode, loccode, orderCode, orderdate, sts);
            if (orderDetailsTb.Rows.Count > 0)
            {
                lblordercode.Text = orderCode;
                dtdet.ItemsSource = orderDetailsTb.DefaultView;

                lblorderdate.Text = orderDetailsTb.DefaultView[0]["ORDERENDATE"].ToString();
                lbltablename.Text = orderDetailsTb.DefaultView[0]["TABLENAME"] as string;

                calcOrderTotal();
            }
            else
            {
                MessageBox.Show("No data found !", Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
        }

        private void SaveDetails()
        {
            if (!(removedDataTable.Rows.Count > 0))
            {
                MessageBox.Show("No changes made to save !", Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                return;
            }
            ReturnsDataHandler returnsDataHandler = new ReturnsDataHandler();
            //MessageBoxResult messageBoxResult = MessageBox.Show("Are you sure you want to save this order?", Constants.CON_OWNER_NAME, MessageBoxButton.YesNo, MessageBoxImage.Information);
            //if (messageBoxResult == MessageBoxResult.Yes)
            //{
            try
            {
                string compcode = Constants.SYS_SESSION_COMPCODE;
                string loccode = Constants.SYS_SESSION_LOCCODE;
                string ordercode = lblordercode.Text.ToString().Trim();
                string sts = Constants.CON_STATUS_DELETED;
                string paysts = Constants.CON_STATUS_ACTIVE;
                string netamount = lblordertotal.Text.ToString().Trim();

                // remove unnecessary columns from the tax table
                for (int i = taxTb.Rows.Count - 1; i >= 0; i--)
                {
                    DataRow taxrw = taxTb.Rows[i];
                    if (taxrw["TAXTYPE"].Equals(Constants.CON_TAX_GROSS_TYPE))
                    {
                        taxTb.Rows.RemoveAt(i);
                    }
                    else if (taxrw["TAXTYPE"].Equals(Constants.CON_TT_TAX_AMOUNT))
                    {
                        taxTb.Rows.RemoveAt(i);
                    }
                    else if (taxrw["TAXTYPE"].Equals(Constants.CON_TAX_NETT_RW))
                    {
                        taxTb.Rows.RemoveAt(i);
                    }
                }

                DataTable dtorders = orderDetailsTb.DefaultView.ToTable(true, "MENUCATCODE", "MENUCODE", "QTY", "RATE", "AMOUNT");
                DataTable taxTable = taxTb.DefaultView.ToTable(true, "TAXPGCODE", "COMPCODE", "LOCCODE", "TAXCODE", "TAXRATE", "TAXAMOUNT");

                Boolean iscreate = returnsDataHandler.updateOrderDetails(compcode, loccode, ordercode, netamount, sts, paysts, dtorders, removedDataTable, taxTable);

                if (iscreate)
                {
                    Constants.CON_MESSAGE_STRING = "Order updated !";
                    MessageBox.Show(Constants.CON_MESSAGE_STRING, Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                }
                else
                {
                    Constants.CON_MESSAGE_STRING = "Data update failed. Please contact sytem admin. ";
                    MessageBox.Show(Constants.CON_MESSAGE_STRING, Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
            //}
        }

        private void BtnDel_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (lblordercode.Text == "")
                {
                    MessageBox.Show("No order selected !", Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                    return;
                }
                MessageBoxResult messageBoxResult = MessageBox.Show("Are you sure you want to delete this order?", Constants.CON_OWNER_NAME, MessageBoxButton.YesNo, MessageBoxImage.Information);
                if (messageBoxResult == MessageBoxResult.Yes)
                {
                    ReturnsDataHandler returnsDataHandler = new ReturnsDataHandler();

                    string ordercode = lblordercode.Text;
                    string compcode = Constants.SYS_SESSION_COMPCODE;
                    string loccode = Constants.SYS_SESSION_LOCCODE;
                    string sts = Constants.CON_STATUS_DELETED;

                    Boolean iscreate = returnsDataHandler.disableOrder(compcode, loccode, ordercode, sts);
                    if (iscreate)
                    {
                        clearAll();
                        Constants.CON_MESSAGE_STRING = "Order cancelled !";
                        MessageBox.Show(Constants.CON_MESSAGE_STRING, Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                    }
                    else
                    {
                        Constants.CON_MESSAGE_STRING = "Data update failed. Please contact sytem admin. ";
                        MessageBox.Show(Constants.CON_MESSAGE_STRING, Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
        }

        private void BtnItmDel_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!(dtdet.SelectedIndex > -1))
                {
                    MessageBox.Show("Please select a row to remove", Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                    return;
                }
                MessageBoxResult messageBoxResult = MessageBox.Show("Are you sure you want to delete this item?", Constants.CON_OWNER_NAME, MessageBoxButton.YesNo, MessageBoxImage.Information);
                if (messageBoxResult == MessageBoxResult.Yes)
                {
                    DataRow row = removedDataTable.NewRow();
                    row["MENUCATCODE"] = orderDetailsTb.Rows[dtdet.SelectedIndex]["MENUCATCODE"];
                    row["MENUCODE"] = orderDetailsTb.Rows[dtdet.SelectedIndex]["MENUCODE"];
                    row["STS"] = orderDetailsTb.Rows[dtdet.SelectedIndex]["STS"];
                    removedDataTable.Rows.Add(row);

                    double rowqty = Double.Parse(orderDetailsTb.Rows[dtdet.SelectedIndex]["QTY"].ToString());
                    if (rowqty <= 1.0)
                    {
                        orderDetailsTb.Rows.RemoveAt(dtdet.SelectedIndex);
                    }
                    else
                    {
                        orderDetailsTb.Rows[dtdet.SelectedIndex]["QTY"] = (Decimal.Parse(orderDetailsTb.Rows[dtdet.SelectedIndex]["QTY"].ToString()) - 1);
                        orderDetailsTb.Rows[dtdet.SelectedIndex]["AMOUNT"] = Math.Round(Decimal.Parse(orderDetailsTb.Rows[dtdet.SelectedIndex]["QTY"].ToString()) * Decimal.Parse(orderDetailsTb.Rows[dtdet.SelectedIndex]["RATE"].ToString()), 2);
                    }

                    calcOrderTotal();
                    SaveDetails();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
        }

        private void ButtonSearch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                getOrderDetails();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
        }

        private void BtnClear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                clearAll();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
        }

        private void BtnFind_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var childWindow = new SalesReturnPopUp();
                childWindow.Owner = this;
                childWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                childWindow.WindowStyle = WindowStyle.SingleBorderWindow;
                childWindow.ResizeMode = ResizeMode.NoResize;
                childWindow.Title = Constants.CON_SYSTEM_NAME + " [" + Constants.CON_OWNER_NAME + "] " + "Select order";
                childWindow.ShowDialog();

                string orderCode = childWindow.getSelectedKey;
                if (orderCode != "")
                {
                    txtbarcode.Text = orderCode.Trim();
                    getOrderDetails();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
        }

        private void Button_Click_Close(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}