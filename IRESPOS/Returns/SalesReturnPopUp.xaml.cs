﻿using BusinessLogicDataHandler.Returns;
using ConstantsDataHandler;
using System;
using System.Data;
using System.Windows;

namespace IRESPOS.Returns
{
    /// <summary>
    /// Interaction logic for SalesReturnPopUp.xaml
    /// </summary>
    public partial class SalesReturnPopUp : Window
    {
        public SalesReturnPopUp()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            loadOrders();
            txtDateTime.Content = DateTime.Now.ToString();
        }

        public string getSelectedKey
        {
            get { return lblordercode.Text; }
        }

        private void loadOrders()
        {
            ReturnsDataHandler returnsDataHandler = new ReturnsDataHandler();

            try
            {
                string compcode = Constants.SYS_SESSION_COMPCODE;
                string loccode = Constants.SYS_SESSION_LOCCODE;
                string sts = Constants.CON_STATUS_DELETED;
                string detsts = Constants.CON_STATUS_DELETED;
                string orderdate = DateTime.UtcNow.Date.ToString("yyyy-MM-dd");

                dtorders.ItemsSource = returnsDataHandler.getOrders(compcode, loccode, sts, detsts, orderdate).DefaultView;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
            finally
            {
                returnsDataHandler = null;
            }
        }

        private Boolean selectRow()
        {
            if (dtorders.SelectedIndex > -1)
            {
                DataRowView row = (DataRowView)dtorders.SelectedItems[0];
                string orderCode = row["ORDERCODE"].ToString();
                lblordercode.Text = orderCode;
                return true;
            }
            else
            {
                MessageBox.Show("Please select a row to continue !", Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
            return false;
        }

        private void btn_Search(object sender, RoutedEventArgs e)
        {
            if (!(txtsearchkey.Text.Length > 0))
            {
                loadOrders();
                return;
            }
            ReturnsDataHandler returnsDataHandler = new ReturnsDataHandler();
            DataTable dtdis;

            try
            {
                string compcode = Constants.SYS_SESSION_COMPCODE;
                string loccode = Constants.SYS_SESSION_LOCCODE;
                string sts = Constants.CON_STATUS_DELETED;
                string detsts = Constants.CON_STATUS_DELETED;
                string key = txtsearchkey.Text.ToString().Trim();
                string orderdate = DateTime.UtcNow.Date.ToString("yyyy-MM-dd");

                dtdis = returnsDataHandler.getOrdersByKey(compcode, loccode, sts, detsts, key, orderdate);
                dtorders.ItemsSource = dtdis.DefaultView;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
            finally
            {
                dtdis = null;
                returnsDataHandler = null;
            }
        }

        private void dtorders_RowDoubleClicked(object sender, RoutedEventArgs e)
        {
            try
            {
                selectRow();
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
        }

        private void btn_Cancel(object sender, RoutedEventArgs e)
        {
            try
            {
                lblordercode.Text = "";
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
        }

        private void btn_Select(object sender, RoutedEventArgs e)
        {
            try
            {
                if (selectRow())
                {
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
        }

        private void Button_Click_Close(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}