﻿using BusinessLogicDataHandler.Reports.Sales;
using BusinessLogicDataHandler.Sales;
using ConstantsDataHandler;
using Microsoft.Reporting.WinForms;
using System;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace IRESPOS.Sales
{
    /// <summary>
    /// Interaction logic for salespayment.xaml
    /// </summary>
    public partial class salespayment : Window
    {
        private DataTable pmtaxtb;
        private DataTable pmdtsales;
        private String pmtableCode;
        private DataTable pmkotmenudt;
        private string COMPCODE;
        private string LOCCODE;
        private string ENUSER;
        private string RESTCODE;
        private string pmtableName;

        public salespayment()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // init variables
            COMPCODE = Constants.SYS_SESSION_COMPCODE;
            LOCCODE = Constants.SYS_SESSION_LOCCODE;
            ENUSER = Constants.SYS_SESSION_USERNAME;
            RESTCODE = Constants.SYS_SESSION_RESTCODE;

            setpaymentoptions();
            setRoomNos();
        }

        public void assignData(string txtOrderCodeNo, DataTable taxtb, DataTable dtsales, String tableCode, String tableName, DataTable kotmenudt)
        {
            pmtaxtb = taxtb;
            pmdtsales = dtsales;
            pmtableCode = tableCode;
            pmkotmenudt = kotmenudt;
            pmtableName = tableName;
            txtlabel.Text = "ORDER FOR TABLE " + tableName + "\nORDER CODE: " + txtOrderCodeNo;
            txtOrderCode.Text = txtOrderCodeNo;
            foreach (DataRow taxrw in pmtaxtb.Rows)
            {
                if (taxrw["TAXTYPE"].ToString().Equals(Constants.CON_TAX_NETT_RW))
                {
                    String netamnt = taxrw["TAXAMOUNT"].ToString();
                    txtnetamnt.Text = netamnt;
                }
            }
        }

        private void setRoomNos()
        {
            SalesDataHandler salesDataHandler = new SalesDataHandler();
            DataTable dtroomno = new DataTable();
            cmdroomno.Items.Clear();

            try
            {
                dtroomno = salesDataHandler.getRoomNoCode(COMPCODE, LOCCODE, DateTime.Today.ToString("yyyy-MM-dd"));
                //dtroomno = salesDataHandler.getRoomNoCode(COMPCODE, LOCCODE, "2019-11-30");
                cmdroomno.ItemsSource = dtroomno.DefaultView;
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
            finally
            {
                salesDataHandler = null;
                dtroomno.Dispose();
                dtroomno = null;
            }
        }

        private void setpaymentoptions()
        {
            SalesDataHandler salesDataHandler = new SalesDataHandler();
            DataTable dtdis = new DataTable();
            cmbpaymenttyp.Items.Clear();

            try
            {
                dtdis = salesDataHandler.getpaymentoptions(COMPCODE, LOCCODE, Constants.CON_STATUS_ACTIVE);
                cmbpaymenttyp.ItemsSource = dtdis.DefaultView;
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
            finally
            {
                salesDataHandler = null;
                dtdis.Dispose();
                dtdis = null;
            }
        }

        private void BtnExit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Btnsave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                createOrders();
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
        }

        private void createOrders()
        {
            DataTable cppmtaxtb = pmtaxtb.Copy();
            DataTable cppmdtsales = pmdtsales.Copy();

            SalesDataHandler esalesDataHandler = new SalesDataHandler();
            if (cppmdtsales.Columns.Contains("MENUNAME"))
            {
                cppmdtsales.Columns.Remove("MENUNAME");
                cppmdtsales.Columns.Remove("ISTAXAPPLY");
            }
            cppmdtsales.AcceptChanges();

            for (int i = cppmtaxtb.Rows.Count - 1; i >= 0; i--)
            {
                DataRow taxrw = cppmtaxtb.Rows[i];

                if (taxrw["TAXTYPE"].Equals(Constants.CON_TAX_GROSS_TYPE))
                {
                    //GROSSAMNT = taxrw["TAXAMOUNT"].ToString();
                    taxrw.Delete();
                }
                else if (taxrw["TAXTYPE"].Equals(Constants.CON_TT_TAX_AMOUNT))
                {
                    //TOTTAXAMNT = taxrw["TAXAMOUNT"].ToString();
                    taxrw.Delete();
                }
                else if (taxrw["TAXTYPE"].Equals(Constants.CON_TAX_NETT_RW))
                {
                    //NETAMNT = taxrw["TAXAMOUNT"].ToString();
                    taxrw.Delete();
                }
            }

            if (cppmtaxtb.Columns.Contains("TAXTYPE"))
            {
                cppmtaxtb.Columns.Remove("SEQ");
                cppmtaxtb.Columns.Remove("TAXTYPE");
                cppmtaxtb.Columns.Remove("TAXAPPLIEDFOR");
            }

            cppmtaxtb.AcceptChanges();

            string ORDERCODE = txtOrderCode.Text;
            string GUESTREFCODE = "";
            string TABLECODE = pmtableCode;
            string ORDERENDATE = DateTime.Today.ToString("yyyy-MM-dd HH:mm:ss.fff");
            string LOCDATE = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
            string ORDERSTS = Constants.CON_STATUS_ACTIVE;
            string ORDERDETSTS = Constants.CON_STATUS_ACTIVE;
            string PAYMENTSTS = Constants.CON_STATUS_ACTIVE;

            string[] PAYMENSTSTCODE = cmbpaymenttyp.SelectedValue.ToString().Split('|');
            string PAYMENTCODE = PAYMENSTSTCODE[0].ToString();
            string BILLAMOUNT = txtnetamnt.Text.ToString();
            string PAIDAMOUNT = "";
            if (Convert.ToDouble(BILLAMOUNT) <= Convert.ToDouble(txtamntpaid.Text.ToString()))
            {
                PAIDAMOUNT = txtamntpaid.Text.ToString();
            }
            string DUEAMOUNT = txtbalance.Text.ToString();
            string REMARK = txtremark.Text.ToString();

            if (pmdtsales.Rows.Count > 0 && !PAIDAMOUNT.Equals("") && !PAYMENTCODE.Equals(""))
            {
                ORDERCODE = esalesDataHandler.createOrder(cppmdtsales, cppmtaxtb, COMPCODE, LOCCODE, RESTCODE, ORDERCODE, PAYMENTCODE, GUESTREFCODE, TABLECODE,
                   ORDERENDATE, LOCDATE, ORDERSTS, ORDERDETSTS, PAYMENTSTS, ENUSER, BILLAMOUNT, PAIDAMOUNT, DUEAMOUNT, REMARK, pmkotmenudt, txtdiscount.Text, cmdroomno.SelectedValue.ToString());
                if (ORDERCODE != null || !ORDERCODE.Equals(""))
                {
                    printReceipt(ORDERCODE);
                    Constants.CON_MESSAGE_STRING = "Data updated Successfully";
                    System.Windows.MessageBox.Show(Constants.CON_MESSAGE_STRING, Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                }
                else
                {
                    Constants.CON_MESSAGE_STRING = "Data update failed. Please contact system admin. ";
                    System.Windows.MessageBox.Show(Constants.CON_MESSAGE_STRING, Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                }
            }
            else
            {
                Constants.CON_MESSAGE_STRING = "Please check your fields. Thank you";
                System.Windows.MessageBox.Show(Constants.CON_MESSAGE_STRING, Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
        }

        private void printReceipt(string ORDERCODE)
        {
            ReportSalesDataHandler reportSalesDataHandler = new ReportSalesDataHandler();

            DataTable receiptdt = null;
            DataTable receiptOrderdt = null;
            DataTable receiptTAXdt = null;
            DataTable receiptheaderfooter = null;
            try
            {
                receiptdt = reportSalesDataHandler.getReceiptDetails(ORDERCODE, RESTCODE);
                receiptOrderdt = reportSalesDataHandler.getReceiptOrderDetails(ORDERCODE);
                receiptTAXdt = reportSalesDataHandler.getReceiptTaxDetails(ORDERCODE, RESTCODE);
                receiptheaderfooter = reportSalesDataHandler.getReceiptHeaderFooter(COMPCODE, LOCCODE, RESTCODE);

                rvdetails.LocalReport.DataSources.Clear();
                rvdetails.Reset();
                ReportDataSource ReceiptDetails = new ReportDataSource("ReceiptDetails", receiptdt);
                ReportDataSource ReceiptOrders = new ReportDataSource("ReceiptOrders", receiptOrderdt);
                ReportDataSource ReceiptTaxDetails = new ReportDataSource("ReceiptTaxDetails", receiptTAXdt);

                this.rvdetails.LocalReport.ReportEmbeddedResource = "IRESPOS.Sales.Receipt.rdlc";
                this.rvdetails.LocalReport.DataSources.Clear();
                this.rvdetails.LocalReport.DataSources.Add(ReceiptDetails);
                this.rvdetails.LocalReport.DataSources.Add(ReceiptOrders);
                this.rvdetails.LocalReport.DataSources.Add(ReceiptTaxDetails);

                this.rvdetails.ProcessingMode = ProcessingMode.Local;
                ReportParameter[] param = new ReportParameter[8];
                param[0] = new ReportParameter("paraheader", receiptheaderfooter.Rows[0]["HEADER"].ToString()); ;
                param[1] = new ReportParameter("parafooter", receiptheaderfooter.Rows[0]["FOOTER"].ToString());
                param[2] = new ReportParameter("pararesname", Constants.SYS_SESSION_COMPANYNAME);
                param[3] = new ReportParameter("pararesaddress", Constants.SYS_SESSION_ADDRESS);
                param[4] = new ReportParameter("paratableno", pmtableName);
                param[5] = new ReportParameter("parabillno", ORDERCODE);
                param[6] = new ReportParameter("paradatetime", DateTime.Now.ToString());
                if (receiptdt.Rows[0]["ROOMNO"].ToString() != null || !receiptdt.Rows[0]["ROOMNO"].ToString().Equals(""))
                {
                    param[7] = new ReportParameter("pararoomno", receiptdt.Rows[0]["ROOMNO"].ToString());
                }
                else
                {
                    param[7] = new ReportParameter("pararoomno", "-");
                }
                this.rvdetails.LocalReport.SetParameters(param);
                this.rvdetails.LocalReport.Refresh();
                this.rvdetails.RefreshReport();
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
            finally
            {
                receiptdt.Dispose();
                receiptOrderdt.Dispose();
                receiptTAXdt.Dispose();
            }
        }

        private void Txtamntpaid_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                calcuteBalance();
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
        }

        private void Txtdiscount_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                calcuteBalance();
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
        }

        private void calcuteBalance()
        {
            double balance = 0;
            double discount = 0;

            if (txtdiscount.Text.Any())
            {
                discount = Convert.ToDouble(txtdiscount.Text);
                if (discount > Convert.ToDouble(txtnetamnt.Text))
                {
                    discount = Convert.ToDouble(txtnetamnt.Text);
                    txtdiscount.Text = discount.ToString();
                }
            }
            else
            {
                discount = 0;
                txtdiscount.Text = "";
            }

            if (txtamntpaid.Text.Any())
            {
                balance = Convert.ToDouble(txtamntpaid.Text) - (Convert.ToDouble(txtnetamnt.Text) - discount);
                if (balance >= 0)
                {
                    txtbalance.Text = Math.Round(balance, 2).ToString();
                }
                else
                {
                    txtbalance.Text = "0";
                }
            }
            else
            {
                txtbalance.Text = "0";
            }
        }
    }
}