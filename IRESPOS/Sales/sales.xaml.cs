﻿using BusinessLogicDataHandler.Sales;
using BusinessLogicDataHandler.Tax;
using ConstantsDataHandler;
using System;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace IRESPOS.Sales
{
    public partial class sales : Window
    {
        private DataTable dtsales;
        private DataTable dtsalesOrders;
        private DataTable dtmenuItem = new DataTable();
        private DataTable dtrmtables = new DataTable();
        private DataTable taxTb;
        private DataTable kotmenudt;
        private string COMPCODE;
        private string LOCCODE;
        private string ENUSER;
        private string RESTCODE;
        private System.Windows.Forms.Timer tmr = null;

        public sales()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // init variables
            COMPCODE = Constants.SYS_SESSION_COMPCODE;
            LOCCODE = Constants.SYS_SESSION_LOCCODE;
            ENUSER = Constants.SYS_SESSION_USERNAME;
            RESTCODE = Constants.SYS_SESSION_RESTCODE;

            txtDate.Content = DateTime.Today.ToString("yyyy-MM-dd");
            StartTimer();
            initiatemenudt();
            getMenuCategory();
            getrmTables();
            getPendingOrders();
        }

        //private DataTable getOrderTypes()
        //{
        //    SalesDataHandler eSalesDataHandler = new SalesDataHandler();
        //    DataTable dtorderType = new DataTable();

        //    try
        //    {
        //        dtorderType = eSalesDataHandler.getResOrderTypes(Constants.CON_STATUS_ACTIVE);
        //        return dtorderType;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //        dtorderType.Dispose();
        //        dtorderType = null;
        //        eSalesDataHandler = null;
        //    }

        //}

        private void StartTimer()
        {
            tmr = new System.Windows.Forms.Timer();
            tmr.Interval = 100;
            tmr.Tick += new EventHandler(tmr_Tick);
            tmr.Enabled = true;
        }

        private void tmr_Tick(object sender, EventArgs e)
        {
            txtTime.Content = DateTime.Now.ToLongTimeString();
        }

        private void initiatemenudt()
        {
            dtsales = new DataTable();
            dtsales.Columns.Add("MENUCATCODE", typeof(string));
            dtsales.Columns.Add("MENUCODE", typeof(string));
            dtsales.Columns.Add("MENUNAME", typeof(string));
            dtsales.Columns.Add("QTY", typeof(string));
            dtsales.Columns.Add("RATE", typeof(string));
            dtsales.Columns.Add("ISTAXAPPLY", typeof(string));
            dtsales.Columns.Add("AMOUNT", typeof(double));
            dtsales.PrimaryKey = new DataColumn[] { dtsales.Columns["MENUCODE"] };

            kotmenudt = new DataTable();
            kotmenudt.Columns.Add("MENUCATCODE");
            kotmenudt.Columns.Add("MENUCODE");
            kotmenudt.Columns.Add("STS");

            taxTb = new DataTable();
        }

        private void getMenuCategory()
        {
            SalesDataHandler eSalesDataHandler = new SalesDataHandler();
            DataTable dtmenuCat = new DataTable();

            try
            {
                dtmenuCat = eSalesDataHandler.getMenuCategory(COMPCODE, LOCCODE, RESTCODE, Constants.CON_STATUS_ACTIVE);
                appendDtabletoLview(dtmenuCat, icrmmenucat);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
            finally
            {
                dtmenuCat.Dispose();
                dtmenuCat = null;
                eSalesDataHandler = null;
            }
        }

        private void getrmTables()
        {
            SalesDataHandler eSalesDataHandler = new SalesDataHandler();
            try
            {
                dtrmtables = eSalesDataHandler.getRmTables(COMPCODE, LOCCODE, RESTCODE, Constants.CON_STATUS_ACTIVE);
                //DataTable dtresorderType =  getOrderTypes();
                //dtrmtables.Merge(dtresorderType);
                appendDtabletoLview(dtrmtables, lvrmtables);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
            finally
            {
                dtrmtables.Dispose();
                dtrmtables = null;
                eSalesDataHandler = null;
            }
        }

        private void appendDtabletoLview(DataTable datatable, ListView listview)
        {
            if (datatable.Rows.Count > 0)
            {
                listview.ItemsSource = "";
                listview.ItemsSource = datatable.DefaultView;
            }
        }

        private void getPendingOrders()
        {
            SalesDataHandler eSalesDataHandler = new SalesDataHandler();
            dtsalesOrders = new DataTable();

            try
            {
                dtsalesOrders = eSalesDataHandler.getPendingOrders(COMPCODE, LOCCODE, RESTCODE, Constants.CON_STATUS_PENDING);
                dtsalesOrders.PrimaryKey = new DataColumn[] { dtsalesOrders.Columns["ORDERCODE"] };

                if (dtsalesOrders.Rows.Count > 0)
                {
                    dg_orders.ItemsSource = "";
                    dg_orders.ItemsSource = dtsalesOrders.DefaultView;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
            finally
            {
                eSalesDataHandler = null;
            }
        }

        private void getmenuitems(String rmmenucat)
        {
            SalesDataHandler eOrderDataHandler = new SalesDataHandler();
            try
            {
                dtmenuItem = eOrderDataHandler.getMenu(COMPCODE, LOCCODE, RESTCODE, rmmenucat, Constants.CON_STATUS_ACTIVE);
                dtmenuItem.PrimaryKey = new DataColumn[] { dtmenuItem.Columns["MENUCODE"] };
                appendDtabletoLview(dtmenuItem, icrmmenu);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
            finally
            {
                eOrderDataHandler = null;
            }
        }

        private void txtFilter_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            if (String.IsNullOrEmpty(txtFilter.Text))
            {
                icrmmenu.ItemsSource = "";
                icrmmenu.ItemsSource = dtmenuItem.DefaultView;
            }
            else
            {
                var tblFiltered = dtmenuItem.AsEnumerable().Where(row => row.Field<String>("MENUNAME").ToLower().Contains(txtFilter.Text)
                || row.Field<String>("MENUNAME").Contains(txtFilter.Text)).ToList();

                if (tblFiltered.Any())
                {
                    DataTable dtmenuitemfilter = tblFiltered.CopyToDataTable();
                    icrmmenu.ItemsSource = "";
                    icrmmenu.ItemsSource = dtmenuitemfilter.DefaultView;
                }
                else
                {
                    icrmmenu.ItemsSource = "";
                }
            }
        }

        private void Lv_Menucat_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                DataRowView item = icrmmenucat.SelectedItem as DataRowView;
                if (item != null)
                {
                    String rmmenucat = item.Row["MENUCATCODE"].ToString();
                    menuTabControl.SelectedIndex = 0;
                    getmenuitems(rmmenucat);
                    icrmmenucat.SelectedItems.Clear();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
            finally
            {
            }
        }

        private void Lv_MenuItem_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                DataRowView selectedMenuItemrw = icrmmenu.SelectedItem as DataRowView;

                if (selectedMenuItemrw != null)
                {
                    String MENUCODE = selectedMenuItemrw.Row["MENUCODE"].ToString();
                    String MENUCATCODE = selectedMenuItemrw.Row["MENUCATCODE"].ToString();
                    String RATE = selectedMenuItemrw.Row["RATE"].ToString();
                    String MENUNAME = selectedMenuItemrw.Row["MENUNAME"].ToString();
                    String ISTAXAPPLY = selectedMenuItemrw.Row["ISTAXAPPLY"].ToString();

                    bool itemexist = dtsales.Rows.Contains(MENUCODE);
                    populatedatagd(MENUCODE, MENUCATCODE, MENUNAME, ISTAXAPPLY, RATE, itemexist);
                    icrmmenu.SelectedItems.Clear();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
            finally
            {
            }
        }

        private void populatedatagd(String MENUCODE, string MENUCATCODE, string MENUNAME, string ISTAXAPPLY, string RATE, bool itemexist)
        {
            if (itemexist)
            {
                DataRow menuitemRow = dtsales.Rows.Find(MENUCODE);
                menuitemRow["QTY"] = (Convert.ToDouble(menuitemRow["QTY"].ToString()) + 1).ToString();
                menuitemRow["AMOUNT"] = Convert.ToDouble(String.Format("{0:0.00}", Convert.ToDouble(menuitemRow["QTY"]) * Convert.ToDouble(RATE)));
            }
            else
            {
                DataRow rmmenurw = dtsales.NewRow();
                rmmenurw["MENUCATCODE"] = MENUCATCODE.ToString();
                rmmenurw["MENUCODE"] = MENUCODE.ToString();
                rmmenurw["MENUNAME"] = MENUNAME.ToString();
                rmmenurw["RATE"] = RATE.ToString();
                rmmenurw["ISTAXAPPLY"] = ISTAXAPPLY.ToString();
                rmmenurw["QTY"] = Convert.ToString(1);
                rmmenurw["AMOUNT"] = RATE;

                dtsales.Rows.Add(rmmenurw);
            }

            DataRow kotdtrw = kotmenudt.NewRow();
            kotdtrw["MENUCATCODE"] = MENUCATCODE.ToString();
            kotdtrw["MENUCODE"] = MENUCODE.ToString();
            kotdtrw["STS"] = Constants.CON_STATUS_PENDING;
            kotmenudt.Rows.Add(kotdtrw);
            dtsales.AcceptChanges();
            dtgv.ItemsSource = "";
            dtgv.ItemsSource = dtsales.DefaultView;
            calculatetxtotamnt();
        }

        private void Lvrmtables_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                DataRowView selectedTablerw = lvrmtables.SelectedItem as DataRowView;

                if (selectedTablerw != null)
                {
                    txtTableCode.Text = selectedTablerw.Row["TABLECODE"].ToString();
                    txtTableNo.Text = selectedTablerw.Row["TABLENAME"].ToString();
                    lvrmtables.SelectedItems.Clear();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
            finally
            {
            }
        }

        private void Dg_orders_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SalesDataHandler eOrderDataHandler = new SalesDataHandler();
            try
            {
                DataRowView selectedOrderItemrw = dg_orders.SelectedItem as DataRowView;

                if (selectedOrderItemrw != null)
                {
                    bool itemexist = dtsalesOrders.Rows.Contains(selectedOrderItemrw.Row["ORDERCODE"]);

                    if (itemexist)
                    {
                        DataRow salesorderRow = dtsalesOrders.Rows.Find(selectedOrderItemrw.Row["ORDERCODE"]);
                        txtOrderCodeNo.Text = salesorderRow["ORDERCODE"].ToString();
                        txtTableCode.Text = salesorderRow["TABLECODE"].ToString();
                        txtTableNo.Text = salesorderRow["TABLENAME"].ToString();
                        dtsales = eOrderDataHandler.getMenubyOrder(salesorderRow["ORDERCODE"].ToString(), Constants.CON_STATUS_DELETED);
                        dtsales.PrimaryKey = new DataColumn[] { dtsales.Columns["MENUCODE"] };

                        //dtsales.Columns["Amount"].DataType = typeof(double);
                        if (dtsales.Rows.Count > 0)
                        {
                            dtgv.ItemsSource = "";
                            dtgv.ItemsSource = dtsales.DefaultView;
                            calculatetxtotamnt();
                        }
                    }
                    icrmmenucat.SelectedItems.Clear();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
            finally
            {
                eOrderDataHandler = null;
            }
        }

        private void btn_click_decmenuitemQTY(Object sender, RoutedEventArgs e)
        {
            try
            {
                DataRowView menuitem = dtgv.SelectedItem as DataRowView;
                if (Convert.ToDouble(menuitem["QTY"]) <= 1)
                {
                    menuitem.Delete();
                }
                else
                {
                    menuitem.Row["QTY"] = Convert.ToDouble(menuitem["QTY"]) - 1;
                    menuitem["AMOUNT"] = String.Format("{0:0.00}", Convert.ToDouble(menuitem["QTY"]) * Convert.ToDouble(menuitem.Row["RATE"]));
                }
                for (int i = 0; i < kotmenudt.Rows.Count; i++)
                {
                    if (kotmenudt.Rows[i]["MENUCODE"].Equals(menuitem["MENUCODE"]))
                    {
                        kotmenudt.Rows[i].Delete();
                    }
                }
                kotmenudt.AcceptChanges();
                dtsales.AcceptChanges();
                calculatetxtotamnt();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
            finally
            {
            }
        }

        private void calculatetxtotamnt()
        {
            double totAmntwithtax = 0;
            double totAmntwithouttax = 0;

            if (dtsales != null)
            {
                totAmntwithtax = dtsales.AsEnumerable()
                                         .Where(y => y.Field<string>("ISTAXAPPLY") == Constants.CON_STATUS_TXNOTAPPLIED)
                                         .Sum(x => x.Field<double>("Amount"));

                totAmntwithouttax = dtsales.AsEnumerable()
                    .Where(y => y.Field<string>("ISTAXAPPLY") == Constants.CON_STATUS_TXAPPLIED)
                                   .Sum(x => x.Field<double>("Amount"));

                GLTaxCalculator gLTaxCalculator = new GLTaxCalculator();

                taxTb = gLTaxCalculator.calculateTaxAmount(COMPCODE, LOCCODE, Constants.CON_TAX_RM, totAmntwithouttax, totAmntwithtax);

                dg_menutotaldg.ItemsSource = "";
                dg_menutotaldg.ItemsSource = taxTb.DefaultView;
            }
        }

        private void Button_Click_placeorder(object sender, RoutedEventArgs e)
        {
            SalesDataHandler esalesDataHandler = new SalesDataHandler();
            try
            {
                DataTable cptaxTb = taxTb.Copy();
                DataTable cpdtsales = dtsales.Copy();
                if (cpdtsales.Columns.Contains("MENUNAME"))
                {
                    cpdtsales.Columns.Remove("MENUNAME");
                    cpdtsales.Columns.Remove("ISTAXAPPLY");
                }
                cpdtsales.AcceptChanges();

                String BILLAMOUNT = "0";
                for (int i = cptaxTb.Rows.Count - 1; i >= 0; i--)
                {
                    DataRow taxrw = cptaxTb.Rows[i];

                    if (taxrw["TAXTYPE"].Equals(Constants.CON_TAX_GROSS_TYPE))
                    {
                        taxrw.Delete();
                    }
                    else if (taxrw["TAXTYPE"].Equals(Constants.CON_TT_TAX_AMOUNT))
                    {
                        taxrw.Delete();
                    }
                    else if (taxrw["TAXTYPE"].Equals(Constants.CON_TAX_NETT_RW))
                    {
                        BILLAMOUNT = taxrw["TAXAMOUNT"].ToString();
                        taxrw.Delete();
                    }
                }

                if (cptaxTb.Columns.Contains("TAXTYPE"))
                {
                    cptaxTb.Columns.Remove("SEQ");
                    cptaxTb.Columns.Remove("TAXTYPE");
                    cptaxTb.Columns.Remove("TAXAPPLIEDFOR");
                }

                cptaxTb.AcceptChanges();

                string GUESTREFCODE = "";
                string TABLECODE = txtTableCode.Text;
                string ORDERENDATE = DateTime.Today.ToString("yyyy-MM-dd HH:mm:ss.fff");
                string LOCDATE = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
                string ORDERSTS = Constants.CON_STATUS_PENDING;
                string ORDERDETSTS = Constants.CON_STATUS_ACTIVE;
                string PAYMENTSTS = Constants.CON_STATUS_PENDING;

                string PAYMENTCODE = "";
                if (cptaxTb != null && cpdtsales.Rows.Count > 0 && !txtTableCode.Text.Equals(""))
                {
                    string ORDERCODE = esalesDataHandler.createOrder(cpdtsales, cptaxTb, COMPCODE, LOCCODE, RESTCODE, txtOrderCodeNo.Text, PAYMENTCODE, GUESTREFCODE, TABLECODE,
                        ORDERENDATE, LOCDATE, ORDERSTS, ORDERDETSTS, PAYMENTSTS, ENUSER, BILLAMOUNT, "0", "0", "", kotmenudt, "0", "");
                    if (ORDERCODE != null || !ORDERCODE.Equals(""))
                    {
                        getPendingOrders();
                        clearAll();
                        Constants.CON_MESSAGE_STRING = "Data updated Successfully";
                        MessageBox.Show(Constants.CON_MESSAGE_STRING, Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                    }
                    else
                    {
                        Constants.CON_MESSAGE_STRING = "Data update failed. Please contact system admin. ";
                        MessageBox.Show(Constants.CON_MESSAGE_STRING, Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                    }
                }
                else
                {
                    Constants.CON_MESSAGE_STRING = "Please select menu item, table name before placing the order";
                    MessageBox.Show(Constants.CON_MESSAGE_STRING, Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
        }

        private void Button_Click_Payment(object sender, RoutedEventArgs e)
        {
            try
            {
                if (taxTb != null && dtsales.Rows.Count > 0 && !txtTableCode.Text.Equals(""))
                {
                    salespayment childWindow = new salespayment();
                    if (txtOrderCodeNo.Text.Equals(""))
                    {
                        childWindow.assignData("", taxTb.Copy(), dtsales.Copy(), txtTableCode.Text.ToString(), txtTableNo.Text.ToString(), kotmenudt.Copy());
                    }
                    else
                    {
                        childWindow.assignData(txtOrderCodeNo.Text, taxTb.Copy(), dtsales.Copy(), txtTableCode.Text.ToString(), txtTableNo.Text.ToString(), kotmenudt.Copy());
                    }
                    childWindow.Owner = this;
                    childWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                    childWindow.WindowStyle = WindowStyle.SingleBorderWindow;
                    childWindow.ResizeMode = ResizeMode.NoResize;
                    childWindow.Title = Constants.CON_SYSTEM_NAME + " [" + Constants.CON_OWNER_NAME + "] " + "Sales Payment";
                    childWindow.ShowDialog();
                    menuTabControl.SelectedIndex = 0;
                }
                else
                {
                    Constants.CON_MESSAGE_STRING = "Please check your fields. Thank you";
                    MessageBox.Show(Constants.CON_MESSAGE_STRING, Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
            finally
            {
            }
        }

        private void BarcodeSearch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                searchitmbybarcode();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
            finally
            {
            }
        }

        private void searchitmbybarcode()
        {
            SalesDataHandler eOrderDataHandler = new SalesDataHandler();
            DataTable itemdt = new DataTable();

            DataRowView selectedOrderItemrw = dg_orders.SelectedItem as DataRowView;

            string BARSERMENUCODE = txtbarcode.Text;

            if (!(BARSERMENUCODE.Length > 0))
            {
                return;
            }

            itemdt = eOrderDataHandler.getItemByBarcode(COMPCODE, LOCCODE, RESTCODE, BARSERMENUCODE, Constants.CON_STATUS_ACTIVE);
            if (itemdt.Rows.Count > 0)
            {
                String BRCDMENUCODE = itemdt.Rows[0]["MENUCODE"].ToString();
                String BRCDMENUCATCODE = itemdt.Rows[0]["MENUCATCODE"].ToString();
                String BRCDRATE = itemdt.Rows[0]["RATE"].ToString();
                String BRCDMENUNAME = itemdt.Rows[0]["MENUNAME"].ToString();
                String BRCDISTAXAPPLY = itemdt.Rows[0]["ISTAXAPPLY"].ToString();
                bool itemexist = dtsales.Rows.Contains(BRCDMENUCODE);
                populatedatagd(BRCDMENUCODE, BRCDMENUCATCODE, BRCDMENUNAME, BRCDISTAXAPPLY, BRCDRATE, itemexist);

                txtbarcode.Text = "";
                txtbarcode.Focus();
            }
            else
            {
                MessageBox.Show("No item found !", Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
        }

        private void Button_Click_Clear(object sender, RoutedEventArgs e)
        {
            //dtgv.ItemsSource = "";
            clearAll();
        }

        private void clearAll()
        {
            dtsales.Clear();
            taxTb.Clear();
            kotmenudt.Clear();
            txtTableCode.Text = "";
            txtTableNo.Text = "";
            txtOrderCodeNo.Text = "";
            menuTabControl.SelectedIndex = 0;
        }

        private void Button_Click_Close(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void txtbarcode_TextChanged(object sender, TextChangedEventArgs e)
        {
            searchitmbybarcode();
        }
    }
}