﻿using BusinessLogicDataHandler.Reports.Sales;
using ConstantsDataHandler;
using Microsoft.Reporting.WinForms;
using System;
using System.Data;
using System.Windows;
using System.Windows.Controls;

namespace IRESPOS.Reports.SalesReport
{
    /// <summary>
    /// Interaction logic for Sales.xaml
    /// </summary>
    public partial class Sales : UserControl
    {
        private string selectedReportID;
        private string selectedReportName;
        private string selectedReportPath;

        private string COMPCODE;
        private string LOCCODE;
        private string RESTCODE;

        public Sales()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            COMPCODE = Constants.SYS_SESSION_COMPCODE;
            LOCCODE = Constants.SYS_SESSION_LOCCODE;
            RESTCODE = Constants.SYS_SESSION_RESTCODE;

            loadReports();
        }

        private void loadReports()
        {
            ReportSalesDataHandler salesDataHandler = new ReportSalesDataHandler();
            DataTable dtreporttypes = new DataTable();
            try
            {
                string reportCategory = Constants.REPORT_CATEGORY_SALES;
                dtreporttypes = salesDataHandler.getReportsForCategory(reportCategory, Constants.CON_STATUS_ACTIVE);

                lvreports.ItemsSource = dtreporttypes.DefaultView;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
        }

        private void generateSalesReport(DataTable dtreport, string dateRange)
        {
            try
            {
                rvdetails.LocalReport.DataSources.Clear();
                rvdetails.Reset();
                ReportDataSource rds = new ReportDataSource("Orders", dtreport);

                this.rvdetails.LocalReport.ReportEmbeddedResource = selectedReportPath;
                this.rvdetails.LocalReport.DataSources.Clear();
                this.rvdetails.LocalReport.DataSources.Add(rds);

                this.rvdetails.ProcessingMode = ProcessingMode.Local;
                ReportParameter[] param = new ReportParameter[5];
                param[0] = new ReportParameter("daterange", dateRange);
                param[1] = new ReportParameter("username", Constants.SYS_SESSION_USERNAME);
                param[2] = new ReportParameter("headercompany", Constants.SYS_SESSION_COMPANYNAME);
                param[3] = new ReportParameter("footercompany", Constants.CON_OWNER_NAME);
                param[4] = new ReportParameter("headertitle", selectedReportName);
                this.rvdetails.LocalReport.SetParameters(param);
                this.rvdetails.LocalReport.Refresh();
                this.rvdetails.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ReportSalesDataHandler eSalesDataHandler = new ReportSalesDataHandler();
            DataTable dtreport = new DataTable();
            string dateRange;

            try
            {
                // check if report is selected
                if (lvreports.SelectedIndex < 0)
                {
                    MessageBox.Show("Please select a report from the list first !", Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                    return;
                }

                if (datefrom.SelectedDate != null && dateto.SelectedDate != null)
                {
                    // check if the from date is less than to date
                    if (DateTime.Compare(datefrom.SelectedDate.Value.Date, dateto.SelectedDate.Value.Date) > 0)
                    {
                        MessageBox.Show("Starting date cannot be after the Ending Date !", Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                        return;
                    }
                    else
                    {
                        string fromDate = Convert.ToDateTime(datefrom.Text).ToString("yyyy/MM/dd");
                        string toDate = Convert.ToDateTime(dateto.Text).ToString("yyyy/MM/dd");
                        dateRange = fromDate + " - " + toDate;

                        // generate the datatable based on the selected report id
                        switch (selectedReportID)
                        {
                            case Constants.REPORT_SALES:
                                dtreport = eSalesDataHandler.getOrdersFiltered(COMPCODE, LOCCODE, RESTCODE, fromDate, toDate, Constants.CON_STATUS_ACTIVE);
                                break;

                            default: return;
                        }
                    }
                }
                else
                {
                    dateRange = "All Time";

                    // generate the datatable based on the selected report id
                    switch (selectedReportID)
                    {
                        case Constants.REPORT_SALES:
                            dtreport = eSalesDataHandler.getOrders(COMPCODE, LOCCODE, RESTCODE, Constants.CON_STATUS_ACTIVE);
                            break;

                        default: return;
                    }
                }

                // use the data to generate the report and display
                generateSalesReport(dtreport, dateRange);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
        }

        private void lv_report_selected(object sender, RoutedEventArgs e)
        {
            try
            {
                DataRowView item = lvreports.SelectedItem as DataRowView;
                selectedReportID = item.Row["REPORTID"].ToString();
                selectedReportName = item.Row["REPORTNAME"].ToString();
                selectedReportPath = item.Row["PATH"].ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
        }
    }
}