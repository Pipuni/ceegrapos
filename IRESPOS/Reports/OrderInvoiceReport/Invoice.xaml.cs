﻿using BusinessLogicDataHandler.Reports.Invoice;
using ConstantsDataHandler;
using Microsoft.Reporting.WinForms;
using System;
using System.Data;
using System.Windows;
using System.Windows.Controls;

namespace IRESPOS.Reports.OrderInvoiceReport
{
    /// <summary>
    /// Interaction logic for Invoice.xaml
    /// </summary>
    public partial class Invoice : UserControl
    {
        public Invoice()
        {
            InitializeComponent();
        }

        private void generateInvoiceReport()
        {
            InvoiceDataHandler invoiceDataHandler = new InvoiceDataHandler();
            DataTable dtreport = new DataTable();

            try
            {
                string COMPCODE = Constants.SYS_SESSION_COMPCODE;
                string LOCCODE = Constants.SYS_SESSION_LOCCODE;
                string RESTCODE = Constants.SYS_SESSION_RESTCODE;
                string ordercode = txt_ordercode.Text.ToString().Trim();
                string orderdate = "";
                string ordertable = "";
                string billamount = "";
                string paidamount = "";
                string dueamount = "";
                rvdetails.LocalReport.DataSources.Clear();
                rvdetails.Reset();

                dtreport = invoiceDataHandler.getOrderDetails(COMPCODE, LOCCODE, RESTCODE, ordercode, Constants.CON_STATUS_ACTIVE);
                if (dtreport.Rows.Count > 0)
                {
                    orderdate = dtreport.Rows[0]["ORDERENDATE"].ToString();
                    ordertable = dtreport.Rows[0]["TABLENAME"].ToString();
                    billamount = dtreport.Rows[0]["BILLMOUNT"].ToString();
                    paidamount = dtreport.Rows[0]["PAIDAMOUNT"].ToString();
                    dueamount = dtreport.Rows[0]["DUEAMOUNT"].ToString();
                }
                else
                {
                    MessageBox.Show("No data found for the given order code !", Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                    return;
                }
                ReportDataSource rds = new ReportDataSource("OrderInvoice", dtreport);

                this.rvdetails.LocalReport.ReportEmbeddedResource = "IRESPOS.Reports.OrderInvoiceReport.OrderInvoice.rdlc";
                this.rvdetails.LocalReport.DataSources.Clear();
                this.rvdetails.LocalReport.DataSources.Add(rds);

                this.rvdetails.ProcessingMode = ProcessingMode.Local;
                ReportParameter[] param = new ReportParameter[10];
                param[0] = new ReportParameter("ordercode", ordercode);
                param[1] = new ReportParameter("orderdate", orderdate);
                param[2] = new ReportParameter("ordertable", ordertable);
                param[3] = new ReportParameter("username", Constants.SYS_SESSION_USERNAME);
                param[4] = new ReportParameter("headercompany", Constants.SYS_SESSION_COMPANYNAME);
                param[5] = new ReportParameter("footercompany", Constants.CON_OWNER_NAME);
                param[6] = new ReportParameter("headertitle", "Order Invoice");
                param[7] = new ReportParameter("billamount", billamount);
                param[8] = new ReportParameter("paidamount", paidamount);
                param[9] = new ReportParameter("dueamount", dueamount);
                this.rvdetails.LocalReport.SetParameters(param);
                this.rvdetails.LocalReport.Refresh();
                this.rvdetails.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (txt_ordercode.Text.Trim().Length > 0)
                {
                    generateInvoiceReport();
                }
                else
                {
                    MessageBox.Show("Please enter the order code !", Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
        }
    }
}