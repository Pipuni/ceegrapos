﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace IRESPOS.StockList
{
    /// <summary>
    /// Interaction logic for stocklist.xaml
    /// </summary>
    public partial class stocklist : Window
    {
        public stocklist()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            loadFirstView();
            txtDateTime.Content = DateTime.Now.ToString();
        }

        private void loadFirstView()
        {
            UserControl userControl = new Stockitemlist();
            GridDisplay.Children.Add(userControl);
        }

        private void Tab_Switched(object sender, RoutedEventArgs e)
        {
            UserControl userControl = null;
            GridDisplay.Children.Clear();

            int index = int.Parse(((Button)e.Source).Uid);

            GridCursor.Margin = new Thickness(10 + (150 * index), 0, 0, 0);

            switch (index)
            {
                case 0:
                    userControl = new Stockitemlist();
                    GridDisplay.Children.Add(userControl);
                    break;
            }
        }

        private void Button_Click_Close(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}