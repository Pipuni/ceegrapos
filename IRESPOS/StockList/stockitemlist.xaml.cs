﻿using BusinessLogicDataHandler.Stock;
using ConstantsDataHandler;
using System;
using System.Data;
using System.Windows;
using System.Windows.Controls;

namespace IRESPOS.StockList
{
    /// <summary>
    /// Interaction logic for stockitemlist.xaml
    /// </summary>
    public partial class Stockitemlist : UserControl
    {
        private string COMPCODE;
        private string LOCCODE;
        private string RESTCODE;

        public Stockitemlist()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            COMPCODE = Constants.SYS_SESSION_COMPCODE;
            LOCCODE = Constants.SYS_SESSION_LOCCODE;
            RESTCODE = Constants.SYS_SESSION_RESTCODE;
            getmenuitems();
        }

        private void getmenuitems()
        {
            StockDataHandler eOrderDataHandler = new StockDataHandler();
            DataTable dtmenuItem = new DataTable();

            try
            {
                dtmenuItem = eOrderDataHandler.getMenu(COMPCODE, LOCCODE, RESTCODE, Constants.CON_STATUS_ACTIVE);
                if (dtmenuItem.Rows.Count > 0)
                {
                    Skrmmenu.ItemsSource = dtmenuItem.DefaultView;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
            finally
            {
                eOrderDataHandler = null;
            }
        }

        private void Skrmmenu_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
        }
    }
}