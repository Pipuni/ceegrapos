using BusinessLogicDataHandler.Settings;
using ConstantsDataHandler;
using System;
using System.Data;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace IRESPOS.Settings
{
    /// <summary>
    /// Interaction logic for itemcategory.xaml
    /// </summary>
    public partial class itemcategory : UserControl
    {
        private string COMPCODE;
        private string LOCCODE;
        private string RESTCODE;

        public itemcategory()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            COMPCODE = Constants.SYS_SESSION_COMPCODE;
            LOCCODE = Constants.SYS_SESSION_LOCCODE;
            RESTCODE = Constants.SYS_SESSION_RESTCODE;

            loadMenuCategories();
        }

        private void loadMenuCategories()
        {
            SettingsDataHandler settingsDataHandler = new SettingsDataHandler();

            try
            {
                string sts = Constants.CON_STATUS_ACTIVE;

                dtcat.ItemsSource = settingsDataHandler.getMenuCategory(COMPCODE, LOCCODE, RESTCODE, sts).DefaultView;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
            finally
            {
                settingsDataHandler = null;
            }
        }

        private void clearAndRefresh()
        {
            lblmenucatcode.Text = "";
            txtcatname.Text = "";
            btn_color.Background = Brushes.White;
            loadMenuCategories();
        }

        private void openColorPicker(object sender, RoutedEventArgs e)
        {
            try
            {
                System.Windows.Forms.ColorDialog colDiag = new System.Windows.Forms.ColorDialog();
                if (colDiag.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    //btn_color.Content = "#" + (colDiag.Color.ToArgb() & 0x00FFFFFF).ToString("X6");
                    btn_color.Background = new SolidColorBrush(Color.FromArgb(colDiag.Color.A, colDiag.Color.R, colDiag.Color.G, colDiag.Color.B));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
        }

        private string convertBrushToRGBString(Brush brush)
        {
            Color color = (brush as SolidColorBrush).Color;
            return string.Format("#{0:X2}{1:X2}{2:X2}", color.R, color.G, color.B);
        }

        private Boolean validateFields()
        {
            if (txtcatname.Text.Length > 0) /*&& ( btn_color.) )*/
            {
                return true;
            }
            return false;
        }

        private void btn_edit(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dtcat.SelectedIndex > -1)
                {
                    DataRowView row = (DataRowView)dtcat.SelectedItems[0];
                    lblmenucatcode.Text = row["MENUCATCODE"].ToString();
                    txtcatname.Text = row["MENUCATNAME"].ToString();
                    string colorcode = row["COLORCODE"].ToString();
                    btn_color.Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString(colorcode));
                }
                else
                {
                    MessageBox.Show("Please select a row to continue !", Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
        }

        private void btn_save(object sender, RoutedEventArgs e)
        {
            SettingsDataHandler settingsDataHandler = new SettingsDataHandler();
            try
            {

                string compcode = Constants.SYS_SESSION_COMPCODE;
                string loccode = Constants.SYS_SESSION_LOCCODE;
                string restcode = Constants.SYS_SESSION_RESTCODE;
                string menucatcode = lblmenucatcode.Text.ToString().Trim();
                string menucatname = txtcatname.Text.ToString().Trim();
                string colorcode = convertBrushToRGBString(btn_color.Background);
                string locdate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
                string sts = Constants.CON_STATUS_ACTIVE;

                if (!validateFields())
                {
                    MessageBox.Show("Please fill in all fields !", Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                    return;
                }else
                {
                    DataRow drt = settingsDataHandler.getMenuCategory(COMPCODE, loccode, restcode, sts, menucatname);
                    if (drt != null)
                    {
                        MessageBox.Show(menucatname + "is already available.", Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                        return;
                    }
                    else
                    {
                        Boolean iscreated = settingsDataHandler.CreateRmMenuCategory(compcode, loccode, RESTCODE, menucatcode, menucatname, colorcode, locdate, sts);
                        if (iscreated)
                        {
                            MessageBox.Show("Category Successfully Saved !", Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                            clearAndRefresh();
                        }
                        else
                        {
                            Constants.CON_MESSAGE_STRING = "Data update failed. Please contact sytem admin. ";
                            MessageBox.Show(Constants.CON_MESSAGE_STRING, Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                        }
                    }
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
        }

        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                clearAndRefresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
        }

        private void txtcatname_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void txtcatname_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(e.Text, "[^a-zA-Z]+"))
            {
                e.Handled = true;
            }
        }
    }
}