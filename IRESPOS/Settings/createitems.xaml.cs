﻿using BusinessLogicDataHandler.Referance;
using ConstantsDataHandler;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace IRESPOS.Settings
{
    /// <summary>
    /// Interaction logic for createitems.xaml
    /// </summary>
    public partial class Createitems : UserControl
    {
        private string COMPCODE;
        private string LOCCODE;
        private string RESTCODE;
        private DataTable dttb = new DataTable();

        public Createitems()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            COMPCODE = Constants.SYS_SESSION_COMPCODE;
            LOCCODE = Constants.SYS_SESSION_LOCCODE;
            RESTCODE = Constants.SYS_SESSION_RESTCODE;

            GetParameters();
            getMenuCategory();
            getItems();
            btndelete.Visibility = Visibility.Collapsed;
        }

        private void getItems()
        {
            ReferanceDataHandler eReferanceDataHandler = new ReferanceDataHandler();

            try
            {
                string sts = Constants.CON_STATUS_ACTIVE;

                dttb = eReferanceDataHandler.getMenu(COMPCODE, LOCCODE, RESTCODE, sts);
                dttb.PrimaryKey = new DataColumn[] { dttb.Columns["MENUCODE"] };
                dtgv.ItemsSource = dttb.DefaultView;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
            finally
            {
                eReferanceDataHandler = null;
            }
        }

        private void getMenuCategory()
        {
            ReferanceDataHandler eReferanceDataHandler = new ReferanceDataHandler();
            DataTable dtdis = new DataTable();
            cmbcategory.Items.Clear();

            try
            {
                dtdis = eReferanceDataHandler.getMenuCategory(COMPCODE, LOCCODE, RESTCODE, Constants.CON_STATUS_ACTIVE);
                cmbcategory.ItemsSource = dtdis.DefaultView;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
            finally
            {
                eReferanceDataHandler = null;
            }
        }

        private void Btnupload_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog op = new OpenFileDialog();
            op.Title = "Select a picture";
            op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
              "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
              "Portable Network Graphic (*.png)|*.png";
            if (op.ShowDialog() == true)
            {
                imgitem.Source = new BitmapImage(new Uri(op.FileName));
            }
        }

        private void GetParameters()
        {
            ReferanceDataHandler eReferanceDataHandler = new ReferanceDataHandler();

            try
            {
                Dictionary<string, string> getYesNo = new Dictionary<string, string>();
                getYesNo = eReferanceDataHandler.GetYesNo();
                cmbkitchenitem.ItemsSource = getYesNo;
                cmbtaxapplied.ItemsSource = getYesNo;
                cmbtaxcode.ItemsSource = getYesNo;

                Dictionary<string, string> getUnits = new Dictionary<string, string>();
                getUnits = eReferanceDataHandler.GetUnits();
                cmbmunits.ItemsSource = getUnits;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
            finally
            {
                eReferanceDataHandler = null;
            }
        }

        private Boolean validateFields()
        {
            if (cmbcategory.SelectedIndex > -1 && txtitemname.Text.Length > 0
                && txtsaleprice.Text.Length > 0 && cmbtaxapplied.Text.Length > 0 && cmbtaxcode.SelectedIndex > -1
                && cmbkitchenitem.SelectedIndex > -1 && cmbmunits.SelectedIndex > -1)
            {
                return true;
            }
            return false;
        }

        private void clearFields()
        {
            txtitemname.Text = "";
            txtmenucode.Text = "";
            //txtbarcode.Text = menuitemRow["BARCODE"].ToString();
            txtdescrip.Text = "";
            txtsaleprice.Text = "";
            cmbcategory.SelectedValue = "";
            cmbtaxapplied.SelectedValue = "";
            cmbkitchenitem.SelectedValue = "";
            cmbtaxcode.SelectedValue = "";
            cmbmunits.SelectedValue = "";
            imgitem.Source = null;
            btnsave.Content = "Save";
            btndelete.Visibility = Visibility.Collapsed;
        }

        private void Btnsave_Click(object sender, RoutedEventArgs e)
        {
            ReferanceDataHandler eReferanceDataHandler = new ReferanceDataHandler();

            try
            {
                if (!validateFields())
                {
                    MessageBox.Show("Please fill in all fields !", Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                    return;
                }

                string MENUCATCODE = cmbcategory.SelectedValue.ToString();
                string MENUCODE = txtmenucode.Text.ToString();
                string MENUNAME = txtitemname.Text.ToString();
                string BARCODE = txtbarcode.Text.ToString();
                string RATE = txtsaleprice.Text.ToString();
                string DISCOUNT = "0";
                string ISTAXAPPLY = cmbtaxapplied.SelectedValue.ToString();
                string TAXCODE = cmbtaxcode.SelectedValue.ToString();
                string KITCHENITEM = cmbkitchenitem.SelectedValue.ToString();
                string MEASURINGUNIT = cmbmunits.SelectedValue.ToString();
                string IMAGEPATH = imgitem.Source.ToString();
                string STS = Constants.CON_STATUS_ACTIVE;
                string LOCDATE = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");

                Boolean isupdate = eReferanceDataHandler.CreateRmMenu(COMPCODE, LOCCODE, RESTCODE, MENUCATCODE, MENUCODE, MENUNAME, BARCODE, RATE, DISCOUNT, ISTAXAPPLY, TAXCODE, KITCHENITEM, MEASURINGUNIT, IMAGEPATH, STS, LOCDATE);
                if (isupdate)
                {
                    MessageBox.Show("Item Details Successfullt Updated", Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Asterisk, MessageBoxResult.OK);
                    clearFields();
                    getItems();
                }
                else
                {
                    MessageBox.Show("Can not update details. Please contact Administrator.", Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
            finally
            {
                eReferanceDataHandler = null;
            }
        }

        private void Dtgv_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                DataRowView selectedMenuItemrw = dtgv.SelectedItem as DataRowView;

                if (selectedMenuItemrw != null)
                {
                    bool itemexist = dttb.Rows.Contains(selectedMenuItemrw.Row["MENUCODE"]);

                    if (itemexist)
                    {
                        btnsave.Content = "Modify";
                        btndelete.Visibility = Visibility.Visible;
                        DataRow menuitemRow = dttb.Rows.Find(selectedMenuItemrw.Row["MENUCODE"]);
                        txtitemname.Text = menuitemRow["MENUNAME"].ToString();
                        txtmenucode.Text = menuitemRow["MENUCODE"].ToString();
                        txtbarcode.Text = menuitemRow["BARCODE"].ToString();
                        //txtdescrip.Text = menuitemRow["BARCODE"].ToString();
                        txtsaleprice.Text = menuitemRow["RATE"].ToString();
                        cmbcategory.SelectedValue = menuitemRow["MENUCATCODE"].ToString();
                        cmbtaxapplied.SelectedValue = menuitemRow["ISTAXAPPLY"].ToString();
                        cmbkitchenitem.SelectedValue = menuitemRow["KITCHENITEM"].ToString();
                        cmbtaxcode.SelectedValue = menuitemRow["TAXCODE"].ToString();
                        cmbmunits.SelectedValue = menuitemRow["MEASURINGUNIT"].ToString();
                        imgitem.Source = new BitmapImage(new Uri(menuitemRow["IMAGEPATH"].ToString()));
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
            finally
            {
                dtgv.SelectedItems.Clear();
            }
        }

        private void Btncancel_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                clearFields();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
        }

        private void BtnDeleteItem_click(object sender, RoutedEventArgs e)
        {
            ReferanceDataHandler eReferanceDataHandler = new ReferanceDataHandler();

            try
            {
                if (txtmenucode.Text.Any())
                {
                    DataRow menuitemRow = dttb.Rows.Find(txtmenucode.Text.ToString());

                    string MENUCATCODE = menuitemRow["MENUCATCODE"].ToString();
                    string MENUCODE = menuitemRow["MENUCODE"].ToString();
                    string MENUNAME = menuitemRow["MENUNAME"].ToString();
                    string BARCODE = menuitemRow["BARCODE"].ToString();
                    string RATE = menuitemRow["RATE"].ToString();
                    string DISCOUNT = menuitemRow["DISCOUNT"].ToString();
                    string ISTAXAPPY = menuitemRow["ISTAXAPPLY"].ToString();
                    string TAXCODE = menuitemRow["TAXCODE"].ToString();
                    string KITCHENITEM = menuitemRow["KITCHENITEM"].ToString();
                    string MEASURINGUNIT = menuitemRow["MEASURINGUNIT"].ToString();
                    string IMAGEPATH = menuitemRow["IMAGEPATH"].ToString();
                    string STS = Constants.CON_STATUS_DELETED;
                    string LOCDATE = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");

                    Boolean isupdate = eReferanceDataHandler.CreateRmMenu(COMPCODE, LOCCODE, RESTCODE, MENUCATCODE, MENUCODE, MENUNAME, BARCODE, RATE, DISCOUNT, ISTAXAPPY, TAXCODE, KITCHENITEM, MEASURINGUNIT, IMAGEPATH, STS, LOCDATE);
                    if (isupdate)
                    {
                        MessageBox.Show("Item Details Successfully Deleted", Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Asterisk, MessageBoxResult.OK);
                        clearFields();
                        getItems();
                    }
                    else
                    {
                        MessageBox.Show("Can not delete details. Please contact Administrator.", Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
            finally
            {
                eReferanceDataHandler = null;
                dtgv.SelectedItems.Clear();
            }
        }

        private void txtbarcode_TextChanged(object sender, TextChangedEventArgs e)
        {
            //if (System.Text.RegularExpressions.Regex.IsMatch(txtbarcode.Text, "  ^ [0-9]"))
            //{
            //    txtbarcode.Text = "";
            //}
        }

        private void txtitemname_KeyUp(object sender, KeyEventArgs e)
        {
            //            txtitemname.Text = txtitemname.Text;
            //e.Handled = true;
            //if (System.Text.RegularExpressions.Regex.IsMatch(txtitemname., " ^[a-zA-Z]"))
            //{
            //    txtitemname.Text = "";
            //}

            //if (System.Text.RegularExpressions.Regex.IsMatch(e.Text, "^[0-9]+"))
            //{
            //    e.Handled = true;
            //}
        }

        private void txtitemname_KeyDown(object sender, KeyEventArgs e)
        {
        }

        private void txtitemname_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(e.Text, "[^a-zA-Z]+"))
            {
                e.Handled = true;
            }


        }

        private void txtsaleprice_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(e.Text, "[^0-9]"))
            {
                e.Handled = true;
            }
        }
    }
}