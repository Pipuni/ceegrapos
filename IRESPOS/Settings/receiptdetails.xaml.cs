﻿using BusinessLogicDataHandler.Settings;
using ConstantsDataHandler;
using System;
using System.Data;
using System.Windows;
using System.Windows.Controls;

namespace IRESPOS.Settings
{
    /// <summary>
    /// Interaction logic for invoiceheader.xaml
    /// </summary>
    public partial class receiptheader : UserControl
    {
        private string COMPCODE;
        private string LOCCODE;
        private string RESTCODE;
        private string ENUSER;
        private DataTable dttb = new DataTable();

        public receiptheader()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            COMPCODE = Constants.SYS_SESSION_COMPCODE;
            LOCCODE = Constants.SYS_SESSION_LOCCODE;
            RESTCODE = Constants.SYS_SESSION_RESTCODE;
            ENUSER = Constants.SYS_SESSION_USERNAME;
            loadreceiptdetails();
        }

        private void loadreceiptdetails()
        {
            SettingsDataHandler settingsDataHandler = new SettingsDataHandler();

            try
            {
                dttb = settingsDataHandler.getreceiptdetails(COMPCODE, LOCCODE, RESTCODE);

                dtreceiptdtgv.ItemsSource = dttb.DefaultView;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
            finally
            {
                settingsDataHandler = null;
            }
        }

        private void Btnsave_Click(object sender, RoutedEventArgs e)
        {
            SettingsDataHandler settingsDataHandler = new SettingsDataHandler();

            try
            {
                string header = txtBxheader.Text;
                string footer = txtBxfooter.Text;
                string RECEIPTDETCODE = txtBxreceiptcode.Text;
                string sts = Constants.CON_STATUS_ACTIVE;
                string locdate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");

                Boolean iscreate = settingsDataHandler.CreateReceiptDetails(RECEIPTDETCODE, COMPCODE, LOCCODE, RESTCODE, header, footer, sts, ENUSER, locdate);
                if (iscreate)
                {
                    clearAll();
                    loadreceiptdetails();
                    Constants.CON_MESSAGE_STRING = "Receipt details created successfully !";
                    MessageBox.Show(Constants.CON_MESSAGE_STRING, Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                }
                else
                {
                    Constants.CON_MESSAGE_STRING = "Data update failed. Please contact sytem admin. ";
                    MessageBox.Show(Constants.CON_MESSAGE_STRING, Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
            finally
            {
                settingsDataHandler = null;
            }
        }

        private void clearAll()
        {
            txtBxfooter.Text = "";
            txtBxheader.Text = "";
            btnsave.Content = "Save";
        }

        private void Btncancel_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                clearAll();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
            finally
            {
            }
        }

        private void dtreceiptdtgv_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                DataRowView selectedMenuItemrw = dtreceiptdtgv.SelectedItem as DataRowView;

                if (selectedMenuItemrw != null)
                {
                    btnsave.Content = "Modify";
                    txtBxheader.Text = selectedMenuItemrw["HEADER"].ToString();
                    txtBxfooter.Text = selectedMenuItemrw["FOOTER"].ToString();
                    txtBxreceiptcode.Text = selectedMenuItemrw["RECEIPTDETCODE"].ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), Constants.CON_OWNER_NAME, MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
            finally
            {
                dtreceiptdtgv.SelectedItems.Clear();
            }
        }
    }
}