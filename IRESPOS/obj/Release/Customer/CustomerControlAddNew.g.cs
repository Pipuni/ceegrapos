﻿#pragma checksum "..\..\..\Customer\CustomerControlAddNew.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "B9370FE5CBB5CF81C3E9B72BDA79D034A89B6C69E1D42B54353D69E0C2725F2D"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using IRESPOS.Customer;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace IRESPOS.Customer {
    
    
    /// <summary>
    /// CustomerControlAddNew
    /// </summary>
    public partial class CustomerControlAddNew : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 43 "..\..\..\Customer\CustomerControlAddNew.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid UserTypeCursor;
        
        #line default
        #line hidden
        
        
        #line 50 "..\..\..\Customer\CustomerControlAddNew.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt_nicpp;
        
        #line default
        #line hidden
        
        
        #line 53 "..\..\..\Customer\CustomerControlAddNew.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox combo_title;
        
        #line default
        #line hidden
        
        
        #line 56 "..\..\..\Customer\CustomerControlAddNew.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt_fname;
        
        #line default
        #line hidden
        
        
        #line 59 "..\..\..\Customer\CustomerControlAddNew.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt_lname;
        
        #line default
        #line hidden
        
        
        #line 62 "..\..\..\Customer\CustomerControlAddNew.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt_email;
        
        #line default
        #line hidden
        
        
        #line 68 "..\..\..\Customer\CustomerControlAddNew.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lbl_guesttype;
        
        #line default
        #line hidden
        
        
        #line 69 "..\..\..\Customer\CustomerControlAddNew.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox combo_guesttype;
        
        #line default
        #line hidden
        
        
        #line 71 "..\..\..\Customer\CustomerControlAddNew.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lbl_clienttype;
        
        #line default
        #line hidden
        
        
        #line 72 "..\..\..\Customer\CustomerControlAddNew.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox combo_clienttype;
        
        #line default
        #line hidden
        
        
        #line 74 "..\..\..\Customer\CustomerControlAddNew.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lbl_vatno;
        
        #line default
        #line hidden
        
        
        #line 75 "..\..\..\Customer\CustomerControlAddNew.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt_vatno;
        
        #line default
        #line hidden
        
        
        #line 78 "..\..\..\Customer\CustomerControlAddNew.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt_contactno;
        
        #line default
        #line hidden
        
        
        #line 81 "..\..\..\Customer\CustomerControlAddNew.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox combo_country;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/IRESPOS;component/customer/customercontroladdnew.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Customer\CustomerControlAddNew.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 8 "..\..\..\Customer\CustomerControlAddNew.xaml"
            ((IRESPOS.Customer.CustomerControlAddNew)(target)).Loaded += new System.Windows.RoutedEventHandler(this.UserControl_Loaded);
            
            #line default
            #line hidden
            return;
            case 2:
            
            #line 28 "..\..\..\Customer\CustomerControlAddNew.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Btn_UserTypeSelect);
            
            #line default
            #line hidden
            return;
            case 3:
            
            #line 35 "..\..\..\Customer\CustomerControlAddNew.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Btn_UserTypeSelect);
            
            #line default
            #line hidden
            return;
            case 4:
            this.UserTypeCursor = ((System.Windows.Controls.Grid)(target));
            return;
            case 5:
            this.txt_nicpp = ((System.Windows.Controls.TextBox)(target));
            return;
            case 6:
            this.combo_title = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 7:
            this.txt_fname = ((System.Windows.Controls.TextBox)(target));
            return;
            case 8:
            this.txt_lname = ((System.Windows.Controls.TextBox)(target));
            return;
            case 9:
            this.txt_email = ((System.Windows.Controls.TextBox)(target));
            return;
            case 10:
            this.lbl_guesttype = ((System.Windows.Controls.Label)(target));
            return;
            case 11:
            this.combo_guesttype = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 12:
            this.lbl_clienttype = ((System.Windows.Controls.Label)(target));
            return;
            case 13:
            this.combo_clienttype = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 14:
            this.lbl_vatno = ((System.Windows.Controls.Label)(target));
            return;
            case 15:
            this.txt_vatno = ((System.Windows.Controls.TextBox)(target));
            return;
            case 16:
            this.txt_contactno = ((System.Windows.Controls.TextBox)(target));
            return;
            case 17:
            this.combo_country = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 18:
            
            #line 86 "..\..\..\Customer\CustomerControlAddNew.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Btn_Confirm);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

