﻿using BusinessLogicDataHandler.Clog;
using ConnectionDataHandler;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace BusinessLogicDataHandler.Referance
{
    public class ReferanceDataHandler 
    {
        public Dictionary<string, string> GetYesNo()
        {
            Dictionary<string, string> getYesNo = new Dictionary<string, string>();
            getYesNo.Add("", "");
            getYesNo.Add("Yes", "Y");
            getYesNo.Add("No", "N");
            return getYesNo;
        }

        public Dictionary<string, string> GetUnits()
        {
            Dictionary<string, string> getUnits = new Dictionary<string, string>();
            getUnits.Add("", "");
            getUnits.Add("Kg", "K");
            getUnits.Add("Lt", "L");
            getUnits.Add("Units", "unit");
            return getUnits;
        }

        public DataTable getMenuCategory(string COMPCODE, string LOCCODE, string RESTCODE, string STS)
        {

            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @" SELECT MENUCATCODE,MENUCATNAME FROM RMMENU_CATEGORY WHERE COMPCODE= @compcode AND LOCCODE = @LOCCODE AND RESTCODE =@RESTCODE  AND  STS=@STS order by MENUCATCODE";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE);
                            SQLCommand.Parameters.AddWithValue("@LOCCODE", LOCCODE);
                            SQLCommand.Parameters.AddWithValue("@RESTCODE", RESTCODE);
                            SQLCommand.Parameters.AddWithValue("@STS", STS);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }

                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public Boolean CreateRmMenuCategory(string COMPCODE, string LOCCODE, string RESTCODE, string MENUCATCODE, string MENUCATNAME, string COLORCODE, string STS)
        {
            Boolean isupdate = false;

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    using (SqlCommand SQLCommand = new SqlCommand("sp_rmcreatemenucategory", con))
                    {
                        SQLCommand.CommandType = CommandType.StoredProcedure;
                        SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@LOCCODE", LOCCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@RESTCODE", RESTCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@MENUCATCODE", MENUCATCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@MENUCATNAME", MENUCATNAME.Trim());
                        SQLCommand.Parameters.AddWithValue("@COLORCODE", COLORCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@STS", STS.Trim());
                        SQLCommand.ExecuteNonQuery();
                        SQLCommand.Parameters.Clear();

                        isupdate = true;
                    }
                }

                return isupdate;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataTable getMenu(string COMPCODE, string LOCCODE, string RESTCODE, string MENUCATCODE, string STS)
        {

            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @" SELECT RM.COMPCODE AS COMPCODE,RM.LOCCODE AS LOCCODE,RM.MENUCATCODE AS MENUCATCODE,MENUCODE,MENUNAME,RATE,COLORCODE,ISTAXAPPLY,TAXCODE,KITCHENITEM,MEASURINGUNIT,IMAGEPATH,RM.STS AS STS FROM RMMENUS RM " +
                                   " INNER JOIN RMMENU_CATEGORY RC ON RM.MENUCATCODE = RC.MENUCATCODE WHERE RM.COMPCODE= @compcode AND RM.LOCCODE = @loccode AND RM.RESTCODE = @RESTCODE AND  RM.MENUCATCODE = @MENUCATCODE AND  RM.STS=@STS order by MENUNAME";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE);
                            SQLCommand.Parameters.AddWithValue("@LOCCODE", LOCCODE);
                            SQLCommand.Parameters.AddWithValue("@RESTCODE", RESTCODE);
                            SQLCommand.Parameters.AddWithValue("@MENUCATCODE", MENUCATCODE);
                            SQLCommand.Parameters.AddWithValue("@STS", STS);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }
                
                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataTable getMenu(string COMPCODE, string LOCCODE, string RESTCODE, string STS)
        {

            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @" SELECT RM.COMPCODE AS COMPCODE,RM.LOCCODE AS LOCCODE,RM.MENUCATCODE AS MENUCATCODE,MENUCODE,RM.BARCODE,MENUNAME,RATE,DISCOUNT,COLORCODE, " +
                    " ISTAXAPPLY,TAXCODE,KITCHENITEM,MEASURINGUNIT,IMAGEPATH,RM.STS AS STS " +
                    " FROM RMMENUS RM INNER JOIN RMMENU_CATEGORY RC ON RM.MENUCATCODE = RC.MENUCATCODE " +
                    " WHERE RM.COMPCODE= @COMPCODE AND RM.LOCCODE = @LOCCODE AND RM.RESTCODE = @RESTCODE AND RC.STS = @STS AND  RM.STS=@STS " +
                    " ORDER BY MENUCODE ";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE);
                            SQLCommand.Parameters.AddWithValue("@LOCCODE", LOCCODE);
                            SQLCommand.Parameters.AddWithValue("@RESTCODE", RESTCODE);
                            SQLCommand.Parameters.AddWithValue("@STS", STS);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }
                
                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public Boolean CreateRmMenu(string COMPCODE, string LOCCODE, string RESTCODE, string MENUCATCODE, string MENUCODE, string MENUNAME, string BARCODE, string RATE, string DISCOUNT, string ISTAXAPPLY, string TAXCODE, string KITCHENITEM, string MEASURINGUNIT, string IMAGEPATH, string STS, string LOCDATE)
        {
            Boolean isupdate = false;

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    using (SqlCommand SQLCommand = new SqlCommand("sp_rmcreatemenu", con))
                    {
                        SQLCommand.CommandType = CommandType.StoredProcedure;
                        SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@LOCCODE", LOCCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@RESTCODE", RESTCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@MENUCATCODE", MENUCATCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@MENUCODE", MENUCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@MENUNAME", MENUNAME.Trim());
                        SQLCommand.Parameters.AddWithValue("@BARCODE", BARCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@RATE", RATE.Trim());
                        SQLCommand.Parameters.AddWithValue("@DISCOUNT", DISCOUNT.Trim());
                        SQLCommand.Parameters.AddWithValue("@ISTAXAPPLY", ISTAXAPPLY.Trim());
                        SQLCommand.Parameters.AddWithValue("@TAXCODE", TAXCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@KITCHENITEM", KITCHENITEM.Trim());
                        SQLCommand.Parameters.AddWithValue("@MEASURINGUNIT", MEASURINGUNIT.Trim());
                        SQLCommand.Parameters.AddWithValue("@IMAGEPATH", IMAGEPATH.Trim());
                        SQLCommand.Parameters.AddWithValue("@LOCDATE", LOCDATE.Trim());
                        SQLCommand.Parameters.AddWithValue("@STS", STS.Trim());
                        SQLCommand.ExecuteNonQuery();
                        SQLCommand.Parameters.Clear();

                        isupdate = true;
                    }
                }
                
                return isupdate;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataTable getRmTables(string COMPCODE, string LOCCODE, string RESTCODE, string STS)
        {

            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @" SELECT COMPCODE,TABLECODE,TABLENAME,NOOFSEATS,STS FROM RMTABLES WHERE COMPCODE= @COMPCODE AND LOCCODE = @LOCCODE AND RESTCODE = @RESTCODE AND  STS=@STS order by TABLECODE";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE);
                            SQLCommand.Parameters.AddWithValue("@LOCCODE", LOCCODE);
                            SQLCommand.Parameters.AddWithValue("@RESTCODE", RESTCODE);
                            SQLCommand.Parameters.AddWithValue("@STS", STS);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }
                
                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataTable getRmRooms(string COMPCODE, string LOCCODE, string STS)
        {

            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @" SELECT ROOMNOCODE,CONCAT(ROOMNO,' ' ,ROOMNAME) AS ROOM from LOCATION_ROOMS WHERE COMPCODE= @compcode AND LOCCODE =@loccode AND STS= @sts order by ROOMNO";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@compcode", COMPCODE);
                            SQLCommand.Parameters.AddWithValue("@loccode", LOCCODE);
                            SQLCommand.Parameters.AddWithValue("@STS", STS);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }
                
                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public String validateMenuItem(string menucode)
        {

            DataTable SQLDT = new DataTable();
            string Menucode = "";

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @"SELECT CASE WHEN EXISTS ( SELECT TOP 1 MENUCODE FROM SEC_IRES.dbo.RMORDER_DETAILS WHERE MENUCODE = @menucode)THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS MENUCODE";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@menucode", menucode);
                            sqlda.Fill(SQLDT);
                            Menucode = SQLDT.Rows[0]["MENUCODE"].ToString();
                        }
                    }
                }

                return Menucode;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public Boolean CreateRmTables(string COMPCODE, string LOCCODE, string RESTCODE, string TABLECODE, string TABLENAME, string NOOFSEATS, string STS)
        {
            Boolean isupdate = false;

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    using (SqlCommand SQLCommand = new SqlCommand("sp_rmcreatetables", con))
                    {
                        SQLCommand.CommandType = CommandType.StoredProcedure;
                        SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@LOCCODE", LOCCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@RESTCODE", RESTCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@TABLECODE", TABLECODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@TABLENAME", TABLENAME.Trim());
                        SQLCommand.Parameters.AddWithValue("@NOOFSEATS", NOOFSEATS.Trim());
                        SQLCommand.Parameters.AddWithValue("@STS", STS.Trim());
                        SQLCommand.ExecuteNonQuery();
                        SQLCommand.Parameters.Clear();

                        isupdate = true;
                    }
                }

                return isupdate;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }
    }
}