﻿using BusinessLogicDataHandler.Clog;
using ConnectionDataHandler;
using System;
using System.Data;
using System.Data.SqlClient;

namespace BusinessLogicDataHandler.Returns
{
    public class ReturnsDataHandler 
    {
        public DataTable getOrders(string COMPCODE, string LOCCODE, string STS, string DETSTS, string ORDERDATE)
        {
            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @" SELECT ORDERCODE, ORDERENDATE, GUESTREFCODE,ORD.TABLECODE, TBL.TABLENAME, " +
                                        " (SELECT SUM(AMOUNT) FROM RMORDER_DETAILS WHERE ORDERCODE = ORD.ORDERCODE AND STS <> @DETSTS) AS TOTAL " +
                                        " FROM RMORDERS ORD INNER JOIN RMTABLES TBL ON ORD.TABLECODE = TBL.TABLECODE " +
                                        " WHERE ORD.COMPCODE = @COMPCODE AND ORD.LOCCODE = @LOCCODE' AND ORD.STS <> @STS " +
                                        " AND ORD.ORDERENDATE = @ORDERDATE " +
                                        " ORDER BY ORDERCODE DESC ";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE);
                            SQLCommand.Parameters.AddWithValue("@LOCCODE", LOCCODE);
                            SQLCommand.Parameters.AddWithValue("@STS", STS);
                            SQLCommand.Parameters.AddWithValue("@DETSTS", DETSTS);
                            SQLCommand.Parameters.AddWithValue("@ORDERDATE", ORDERDATE);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }
                
                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataTable getOrdersByKey(string COMPCODE, string LOCCODE, string STS, string DETSTS, string KEY, string ORDERDATE)
        {
            DataTable SQLDT = new DataTable();

            try
            {
                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @" SELECT ORDERCODE, ORDERENDATE, GUESTREFCODE,ORD.TABLECODE, TBL.TABLENAME, " +
                    " (SELECT SUM(AMOUNT) FROM RMORDER_DETAILS WHERE ORDERCODE = ORD.ORDERCODE AND STS <> @DETSTS) AS TOTAL " +
                    " FROM RMORDERS ORD INNER JOIN RMTABLES TBL ON ORD.TABLECODE = TBL.TABLECODE " +
                    " WHERE ORD.COMPCODE = @COMPCODE AND ORD.LOCCODE = @LOCCODE AND ORD.STS <> @STS AND ORD.ORDERCODE LIKE @KEY " +
                    " AND ORD.ORDERENDATE = @ORDERDATE " +
                    " ORDER BY ORDERCODE DESC ";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE);
                            SQLCommand.Parameters.AddWithValue("@LOCCODE", LOCCODE);
                            SQLCommand.Parameters.AddWithValue("@STS", STS);
                            SQLCommand.Parameters.AddWithValue("@DETSTS", DETSTS);
                            SQLCommand.Parameters.AddWithValue("@KEY", KEY);
                            SQLCommand.Parameters.AddWithValue("@ORDERDATE", ORDERDATE);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }
                
                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataTable getOrderDetails(string COMPCODE, string LOCCODE, string @ORDERCODE, string ORDERDATE, string STS)
        {
            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @" SELECT KOT.ORDERCODE, KOT.MENUCATCODE, CAT.MENUCATNAME, KOT.MENUCODE, MNU.MENUNAME, KOT.STS, " +
                    " CAST(COUNT(KOT.MENUCODE) AS DECIMAL(9, 2)) AS QTY, MNU.RATE, ((COUNT(KOT.MENUCODE)) * RATE) AS AMOUNT, ORD.ORDERENDATE, TBL.TABLENAME, MNU.ISTAXAPPLY " +
                    " FROM RMORDERS ORD INNER JOIN RMORDER_KOT_DETAILS KOT " +
                    " ON ORD.ORDERCODE = KOT.ORDERCODE INNER JOIN RMMENU_CATEGORY CAT " +
                    " ON CAT.MENUCATCODE = KOT.MENUCATCODE INNER JOIN RMMENUS MNU " +
                    " ON MNU.MENUCODE = KOT.MENUCODE INNER JOIN RMTABLES TBL " +
                    " ON TBL.TABLECODE = ORD.TABLECODE " +
                    " WHERE KOT.STS <> @STS AND ORD.COMPCODE = @COMPCODE AND ORD.LOCCODE = @LOCCODE AND KOT.ORDERCODE = @ORDERCODE " +
                    " AND ORDERENDATE =  @ORDERDATE " +
                    " GROUP BY KOT.MENUCODE, KOT.ORDERCODE, KOT.STS,KOT.MENUCATCODE,CAT.MENUCATNAME,KOT.MENUCODE,MNU.MENUNAME, " +
                    " MNU.RATE, ORD.ORDERENDATE, TBL.TABLENAME, MNU.ISTAXAPPLY " +
                    " ORDER BY KOT.ORDERCODE ";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {

                            SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE);
                            SQLCommand.Parameters.AddWithValue("@LOCCODE", LOCCODE);
                            SQLCommand.Parameters.AddWithValue("@STS", STS);
                            SQLCommand.Parameters.AddWithValue("@ORDERCODE", ORDERCODE);
                            SQLCommand.Parameters.AddWithValue("@ORDERDATE", ORDERDATE);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }
                
                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public Boolean disableOrder(string COMPCODE, string LOCCODE, string ORDERCODE, string STS)
        {
            Boolean isupdate = false;

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    using (SqlCommand SQLCommand = new SqlCommand("sp_rmdeleteorder", con))
                    {
                        SQLCommand.CommandType = CommandType.StoredProcedure;
                        SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@LOCCODE", LOCCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@ORDERCODE", ORDERCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@STS", STS.Trim());
                        SQLCommand.ExecuteNonQuery();
                        SQLCommand.Parameters.Clear();

                        isupdate = true;
                    }
                }

                return isupdate;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public Boolean updateOrderDetails(string COMPCODE, string LOCCODE, string ORDERCODE, string NETAMOUNT, string STS, string STSPAYACTIVE, DataTable DTORDERS, DataTable DTREMOVED, DataTable DTTAX)
        {
            Boolean isupdate = false;

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    using (SqlCommand SQLCommand = new SqlCommand("sp_rmupdateorders", con))
                    {
                        SQLCommand.CommandType = CommandType.StoredProcedure;
                        SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@LOCCODE", LOCCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@ORDERCODE", ORDERCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@NETAMOUNT", NETAMOUNT.Trim());
                        SQLCommand.Parameters.AddWithValue("@STS", STS.Trim());
                        SQLCommand.Parameters.AddWithValue("@STSPAYACTIVE", STSPAYACTIVE.Trim());
                        SQLCommand.Parameters.AddWithValue("@paramrem", SqlDbType.Structured).Value = DTREMOVED;
                        SQLCommand.Parameters.AddWithValue("@paramdet", SqlDbType.Structured).Value = DTORDERS;
                        SQLCommand.Parameters.AddWithValue("@paramtax", SqlDbType.Structured).Value = DTTAX;
                        SQLCommand.ExecuteNonQuery();
                        SQLCommand.Parameters.Clear();

                        isupdate = true;
                    }
                }

                return isupdate;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }
    }
}