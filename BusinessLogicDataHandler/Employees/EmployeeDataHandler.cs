﻿using BusinessLogicDataHandler.Clog;
using ConnectionDataHandler;
using System;
using System.Data;
using System.Data.SqlClient;

namespace BusinessLogicDataHandler.Employees
{
    public class EmployeeDataHandler 
    {
        public DataTable getUsers(string COMPCODE, string STS)
        {
            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @" SELECT DISTINCT(UA.IDNO) AS USERID,USERNAME,GROUPNAME, EXPDATE " +
                    " FROM USERACCOUNTS UA INNER JOIN USERGROUPS UG ON UA.GROUPCODE = UG.GROUPCODE " +
                    " WHERE UA.COMPCODE = @COMPCODE AND UA.STS = @STS ORDER BY UA.IDNO "; 
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@STS", STS);
                            SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }
                
                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataTable searchUsers(string COMPCODE, string LOCCODE, string STS, string KEYWORD)
        {
            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @" SELECT DISTINCT(UA.IDNO) AS USERID,USERNAME,GROUPNAME, EXPDATE " +
                    " FROM USERACCOUNTS UA INNER JOIN USERGROUPS UG ON UA.GROUPCODE = UG.GROUPCODE " +
                    " WHERE UA.COMPCODE = @COMPCODE AND UA.STS = @STS " +
                    " AND (USERNAME LIKE @KEYWORD OR GROUPNAME LIKE @KEYWORD) ";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE);
                            SQLCommand.Parameters.AddWithValue("@LOCCODE", LOCCODE);
                            SQLCommand.Parameters.AddWithValue("@STS", STS);
                            SQLCommand.Parameters.AddWithValue("@KEYWORD", KEYWORD);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }
                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataTable getRestaurantUsers(string COMPCODE, string LOCCODE, string RESTCODE, string STS)
        {
            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @" SELECT USERNAME " +
                    " FROM MAP_LOCATION_RESTAURENTS_USERS " +
                    " WHERE COMPCODE = @COMPCODE AND LOCCODE = @LOCCODE AND RESTCODE = @RESTCODE " +
                    " AND STS = @STS ";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE);
                            SQLCommand.Parameters.AddWithValue("@LOCCODE", LOCCODE);
                            SQLCommand.Parameters.AddWithValue("@RESTCODE", RESTCODE);
                            SQLCommand.Parameters.AddWithValue("@STS", STS);
                            
                            sqlda.Fill(SQLDT);
                        }
                    }
                }
                
                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataTable getUserGroups(string COMPCODE, string LOCCODE, string STS)
        {
            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @" SELECT GROUPCODE, GROUPNAME " +
                    " FROM RMUSERGROUPS " +
                    " WHERE COMPCODE = @COMPCODE AND LOCCODE = @LOCCODE AND STS = @STS ";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE);
                            SQLCommand.Parameters.AddWithValue("@LOCCODE", LOCCODE);
                            SQLCommand.Parameters.AddWithValue("@STS", STS);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }
                
                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataTable getUserGroupsForUser(string COMPCODE, string LOCCODE, string RESTCODE, string USERNAME, string STS)
        {
            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @" SELECT USERGROUP " +
                    " FROM MAP_LOCATION_RESTAURENTS_USERS " +
                    " WHERE COMPCODE = @COMPCODE AND LOCCODE = @LOCCODE AND RESTCODE = @RESTCODE " +
                    " AND STS = @STS AND USERNAME = @USERNAME ";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE);
                            SQLCommand.Parameters.AddWithValue("@LOCCODE", LOCCODE);
                            SQLCommand.Parameters.AddWithValue("@RESTCODE", RESTCODE);
                            SQLCommand.Parameters.AddWithValue("@USERNAME", USERNAME);
                            SQLCommand.Parameters.AddWithValue("@STS", STS);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }
                
                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public Boolean createUserMapping(string compcode, string loccode, string restcode, string username, string usergroup, string enuser, string startdate, string locdate, string actsts, string delsts)
        {
            Boolean isupdate = false;

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    using (SqlCommand SQLCommand = new SqlCommand("sp_rmcreateusermapping", con))
                    {
                        SQLCommand.CommandType = CommandType.StoredProcedure;
                        SQLCommand.Parameters.AddWithValue("@COMPCODE", compcode.Trim());
                        SQLCommand.Parameters.AddWithValue("@LOCCODE", loccode.Trim());
                        SQLCommand.Parameters.AddWithValue("@RESTCODE", restcode.Trim());
                        SQLCommand.Parameters.AddWithValue("@USERNAME", username.Trim());
                        SQLCommand.Parameters.AddWithValue("@ENUSER", enuser.Trim());
                        SQLCommand.Parameters.AddWithValue("@STARTDATE", startdate.Trim());
                        SQLCommand.Parameters.AddWithValue("@USERGROUP", usergroup.Trim());
                        SQLCommand.Parameters.AddWithValue("@LOCDATE", locdate.Trim());
                        SQLCommand.Parameters.AddWithValue("@ACTSTS", actsts.Trim());
                        SQLCommand.Parameters.AddWithValue("@DELSTS", delsts.Trim());
                        SQLCommand.ExecuteNonQuery();
                        SQLCommand.Parameters.Clear();

                        isupdate = true;
                    }
                }

                return isupdate;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }
    }
}