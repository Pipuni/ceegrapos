﻿using BusinessLogicDataHandler.Clog;
using ConstantsDataHandler;
using System;
using System.Data;

namespace BusinessLogicDataHandler.Tax
{
    public class GLTaxCalculator
    {
        public DataTable calculateTaxAmount(string COMPCODE, string LOCCODE, string TAXPGCODE, double GROSSAMNTWITHOUTTAX, double GROSSAMNTWITHTAX)
        {
            GLTaxDataHandler gltaxdatahandler = new GLTaxDataHandler();
            double TOTALAMOUNT = GROSSAMNTWITHOUTTAX;
            DataTable taxseqdt = new DataTable();
            try
            {
                taxseqdt = gltaxdatahandler.getTaxSeq(COMPCODE, LOCCODE, TAXPGCODE);
                taxseqdt.Columns.Add("TAXAMOUNT", typeof(double));
                DataRow taxseqrw = taxseqdt.NewRow();
                taxseqrw["COMPCODE"] = COMPCODE;
                taxseqrw["LOCCODE"] = LOCCODE;
                taxseqrw["TAXTYPE"] = Constants.CON_TAX_GROSS_TYPE;
                double grossamnt = GROSSAMNTWITHOUTTAX + GROSSAMNTWITHTAX;
                taxseqrw["TAXAMOUNT"] = Math.Round(grossamnt, 2);
                taxseqdt.AcceptChanges();
                foreach (DataRow dataRow in taxseqdt.Rows)
                {
                    if (dataRow["TAXAPPLIEDFOR"].Equals(Constants.CON_TAX_GROSS))
                    {
                        double taxamount = GROSSAMNTWITHOUTTAX * (Convert.ToDouble(dataRow["TAXRATE"]) / 100);
                        TOTALAMOUNT = TOTALAMOUNT + taxamount;
                        dataRow["TAXAMOUNT"] = Math.Round(taxamount, 2);
                        taxseqdt.AcceptChanges();
                    }
                    else if (dataRow["TAXAPPLIEDFOR"].Equals(Constants.CON_TAX_NETT))
                    {
                        double taxamount = TOTALAMOUNT * (Convert.ToDouble(dataRow["TAXRATE"]) / 100);
                        TOTALAMOUNT = TOTALAMOUNT + taxamount;
                        dataRow["TAXAMOUNT"] = Math.Round(taxamount, 2);
                        taxseqdt.AcceptChanges();
                    }
                }
                taxseqdt.Rows.InsertAt(taxseqrw, 0);
                taxseqdt.Rows.Add(TAXPGCODE, COMPCODE, LOCCODE, null, null, null, Constants.CON_TT_TAX_AMOUNT, null, Math.Round((TOTALAMOUNT - GROSSAMNTWITHOUTTAX), 2)).AcceptChanges();
                taxseqdt.Rows.Add(TAXPGCODE, COMPCODE, LOCCODE, null, null, null, Constants.CON_TAX_NETT_RW, null, Math.Round((TOTALAMOUNT + GROSSAMNTWITHTAX), 2)).AcceptChanges();
                return taxseqdt;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
            finally
            {
                taxseqdt.Dispose();
                taxseqdt = null;
            }
        }
    }
}