﻿using ConstantsDataHandler;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BusinessLogicDataHandler.Clog
{
    public class CeegralogDataHandler
    {

        public static void LogError(Exception ex)
        {
            string message = string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            message += string.Format("Message: {0}", ex.Message);
            message += Environment.NewLine;
            message += string.Format("Message: {0}", ex.HResult);
            message += Environment.NewLine;
            message += string.Format("Message: {0}", ex.GetType().Name.ToString());
            message += Environment.NewLine;
            message += string.Format("Message: {0}", ex.GetType().ToString());
            message += Environment.NewLine;
            message += string.Format("StackTrace: {0}", ex.StackTrace);
            message += Environment.NewLine;
            message += string.Format("Source: {0}", ex.Source);
            message += Environment.NewLine;
            message += string.Format("TargetSite: {0}", ex.TargetSite.ToString());
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;

            try
            {
                string filepath = Constants.CON_ERRORLOG_PATH;
                filepath += DateTime.Today.ToString("dd-MM-yy") + ".txt";
                if (!File.Exists(filepath))
                {
                    File.Create(filepath).Dispose();
                }

                using (StreamWriter writer = new StreamWriter(filepath, true))
                {
                    writer.WriteLine(message);
                    writer.Close();
                }

            }
            catch (Exception e)
            {
                e.ToString();

            }
        }

    }
}
