﻿using BusinessLogicDataHandler.Clog;
using ConnectionDataHandler;
using ConstantsDataHandler;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Text;

namespace BusinessLogicDataHandler.Login
{
    public class LoginDataHandler 
    {
        public string EnryptPassString(string password)
        {
            string saltedpassword = password + Constants.CON_PASSWORD_SALT;
            string strmsg = ComputeSha256Hash(saltedpassword);
            byte[] encode = new byte[password.Length];
            encode = Encoding.UTF8.GetBytes(strmsg);
            strmsg = Convert.ToBase64String(encode);

            return strmsg;
        }

        private static string ComputeSha256Hash(string sha256Text)
        {
            using (SHA256 sha256Hash = SHA256.Create())
            {
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(sha256Text));
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }

        public DataRow validateCredentials(string USERNAME, string PASSWORD, string CURDATE, string STS)
        {
            DataRow dataRow = null;
            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @" SELECT MAP.COMPCODE, MAP.LOCCODE, MAP.RESTCODE, MAP.USERNAME, USR.GROUPCODE, MAP.USERGROUP,CMP.COMPNAME, RES.RESTNAME,   " +
                    " CONCAT(CMP.COMPADDRESS1, ',', CMP.COMPADDRESS2, ', ', CMP.COMPADDRESS3) AS ADDRESS " +
                    " FROM MAP_LOCATION_RESTAURENTS_USERS MAP INNER JOIN USERACCOUNTS USR ON MAP.USERNAME = USR.USERNAME " +
                    " INNER JOIN COMPANY CMP ON CMP.COMPCODE = USR.COMPCODE " +
                    " INNER JOIN LOCATION_RESTAURENTS RES ON RES.RESTCODECODE = MAP.RESTCODE " +
                    " WHERE MAP.STS = @STS AND USR.STS = @STS AND MAP.STARTDATE >= @CURDATE " +
                    " AND MAP.USERNAME = @USERNAME AND USR.PASSWORD = @PASSWORD " +
                    " ORDER BY MAP.STARTDATE DESC ";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@USERNAME", USERNAME);
                            SQLCommand.Parameters.AddWithValue("@PASSWORD", PASSWORD);
                            SQLCommand.Parameters.AddWithValue("@CURDATE", CURDATE);
                            SQLCommand.Parameters.AddWithValue("@STS", STS);
                            sqlda.Fill(SQLDT);
                            if (SQLDT.Rows.Count > 0)
                            {
                                dataRow = SQLDT.Rows[0];
                            }
                        }
                    }
                }
                
                return dataRow;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public Boolean userLoginValidation(string username, string password, string curDate, string sts, string dsts)
        {
            Boolean validCredentials = false;
            string COMPCODE = "";
            string LOCCODE = "";
            string RESTCODE = "";
            string USERNAME = "";
            string GROUPCODE = "";
            string USERGROUP = "";
            string COMPNAME = "";
            string RESTNAME = "";
            string ADDRESS = "";

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    using (SqlCommand SQLCommand = new SqlCommand("sp_rmloginvalidation", con))
                    {
                        SQLCommand.CommandType = CommandType.StoredProcedure;
                        SQLCommand.Parameters.AddWithValue("@UNAME", username.Trim());
                        SQLCommand.Parameters.AddWithValue("@PASSWORD", password.Trim());
                        SQLCommand.Parameters.AddWithValue("@CURDATE", curDate.Trim());
                        SQLCommand.Parameters.AddWithValue("@STS", sts.Trim());
                        SQLCommand.Parameters.AddWithValue("@DSTS", dsts.Trim());
                        SQLCommand.Parameters.Add("@COMPCODE", SqlDbType.VarChar, 20);
                        SQLCommand.Parameters.Add("@LOCCODE", SqlDbType.VarChar, 20);
                        SQLCommand.Parameters.Add("@RESTCODE", SqlDbType.VarChar, 20);
                        SQLCommand.Parameters.Add("@USERNAME", SqlDbType.VarChar, 100);
                        SQLCommand.Parameters.Add("@GROUPCODE", SqlDbType.VarChar, 20);
                        SQLCommand.Parameters.Add("@USERGROUP", SqlDbType.VarChar, 20);
                        SQLCommand.Parameters.Add("@COMPNAME", SqlDbType.VarChar, 100);
                        SQLCommand.Parameters.Add("@RESTNAME", SqlDbType.VarChar, 100);
                        SQLCommand.Parameters.Add("@ADDRESS", SqlDbType.VarChar, 200);
                        SQLCommand.Parameters["@COMPCODE"].Direction = ParameterDirection.Output;
                        SQLCommand.Parameters["@LOCCODE"].Direction = ParameterDirection.Output;
                        SQLCommand.Parameters["@RESTCODE"].Direction = ParameterDirection.Output;
                        SQLCommand.Parameters["@USERNAME"].Direction = ParameterDirection.Output;
                        SQLCommand.Parameters["@GROUPCODE"].Direction = ParameterDirection.Output;
                        SQLCommand.Parameters["@USERGROUP"].Direction = ParameterDirection.Output;
                        SQLCommand.Parameters["@COMPNAME"].Direction = ParameterDirection.Output;
                        SQLCommand.Parameters["@RESTNAME"].Direction = ParameterDirection.Output;
                        SQLCommand.Parameters["@ADDRESS"].Direction = ParameterDirection.Output;
                        SQLCommand.ExecuteNonQuery();
                        SQLCommand.Parameters.Clear();

                        COMPCODE = Convert.ToString(SQLCommand.Parameters["@COMPCODE"].Value);
                        LOCCODE = Convert.ToString(SQLCommand.Parameters["@LOCCODE"].Value);
                        RESTCODE = Convert.ToString(SQLCommand.Parameters["@RESTCODE"].Value);
                        USERNAME = Convert.ToString(SQLCommand.Parameters["@USERNAME"].Value);
                        GROUPCODE = Convert.ToString(SQLCommand.Parameters["@GROUPCODE"].Value);
                        USERGROUP = Convert.ToString(SQLCommand.Parameters["@USERGROUP"].Value);
                        COMPNAME = Convert.ToString(SQLCommand.Parameters["@COMPNAME"].Value);
                        RESTNAME = Convert.ToString(SQLCommand.Parameters["@RESTNAME"].Value);
                        ADDRESS = Convert.ToString(SQLCommand.Parameters["@ADDRESS"].Value);

                        if (COMPCODE != "" && LOCCODE != "" && RESTCODE != "" && USERNAME != "" && GROUPCODE != "" && USERGROUP != "" && COMPNAME != "" && RESTNAME != "" && ADDRESS != "")
                        {
                            // set sessions
                            Constants.SYS_SESSION_COMPCODE = COMPCODE;
                            Constants.SYS_SESSION_USERNAME = USERNAME;
                            Constants.SYS_SESSION_USERGROUP = USERGROUP;
                            Constants.SYS_SESSION_LOCCODE = LOCCODE;
                            Constants.SYS_SESSION_RESTCODE = RESTCODE;
                            Constants.SYS_SESSION_COMPANYNAME = RESTNAME;
                            Constants.SYS_SESSION_ADDRESS = ADDRESS;

                            // credentials validated
                            validCredentials = true;
                        }

                    }
                }
                
                return validCredentials;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }
    }
}