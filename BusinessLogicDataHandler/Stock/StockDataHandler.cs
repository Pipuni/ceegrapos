﻿using BusinessLogicDataHandler.Clog;
using ConnectionDataHandler;
using System;
using System.Data;
using System.Data.SqlClient;

namespace BusinessLogicDataHandler.Stock
{
    public class StockDataHandler
    {
        public DataTable getMenu(string COMPCODE, string LOCCODE, string RESTCODE, string STS)
        {

            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @" SELECT RM.MENUCATCODE AS MENUCATCODE,MENUCODE,MENUNAME,IMAGEPATH,RATE,COLORCODE,RM.STS AS STS,RM.ISTAXAPPLY AS ISTAXAPPLY FROM RMMENUS RM " +
                                   " INNER JOIN RMMENU_CATEGORY RC ON RM.MENUCATCODE = RC.MENUCATCODE WHERE RM.COMPCODE= @COMPCODE AND RM.LOCCODE = @LOCCODE AND RM.RESTCODE = @RESTCODE AND RC.STS = @STS AND  RM.STS= @STS order by MENUCODE";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE);
                            SQLCommand.Parameters.AddWithValue("@LOCCODE", LOCCODE);
                            SQLCommand.Parameters.AddWithValue("@RESTCODE", RESTCODE);
                            SQLCommand.Parameters.AddWithValue("@STS", STS);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }

                
                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }
    }
}