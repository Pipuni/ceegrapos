﻿using BusinessLogicDataHandler.Clog;
using ConnectionDataHandler;
using System;
using System.Data;
using System.Data.SqlClient;

namespace BusinessLogicDataHandler.Reports.Invoice
{
    public class InvoiceDataHandler 
    {
        public DataTable getOrderDetails(string COMPCODE, string LOCCODE, string RESTCODE, string ORDERCODE, string STS)
        {

            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @" SELECT DET.MENUCATCODE, CAT.MENUCATNAME, DET.MENUCODE, MNU.MENUNAME, QTY,DET.RATE,  " +
                                    " CAST(AMOUNT as DECIMAL(9, 2)) AMOUNT, ORD.ORDERENDATE, TABLENAME, " +
                                    " PAY.BILLMOUNT, PAY.PAIDAMOUNT, PAY.DUEAMOUNT " +
                                    " FROM RMORDER_DETAILS DET INNER JOIN RMMENU_CATEGORY CAT ON DET.MENUCATCODE = CAT.MENUCATCODE " +
                                    " INNER JOIN RMMENUS MNU ON MNU.MENUCODE = DET.MENUCODE " +
                                    " INNER JOIN RMORDERS ORD ON ORD.ORDERCODE = DET.ORDERCODE " +
                                    " INNER JOIN RMTABLES TBL ON TBL.TABLECODE = ORD.TABLECODE " +
                                    " INNER JOIN RMORDER_PAYMENT PAY ON PAY.ORDERCODE = ORD.ORDERCODE " +
                                    " WHERE ORD.COMPCODE = @COMPCODE AND ORD.LOCCODE = @LOCCODE' AND ORD.RESTCODE = @RESTCODE " +
                                    " AND DET.ORDERCODE = @COMPCODE AND DET.STS = @STS " +
                                    " ORDER BY DET.IDNO DESC ";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@compcode", COMPCODE);
                            SQLCommand.Parameters.AddWithValue("@loccode", LOCCODE);
                            SQLCommand.Parameters.AddWithValue("@RESTCODE", RESTCODE);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }
                
                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }
    }
}