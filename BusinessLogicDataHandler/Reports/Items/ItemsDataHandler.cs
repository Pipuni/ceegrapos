﻿using BusinessLogicDataHandler.Clog;
using ConnectionDataHandler;
using System;
using System.Data;
using System.Data.SqlClient;

namespace BusinessLogicDataHandler.Reports.Items
{
    public class ItemsDataHandler 
    {
        public DataTable getReportsForCategory(string CATEGORY, string STS)
        {
            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @" SELECT REPORTID, REPORTNAME, PATH " +
                    " FROM REPORTS " +
                    " WHERE REPORTCAT = @CATEGORY ";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@STS", STS);
                            SQLCommand.Parameters.AddWithValue("@CATEGORY", CATEGORY);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }
                
                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataTable getItems(string COMPCODE, string LOCCODE, string RESTCODE, string STS)
        {

            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @" SELECT MEN.MENUNAME, SUM(DET.QTY) AS QTY, SUM(DET.AMOUNT) AS TOTAL " +
                    " FROM RMORDER_DETAILS DET INNER JOIN RMMENUS MEN ON DET.MENUCODE = MEN.MENUCODE " +
                    " INNER JOIN RMMENU_CATEGORY CAT ON CAT.MENUCATCODE = DET.MENUCATCODE " +
                    " INNER JOIN RMORDERS ORD ON ORD.ORDERCODE = DET.ORDERCODE " +
                    " WHERE ORD.COMPCODE = @COMPCODE' AND ORD.LOCCODE = @LOCCODE AND ORD.RESTCODE = @RESTCODE AND DET.STS=@STS " +
                    " GROUP BY MEN.MENUNAME " +
                    " ORDER BY SUM(DET.QTY) DESC ";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE);
                            SQLCommand.Parameters.AddWithValue("@LOCCODE", LOCCODE);
                            SQLCommand.Parameters.AddWithValue("@RESTCODE", RESTCODE);
                            SQLCommand.Parameters.AddWithValue("@STS", STS);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }
                
                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataTable getItemsFiltered(string COMPCODE, string LOCCODE, string RESTCODE, string FORM, string TO, string STS)
        {

            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @" SELECT MEN.MENUNAME, SUM(DET.QTY) AS QTY, SUM(DET.AMOUNT) AS TOTAL " +
                    " FROM RMORDER_DETAILS DET INNER JOIN RMMENUS MEN ON DET.MENUCODE = MEN.MENUCODE " +
                    " INNER JOIN RMMENU_CATEGORY CAT ON CAT.MENUCATCODE = DET.MENUCATCODE " +
                    " INNER JOIN RMORDERS ORD ON ORD.ORDERCODE = DET.ORDERCODE " +
                    " WHERE ORD.COMPCODE = @COMPCODE AND ORD.LOCCODE = @LOCCODE AND ORD.RESTCODE = @RESTCODE AND DET.STS= @STS " +
                    " AND ORDERENDATE >= @FORM' AND ORDERENDATE<DATEADD(DAYOFYEAR, 1, @TO') " +
                    " GROUP BY MEN.MENUNAME " +
                    " ORDER BY SUM(DET.QTY) DESC ";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE);
                            SQLCommand.Parameters.AddWithValue("@LOCCODE", LOCCODE);
                            SQLCommand.Parameters.AddWithValue("@RESTCODE", RESTCODE);
                            SQLCommand.Parameters.AddWithValue("@FORM", FORM);
                            SQLCommand.Parameters.AddWithValue("@TO", TO);
                            SQLCommand.Parameters.AddWithValue("@STS", STS);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }
                
                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataTable getMenuCategory(string COMPCODE, string LOCCODE, string RESTCODE, string STS)
        {

            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @" SELECT CAT.MENUCATNAME, SUM(DET.QTY) AS QTY, SUM(DET.AMOUNT) AS TOTAL " +
                    " FROM RMORDER_DETAILS DET INNER JOIN RMMENUS MEN ON DET.MENUCODE = MEN.MENUCODE " +
                    " INNER JOIN RMMENU_CATEGORY CAT ON CAT.MENUCATCODE = DET.MENUCATCODE " +
                    " INNER JOIN RMORDERS ORD ON ORD.ORDERCODE = DET.ORDERCODE " +
                    " WHERE ORD.COMPCODE = @COMPCODE AND ORD.LOCCODE = @LOCCODE AND ORD.RESTCODE = @RESTCODE AND DET.STS= @STS " +
                    " GROUP BY CAT.MENUCATNAME " +
                    " ORDER BY SUM(DET.QTY) DESC ";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE);
                            SQLCommand.Parameters.AddWithValue("@LOCCODE", LOCCODE);
                            SQLCommand.Parameters.AddWithValue("@RESTCODE", RESTCODE);
                            SQLCommand.Parameters.AddWithValue("@STS", STS);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }
                
                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataTable getMenuCategoryFiltered(string COMPCODE, string LOCCODE, string RESTCODE, string FORM, string TO, string STS)
        {

            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @" SELECT CAT.MENUCATNAME, SUM(DET.QTY) AS QTY, SUM(DET.AMOUNT) AS TOTAL " +
                                    " FROM RMORDER_DETAILS DET INNER JOIN RMMENUS MEN ON DET.MENUCODE = MEN.MENUCODE " +
                                    " INNER JOIN RMMENU_CATEGORY CAT ON CAT.MENUCATCODE = DET.MENUCATCODE " +
                                    " INNER JOIN RMORDERS ORD ON ORD.ORDERCODE = DET.ORDERCODE " +
                                    " WHERE ORD.COMPCODE = @COMPCODE AND ORD.LOCCODE = @LOCCODE AND ORD.RESTCODE = @RESTCODE AND DET.STS= @STS " +
                                    " AND ORDERENDATE >= @FORM' AND ORDERENDATE<DATEADD(DAYOFYEAR, 1, @TO) " +
                                    " GROUP BY CAT.MENUCATNAME " +
                                    " ORDER BY SUM(DET.QTY) DESC ";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE);
                            SQLCommand.Parameters.AddWithValue("@LOCCODE", LOCCODE);
                            SQLCommand.Parameters.AddWithValue("@RESTCODE", RESTCODE);
                            SQLCommand.Parameters.AddWithValue("@FORM", FORM);
                            SQLCommand.Parameters.AddWithValue("@TO", TO);
                            SQLCommand.Parameters.AddWithValue("@STS", STS);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }

                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }
    }
}