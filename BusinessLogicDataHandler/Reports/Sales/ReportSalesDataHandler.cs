﻿using BusinessLogicDataHandler.Clog;
using ConnectionDataHandler;
using ConstantsDataHandler;
using System;
using System.Data;
using System.Data.SqlClient;

namespace BusinessLogicDataHandler.Reports.Sales
{
    public class ReportSalesDataHandler
    {
        public DataTable getOrders(string COMPCODE, string LOCCODE, string RESTCODE, string ORDERSTS)
        {

            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = " SELECT ORD.ORDERCODE, ORDERENDATE, TABLENAME, PAY.BILLMOUNT AS TOTAL " +
                    " FROM RMORDERS ORD INNER JOIN RMTABLES TBL ON ORD.TABLECODE = TBL.TABLECODE " +
                    " INNER JOIN RMORDER_PAYMENT PAY ON PAY.ORDERCODE = ORD.ORDERCODE " +
                    " WHERE ORD.COMPCODE = @COMPCODE AND ORD.LOCCODE = @LOCCODE " +
                    " AND ORD.RESTCODE = @RESTCODE AND ORD.STS = @ORDERSTS " +
                    " ORDER BY ORD.ORDERCODE DESC ";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE);
                            SQLCommand.Parameters.AddWithValue("@LOCCODE", LOCCODE);
                            SQLCommand.Parameters.AddWithValue("@RESTCODE", RESTCODE);
                            SQLCommand.Parameters.AddWithValue("@ORDERSTS", ORDERSTS);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }
                
                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataTable getOrdersFiltered(string COMPCODE, string LOCCODE, string RESTCODE, string FORM, string TO,  string ORDERSTS)
        {

            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = " SELECT ORD.ORDERCODE, ORDERENDATE, TABLENAME, PAY.BILLMOUNT AS TOTAL " +
                    " FROM RMORDERS ORD INNER JOIN RMTABLES TBL ON ORD.TABLECODE = TBL.TABLECODE " +
                    " INNER JOIN RMORDER_PAYMENT PAY ON PAY.ORDERCODE = ORD.ORDERCODE " +
                    " WHERE ORD.COMPCODE = @COMPCODE AND ORD.LOCCODE = @LOCCODE AND ORD.RESTCODE = '" + RESTCODE + "' AND ORD.STS = @ORDERSTS " +
                    " AND ORDERENDATE >= @FORM AND ORDERENDATE < DATEADD(DAYOFYEAR, 1, @TO )" +
                    " ORDER BY ORD.ORDERCODE DESC ";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE);
                            SQLCommand.Parameters.AddWithValue("@LOCCODE", LOCCODE);
                            SQLCommand.Parameters.AddWithValue("@RESTCODE", RESTCODE);
                            SQLCommand.Parameters.AddWithValue("@FORM", FORM);
                            SQLCommand.Parameters.AddWithValue("@TO", TO);
                            SQLCommand.Parameters.AddWithValue("@ORDERSTS", ORDERSTS);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }
                
                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataTable getReportsForCategory(string CATEGORY, string STS)
        {

            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = " SELECT REPORTID, REPORTNAME, PATH " +
                    " FROM REPORTS " +
                    " WHERE REPORTCAT = @CATEGORY ";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@CATEGORY", CATEGORY);
                            SQLCommand.Parameters.AddWithValue("@STS", STS);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }
                
                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataTable getReceiptDetails(string ORDERCODE, string RESTCODE)
        {

            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = "select RMP.ORDERCODE,REP.PAYMENTMODE AS PAYMODE,(SELECT ROOMNO FROM LOCATION_ROOMS LR WHERE LR.ROOMNOCODE=RMP.ROOMNOCODE) AS ROOMNO,RMP.RESTCODE,BILLMOUNT AS BILLAMOUNT,DUEAMOUNT,DISCOUNT,PAIDAMOUNT, (SELECT TABLENAME from RMTABLES RT where RT.TABLECODE = RO.TABLECODE ) AS TABLENAME, RO.ORDERENDATE,(SELECT ORDERTYPE FROM RMORDERTYPES ROT WHERE ROT.ORDERTYPECODE= RO.ORDERTYPECODE) AS ORDERTYPE " +
                    " from RMORDER_PAYMENT RMP left join RESERVATION_PAYMENT_OPTIONS REP on REP.PAYMENTCODE = RMP.PAYMODE left join RMORDERS RO on RO.ORDERCODE = RMP.ORDERCODE WHERE RMP.ORDERCODE = @ORDERCODE AND RMP.RESTCODE = @RESTCODE";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@ORDERCODE", ORDERCODE);
                            SQLCommand.Parameters.AddWithValue("@RESTCODE", RESTCODE);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }
                
                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataTable getReceiptOrderDetails(string ORDERCODE)
        {

            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = "SELECT RM.MENUNAME,(SELECT MENUCATNAME FROM RMMENU_CATEGORY RMC WHERE RMC.MENUCATCODE = RM.MENUCATCODE) AS MENUCATNAME,QTY,AMOUNT,RM.RATE FROM RMORDER_DETAILS ROD LEFT JOIN RMMENUS RM ON RM.MENUCODE = ROD.MENUCODE WHERE ROD.ORDERCODE = @ORDERCODE AND ROD.STS = '" + Constants.CON_STATUS_ACTIVE + "'";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@ORDERCODE", ORDERCODE);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }

                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataTable getReceiptTaxDetails(string ORDERCODE, string RESTCODE)
        {

            DataTable SQLDT = new DataTable();

            try
            {
                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = "SELECT ORDERCODE,(SELECT TAXTYPE FROM GLTAXTYPE GT WHERE GT.TAXCODE = RPD.TAXCODE ) AS TAXTYPE,	RPD.TAXRATE,RPD.AMOUNT AS TAXAMOUNT FROM RMORDER_PAYMENT_DETAILS RPD WHERE ORDERCODE = @ORDERCODE AND RESTCODE = @RESTCODE ";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@ORDERCODE", ORDERCODE);
                            SQLCommand.Parameters.AddWithValue("@RESTCODE", RESTCODE);

                            sqlda.Fill(SQLDT);
                        }
                    }
                }

                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataTable getReceiptHeaderFooter(string COMPCODE, string LOCCODE, string RESTCODE)
        {

            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = "SELECT HEADER,FOOTER FROM RMRECEIPT_DETAILS WHERE RESTCODE = @RESTCODE AND COMPCODE = @COMPCODE AND LOCCODE = @LOCCODE AND STS = '" + Constants.CON_STATUS_ACTIVE + "'";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE);
                            SQLCommand.Parameters.AddWithValue("@LOCCODE", LOCCODE);
                            SQLCommand.Parameters.AddWithValue("@RESTCODE", RESTCODE);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }

                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }
    }
}