﻿using BusinessLogicDataHandler.Clog;
using ConnectionDataHandler;
using System;
using System.Data;
using System.Data.SqlClient;

namespace BusinessLogicDataHandler.Settings
{
    public class SettingsDataHandler 
    {
        public DataRow getMenuCategory(string COMPCODE, string LOCCODE, string RESTCODE, string STS, string MENUCATNAME)
        {
            DataTable SQLDT = new DataTable();
            DataRow dataRow = null;

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @" SELECT MENUCATNAME FROM RMMENU_CATEGORY " +
                    " WHERE COMPCODE= @COMPCODE AND LOCCODE = @LOCCODE AND RESTCODE = @RESTCODE AND  STS= @STS  AND MENUCATNAME=@MENUCATNAME  order by MENUCATCODE";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE);
                            SQLCommand.Parameters.AddWithValue("@LOCCODE", LOCCODE);
                            SQLCommand.Parameters.AddWithValue("@RESTCODE", RESTCODE);
                            SQLCommand.Parameters.AddWithValue("@STS", STS);
                            SQLCommand.Parameters.AddWithValue("@MENUCATNAME", MENUCATNAME);
                            sqlda.Fill(SQLDT);
                            if (SQLDT.Rows.Count > 0)
                            {
                                dataRow = SQLDT.Rows[0];
                            }


                        }
                    }
                }

                return dataRow;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }
        public DataTable getMenuCategory(string COMPCODE, string LOCCODE, string RESTCODE, string STS)
        {

            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @" SELECT MENUCATCODE,MENUCATNAME,COLORCODE " +
                    " FROM RMMENU_CATEGORY " +
                    " WHERE COMPCODE= @COMPCODE AND LOCCODE = @LOCCODE AND RESTCODE = @RESTCODE AND  STS= @STS order by MENUCATCODE";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE);
                            SQLCommand.Parameters.AddWithValue("@LOCCODE", LOCCODE);
                            SQLCommand.Parameters.AddWithValue("@RESTCODE", RESTCODE);
                            SQLCommand.Parameters.AddWithValue("@STS", STS);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }

                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataTable getreceiptdetails(string COMPCODE, string LOCCODE, string RESTCODE)
        {

            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @"SELECT RECEIPTDETCODE ,HEADER, FOOTER, STS FROM RMRECEIPT_DETAILS WHERE COMPCODE= @COMPCODE AND LOCCODE = @LOCCODE AND RESTCODE = @RESTCODE ORDER BY STS";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE);
                            SQLCommand.Parameters.AddWithValue("@LOCCODE", LOCCODE);
                            SQLCommand.Parameters.AddWithValue("@RESTCODE", RESTCODE);
                            
                            sqlda.Fill(SQLDT);
                        }
                    }
                }

                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public Boolean CreateReceiptDetails(string RECEIPTDETCODE, string COMPCODE, string LOCCODE, string RESTCODE, string HEADER, string FOOTER, string STS, string ENUSER, string LOCDATE)
        {
            Boolean isupdate = false;

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    using (SqlCommand SQLCommand = new SqlCommand("sp_rmcreatereceiptdetails", con))
                    {
                        SQLCommand.CommandType = CommandType.StoredProcedure;
                        SQLCommand.Parameters.AddWithValue("@RECEIPTDETCODE", RECEIPTDETCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@LOCCODE", LOCCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@RESTCODE", RESTCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@HEADER", HEADER.Trim());
                        SQLCommand.Parameters.AddWithValue("@FOOTER", FOOTER.Trim());
                        SQLCommand.Parameters.AddWithValue("@ENUSER", ENUSER.Trim());
                        SQLCommand.Parameters.AddWithValue("@LOCDATE", LOCDATE.Trim());
                        SQLCommand.Parameters.AddWithValue("@STS", STS.Trim());
                        SQLCommand.ExecuteNonQuery();
                        SQLCommand.Parameters.Clear();

                        isupdate = true;
                    }
                }

                return isupdate;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public Boolean CreateRmMenuCategory(string COMPCODE, string LOCCODE, string RESTCODE, string MENUCATCODE, string MENUCATNAME, string COLORCODE, string LOCDATE, string STS)
        {
            Boolean isupdate = false;

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    using (SqlCommand SQLCommand = new SqlCommand("sp_rmcreatemenucategory", con))
                    {
                        SQLCommand.CommandType = CommandType.StoredProcedure;
                        SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@LOCCODE", LOCCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@RESTCODE", RESTCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@MENUCATCODE", MENUCATCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@MENUCATNAME", MENUCATNAME.Trim());
                        SQLCommand.Parameters.AddWithValue("@COLORCODE", COLORCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@LOCDATE", LOCDATE.Trim());
                        SQLCommand.Parameters.AddWithValue("@STS", STS.Trim());
                        SQLCommand.ExecuteNonQuery();
                        SQLCommand.Parameters.Clear();

                        isupdate = true;
                    }
                }

                return isupdate;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataRow getRmTables(string COMPCODE, string LOCCODE, string RESTCODE, string STS, string TABLENAME)
        {

           

            DataTable SQLDT = new DataTable();
            DataRow dataRow = null;

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @" SELECT TABLENAME FROM RMTABLES " +
                    " WHERE COMPCODE= @COMPCODE AND LOCCODE = @LOCCODE AND RESTCODE = @RESTCODE AND  STS= @STS  AND TABLENAME=@TABLENAME ";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE);
                            SQLCommand.Parameters.AddWithValue("@LOCCODE", LOCCODE);
                            SQLCommand.Parameters.AddWithValue("@RESTCODE", RESTCODE);
                            SQLCommand.Parameters.AddWithValue("@STS", STS);
                            SQLCommand.Parameters.AddWithValue("@TABLENAME", TABLENAME);
                            sqlda.Fill(SQLDT);
                            if (SQLDT.Rows.Count > 0)
                            {
                                dataRow = SQLDT.Rows[0];
                            }
                        }
                    }
                }

                return dataRow;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }
        public DataTable getRmTables(string COMPCODE, string LOCCODE, string RESTCODE, string STS)
        {

            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @" SELECT TABLECODE,TABLENAME,NOOFSEATS,COLORCODE " +
                    " FROM RMTABLES " +
                    " WHERE COMPCODE= @COMPCODE AND LOCCODE = @LOCCODE AND RESTCODE = @RESTCODE AND  STS= @STS order by TABLECODE";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE);
                            SQLCommand.Parameters.AddWithValue("@LOCCODE", LOCCODE);
                            SQLCommand.Parameters.AddWithValue("@RESTCODE", RESTCODE);
                            SQLCommand.Parameters.AddWithValue("@STS", STS);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }

                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public Boolean CreateRmTables(string COMPCODE, string LOCCODE, string RESTCODE, string TABLECODE, string TABLENAME, string NOOFSEATS, string COLORCODE, string LOCDATE, string STS)
        {
            Boolean isupdate = false;

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    using (SqlCommand SQLCommand = new SqlCommand("sp_rmcreatetables", con))
                    {
                        SQLCommand.CommandType = CommandType.StoredProcedure;
                        SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@LOCCODE", LOCCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@RESTCODE", RESTCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@TABLECODE", TABLECODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@TABLENAME", TABLENAME.Trim());
                        SQLCommand.Parameters.AddWithValue("@NOOFSEATS", NOOFSEATS.Trim());
                        SQLCommand.Parameters.AddWithValue("@LOCDATE", LOCDATE.Trim());
                        SQLCommand.Parameters.AddWithValue("@COLORCODE", COLORCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@STS", STS.Trim());
                        SQLCommand.ExecuteNonQuery();
                        SQLCommand.Parameters.Clear();

                        isupdate = true;
                    }
                }
                
                return isupdate;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }
    }
}