﻿using BusinessLogicDataHandler.Clog;
using ConnectionDataHandler;
using System;
using System.Data;
using System.Data.SqlClient;

namespace BusinessLogicDataHandler.KitchenDisplay
{
    public class KitchenDisplayDataHandler 
    {
        public DataTable getKitchenItems(string COMPCODE, string LOCCODE, string RESTCODE, string STS)
        {

            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @"SELECT  RMD.ORDERCODE,RMD.MENUCODE, COUNT(RMD.MENUCODE) AS QTY,RMD.MENUCATCODE,RMD.MENUCODE, RMN.MENUNAME,RMN.IMAGEPATH, RMD.STS, RMT.TABLENAME,RMT.COLORCODE, RM.ENUSER " +
                    "FROM RMORDER_KOT_DETAILS RMD LEFT JOIN RMORDERS  RM ON RM.ORDERCODE = RMD.ORDERCODE LEFT JOIN RMTABLES RMT ON RM.TABLECODE = RMT.TABLECODE " +
                    "LEFT JOIN RMMENUS RMN ON RMN.MENUCODE = RMD.MENUCODE  WHERE RM.COMPCODE = @COMPCODE AND RM.LOCCODE = @LOCCODE AND RM.RESTCODE = @RESTCODE AND RMD.STS = @STS " +
                    "GROUP BY RMD.MENUCODE,RMD.ORDERCODE,RMD.MENUCATCODE,RMD.MENUCODE, RMN.MENUNAME,RMN.IMAGEPATH, RMD.STS, RMT.TABLENAME,RMT.COLORCODE, RM.ENUSER ORDER BY RMD.ORDERCODE";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE);
                            SQLCommand.Parameters.AddWithValue("@LOCCODE", LOCCODE);
                            SQLCommand.Parameters.AddWithValue("@RESTCODE", RESTCODE);
                            SQLCommand.Parameters.AddWithValue("@STS", STS);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }

                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public bool setOrderReady(string ORDERCODE, string MENUCATCODE, string MENUCODE, string STS, string LOCDATE)
        {
            Boolean isupdate = false;

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    using (SqlCommand SQLCommand = new SqlCommand("sp_rmupdatektdisplay", con))
                    {
                        SQLCommand.CommandType = CommandType.StoredProcedure;
                        SQLCommand.Parameters.AddWithValue("@ORDERCODE", ORDERCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@MENUCATCODE", MENUCATCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@MENUCODE", MENUCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@STS", STS.Trim());
                        SQLCommand.Parameters.AddWithValue("@LOCDATE", LOCDATE.Trim());
                        SQLCommand.ExecuteNonQuery();
                        SQLCommand.Parameters.Clear();

                        isupdate = true;
                    }
                }

                return isupdate;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }
    }
}