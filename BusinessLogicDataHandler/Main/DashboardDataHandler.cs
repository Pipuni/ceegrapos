﻿using BusinessLogicDataHandler.Clog;
using ConnectionDataHandler;
using System;
using System.Data;
using System.Data.SqlClient;

namespace BusinessLogicDataHandler.Main
{
    public class DashboardDataHandler
    {
        public DataTable getAccessRights(string COMPCODE, string LOCCODE, string GROUPCODE, string STS)
        {

            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @" SELECT MAP.MODULECODE " +
                    " FROM RMUSERGROUPS GRP INNER JOIN RMMAPMODULES MAP " +
                    " ON GRP.GROUPCODE = MAP.GROUPCODE " +
                    " WHERE GRP.COMPCODE = @COMPCODE AND GRP.LOCCODE = @LOCCODE AND " +
                    " MAP.STS = @STS AND MAP.GROUPCODE = @GROUPCODE ";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE);
                            SQLCommand.Parameters.AddWithValue("@LOCCODE", LOCCODE);
                            SQLCommand.Parameters.AddWithValue("@GROUPCODE", GROUPCODE);
                            SQLCommand.Parameters.AddWithValue("@STS", STS);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }

                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }
    }
}