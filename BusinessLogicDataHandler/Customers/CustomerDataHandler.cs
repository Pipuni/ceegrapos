﻿using BusinessLogicDataHandler.Clog;
using ConnectionDataHandler;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace BusinessLogicDataHandler.Customers
{
    public class CustomerDataHandler 
    {
        public Dictionary<string, string> getCustomerTitles()
        {
            Dictionary<string, string> getstatus = new Dictionary<string, string>();
            getstatus.Add("Mr", "Mr");
            getstatus.Add("Ms", "Ms");
            getstatus.Add("Mrs", "Mrs");
            getstatus.Add("Mess", "Mess");
            return getstatus;
        }

        public DataTable getCustomers(string COMPCODE, string LOCCODE, string STS)
        {

            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString  = @" SELECT GUESTREFCODE AS ID, NICPP, CONCAT(TITLE, ' ', FIRSTNAME, ' ', LASTNAME) AS FULLNAME, EMAIL, CONTACTNO, COUNTRY, 'GUEST' AS CUSTYPE " +
                    " FROM GUESTS " +
                    " WHERE COMPCODE = @COMPCODE AND LOCCODE = @LOCCODE AND STS= @STS " +
                    " UNION " +
                    " SELECT CLIENTREFCODE AS ID, NICPP, CONCAT(TITLE, ' ' , FIRSTNAME, ' ', LASTNAME) AS FULLNAME, EMAIL, CONTACTNO, COUNTRY, 'CLIENT' AS CUSTYPE " +
                    " FROM GLCLIENTS CL " +
                    " WHERE COMPCODE = @COMPCODE AND STS = @STS " +
                    " ORDER BY FULLNAME";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE);
                            SQLCommand.Parameters.AddWithValue("@LOCCODE", LOCCODE);
                            SQLCommand.Parameters.AddWithValue("@STS", STS);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }
                
                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataTable searchCustomers(string COMPCODE, string LOCCODE, string STS, string KEYWORD)
        {

            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString  = @" SELECT GUESTREFCODE AS ID, NICPP, CONCAT(TITLE, ' ', FIRSTNAME, ' ', LASTNAME) AS FULLNAME, EMAIL, CONTACTNO, COUNTRY, 'GUEST' AS CUSTYPE " +
                    " FROM GUESTS " +
                    " WHERE COMPCODE = @COMPCODE AND LOCCODE = @LOCCODE AND STS= @STS " +
                    " AND (TITLE LIKE @KEYWORD OR FIRSTNAME LIKE @KEYWORD OR LASTNAME LIKE @KEYWORD OR NICPP LIKE @KEYWORD OR CONTACTNO LIKE @KEYWORD ) " +
                    " UNION " +
                    " SELECT CLIENTREFCODE AS ID, NICPP, CONCAT(TITLE, ' ' , FIRSTNAME, ' ', LASTNAME) AS FULLNAME, EMAIL, CONTACTNO, COUNTRY, 'CLIENT' AS CUSTYPE " +
                    " FROM GLCLIENTS CL " +
                    " WHERE COMPCODE = @COMPCODE AND STS = @STS " +
                    " AND (TITLE LIKE @KEYWORD OR FIRSTNAME LIKE @KEYWORD' OR LASTNAME LIKE @KEYWORD OR NICPP LIKE @KEYWORD' OR CONTACTNO LIKE @KEYWORD ) " +
                    " ORDER BY FULLNAME ";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE);
                            SQLCommand.Parameters.AddWithValue("@LOCCODE", LOCCODE);
                            SQLCommand.Parameters.AddWithValue("@STS", STS);
                            SQLCommand.Parameters.AddWithValue("@KEYWORD", '%' + KEYWORD + '%');
                            sqlda.Fill(SQLDT);
                        }
                    }
                }
                
                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataTable getClientTypes(string STS)
        {

            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = "SELECT CLIENTCODE,DESCRIPTION from GLCLIENTTYPES " +
                    "WHERE STS = @STS order by DESCRIPTION";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@STS", STS);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }
                
                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataTable getGuestTypes(string STS)
        {

            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString  = @" SELECT GUESTCODE,GUESTTYPE FROM GUESTTYPES WHERE STS = @STS order by GUESTCODE";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@STS", STS);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }
                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataTable getCountries()
        {

            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = "SELECT IDNO,COUNTRY FROM COUNTRY order by COUNTRY";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            sqlda.Fill(SQLDT);
                        }
                    }
                }
                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public Boolean createClient(string CLIENTREFCODE, string COMPCODE, string CLIENTCODE,
            string VATNUMBER, string NICPP, string TITLE, string FIRSTNAME, string LASTNAME, string EMAIL,
            string CONTACTNO, string COUNTRY, string STS, string ENUSER, string LOCDATE)
        {
            Boolean isupdate = false;

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    using (SqlCommand SQLCommand = new SqlCommand("sp_glcreateclient", con))
                    {
                        SQLCommand.CommandType = CommandType.StoredProcedure;
                        SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@CLIENTREFCODE", CLIENTREFCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@CLIENTCODE", CLIENTCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@VATNUMBER", VATNUMBER.Trim());
                        SQLCommand.Parameters.AddWithValue("@NICPP", NICPP.Trim());
                        SQLCommand.Parameters.AddWithValue("@TITLE", TITLE.Trim());
                        SQLCommand.Parameters.AddWithValue("@FIRSTNAME", FIRSTNAME.Trim());
                        SQLCommand.Parameters.AddWithValue("@LASTNAME", LASTNAME.Trim());
                        SQLCommand.Parameters.AddWithValue("@EMAIL", EMAIL.Trim());
                        SQLCommand.Parameters.AddWithValue("@CONTACTNO", CONTACTNO.Trim());
                        SQLCommand.Parameters.AddWithValue("@COUNTRY", COUNTRY.Trim());
                        SQLCommand.Parameters.AddWithValue("@STS", STS.Trim());
                        SQLCommand.Parameters.AddWithValue("@ENUSER", ENUSER.Trim());
                        SQLCommand.Parameters.AddWithValue("@LOCDATE", LOCDATE);
                        SQLCommand.ExecuteNonQuery();
                        SQLCommand.Parameters.Clear();

                        isupdate = true;
                    }
                }
                
                return isupdate;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public Boolean CreateGuest(string COMPCODE, string LOCCODE, string GUESTREFCODE, string GUESTCODE, string NICPP, string TITLE, string FIRSTNAME, string LASTNAME, string EMAIL, string CONTACTNO, string COUNTRY, string STS, string ENUSER, string LOCDATE)
        {
            Boolean isupdate = false;

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    using (SqlCommand SQLCommand = new SqlCommand("sp_createguest", con))
                    {
                        SQLCommand.CommandType = CommandType.StoredProcedure;
                        SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@LOCCODE", LOCCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@GUESTREFCODE", GUESTREFCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@GUESTCODE", GUESTCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@NICPP", NICPP.Trim());
                        SQLCommand.Parameters.AddWithValue("@TITLE", TITLE.Trim());
                        SQLCommand.Parameters.AddWithValue("@FIRSTNAME", FIRSTNAME.Trim());
                        SQLCommand.Parameters.AddWithValue("@LASTNAME", LASTNAME.Trim());
                        SQLCommand.Parameters.AddWithValue("@EMAIL", EMAIL.Trim());
                        SQLCommand.Parameters.AddWithValue("@CONTACTNO", CONTACTNO.Trim());
                        SQLCommand.Parameters.AddWithValue("@COUNTRY", COUNTRY.Trim());
                        SQLCommand.Parameters.AddWithValue("@ENUSER", ENUSER.Trim());
                        SQLCommand.Parameters.AddWithValue("@STS", STS.Trim());
                        SQLCommand.Parameters.AddWithValue("@LOCDATE", LOCDATE.Trim());
                        SQLCommand.ExecuteNonQuery();
                        SQLCommand.ExecuteNonQuery();
                        SQLCommand.Parameters.Clear();

                        isupdate = true;
                    }
                }

                return isupdate;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public Boolean createMembershipGroup(string compcode, string loccode, string membershipcode, string groupname, string discount, string amountforpoints, string pointsforamount, string pointstoamount, string amounttopoints, string sts, string enuser, string locdate)
        {
            Boolean isupdate = false;

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    using (SqlCommand SQLCommand = new SqlCommand("sp_rmcreatemembershipgroup", con))
                    {
                        SQLCommand.CommandType = CommandType.StoredProcedure;
                        SQLCommand.Parameters.AddWithValue("@COMPCODE", compcode.Trim());
                        SQLCommand.Parameters.AddWithValue("@LOCCODE", loccode.Trim());
                        SQLCommand.Parameters.AddWithValue("@MEMBERSHIPCODE", membershipcode.Trim());
                        SQLCommand.Parameters.AddWithValue("@GROUPNAME", groupname.Trim());
                        SQLCommand.Parameters.AddWithValue("@DISCOUNT", discount.Trim());
                        SQLCommand.Parameters.AddWithValue("@AMOUNTFORPOINTS", amountforpoints.Trim());
                        SQLCommand.Parameters.AddWithValue("@POINTSFORAMOUNT", pointsforamount.Trim());
                        SQLCommand.Parameters.AddWithValue("@POINTSTOAMOUNT", pointstoamount.Trim());
                        SQLCommand.Parameters.AddWithValue("@AMOUNTTOPOINTS", amounttopoints.Trim());
                        SQLCommand.Parameters.AddWithValue("@STS", sts.Trim());
                        SQLCommand.Parameters.AddWithValue("@ENUSER", enuser.Trim());
                        SQLCommand.Parameters.AddWithValue("@LOCDATE", locdate.Trim());
                        SQLCommand.ExecuteNonQuery();
                        SQLCommand.Parameters.Clear();

                        isupdate = true;
                    }
                }

                return isupdate;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataTable getMemberships(string COMPCODE, string STS)
        {

            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString  = @" SELECT MEM.MEMBERSHIPCODE, GROUPNAME, CONCAT(AMOUNTFORPOINTS, ' PTS FOR ', POINTSFORAMOUNT) AS POINTSFORAMOUNT, CONCAT(POINTSTOAMOUNT, ' FOR ', AMOUNTTOPOINTS, ' PTS') AS AMOUNTFORPOINTS " +
                    " FROM RMMEMBERSHIPS MEM INNER JOIN RMMEMBERSHIP_DETAILS DET " +
                    " ON MEM.MEMBERSHIPCODE = DET.MEMBERSHIPCODE " +
                    " WHERE MEM.COMPCODE = @COMPCODE AND MEM.STS = @STS AND DET.STS = @STS";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE);
                            SQLCommand.Parameters.AddWithValue("@STS", STS);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }

                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataTable getSelectedMembership(string COMPCODE, string MEMBERSHIPCODE, string STS)
        {

            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString  = @" SELECT MEM.MEMBERSHIPCODE, GROUPNAME, DISCOUNT,AMOUNTFORPOINTS, POINTSFORAMOUNT, POINTSTOAMOUNT, AMOUNTTOPOINTS " +
                    " FROM RMMEMBERSHIPS MEM INNER JOIN RMMEMBERSHIP_DETAILS DET " +
                    " ON MEM.MEMBERSHIPCODE = DET.MEMBERSHIPCODE " +
                    " WHERE MEM.COMPCODE = @COMPCODE AND MEM.MEMBERSHIPCODE=@MEMBERSHIPCODE AND MEM.STS = @STS AND DET.STS = @STS";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE);
                            SQLCommand.Parameters.AddWithValue("@MEMBERSHIPCODE", MEMBERSHIPCODE);
                            SQLCommand.Parameters.AddWithValue("@STS", STS);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }
                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public Boolean createGiftCertificate(string compcode, string loccode, string certificatecode, string referencecode, string giftname, string amount, string expirydate, string sts, string enuser, string locdate)
        {
            Boolean isupdate = false;

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    using (SqlCommand SQLCommand = new SqlCommand("sp_rmcreategiftcertificate", con))
                    {
                        SQLCommand.CommandType = CommandType.StoredProcedure;
                        SQLCommand.Parameters.AddWithValue("@COMPCODE", compcode.Trim());
                        SQLCommand.Parameters.AddWithValue("@LOCCODE", loccode.Trim());
                        SQLCommand.Parameters.AddWithValue("@CERTIFICATECODE", certificatecode.Trim());
                        SQLCommand.Parameters.AddWithValue("@REFERENCECODE", referencecode.Trim());
                        SQLCommand.Parameters.AddWithValue("@GIFTNAME", giftname.Trim());
                        SQLCommand.Parameters.AddWithValue("@AMOUNT", amount.Trim());
                        SQLCommand.Parameters.AddWithValue("@EXPIRYDATE", expirydate.Trim());
                        SQLCommand.Parameters.AddWithValue("@STS", sts.Trim());
                        SQLCommand.Parameters.AddWithValue("@ENUSER", enuser.Trim());
                        SQLCommand.Parameters.AddWithValue("@LOCDATE", locdate.Trim());
                        SQLCommand.ExecuteNonQuery();
                        SQLCommand.Parameters.Clear();

                        isupdate = true;
                    }
                }

                return isupdate;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataTable getSelectedGiftCertificate(string COMPCODE, string GIFTCERTIFICATECODE, string STS)
        {
            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString  = @" SELECT GC.CERTIFICATECODE, GC.REFERENCECODE, GC.GIFTNAME, GD.AMOUNT, GD.EXPIRYDATE " +
                    " FROM RMGIFTCERTIFICATES GC INNER JOIN RMGIFTCERTIFICATE_DETAILS GD " +
                    " ON GC.CERTIFICATECODE = GD.CERTIFICATECODE " +
                    " WHERE GC.COMPCODE = @COMPCODE AND GC.STS = @STS  AND GD.STS = @STS AND GC.CERTIFICATECODE= @GIFTCERTIFICATECODE";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE);
                            SQLCommand.Parameters.AddWithValue("@GIFTCERTIFICATECODE", GIFTCERTIFICATECODE);
                            SQLCommand.Parameters.AddWithValue("@STS", STS);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }
                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataTable getGiftCertificates(string COMPCODE, string STS)
        {
            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString  = @" SELECT GC.CERTIFICATECODE, GC.REFERENCECODE, GC.GIFTNAME, GD.AMOUNT, GD.EXPIRYDATE " +
                    " FROM RMGIFTCERTIFICATES GC INNER JOIN RMGIFTCERTIFICATE_DETAILS GD " +
                    " ON GC.CERTIFICATECODE = GD.CERTIFICATECODE " +
                    " WHERE GC.COMPCODE = @COMPCODE AND GC.STS = @COMPCODE AND GD.STS = @COMPCODE ";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE);
                            SQLCommand.Parameters.AddWithValue("@STS", STS);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }
                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }
    }
}