﻿using BusinessLogicDataHandler.Clog;
using ConnectionDataHandler;
using System;
using System.Data;
using System.Data.SqlClient;

namespace BusinessLogicDataHandler.Sales
{
    public class SalesDataHandler
    {
        public DataTable getMenuCategory(string COMPCODE, string LOCCODE, string RESTCODE, string STS)
        {

            DataTable SQLDT = new DataTable();

            try
            {
                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @" SELECT COMPCODE,MENUCATCODE,MENUCATNAME,COLORCODE,STS FROM RMMENU_CATEGORY WHERE COMPCODE= @COMPCODE AND LOCCODE = @LOCCODE AND RESTCODE = @RESTCODE AND  STS= @STS order by MENUCATCODE";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE);
                            SQLCommand.Parameters.AddWithValue("@LOCCODE", LOCCODE);
                            SQLCommand.Parameters.AddWithValue("@STS", STS);
                            SQLCommand.Parameters.AddWithValue("@RESTCODE", RESTCODE);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }

                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataTable getResOrderTypes(string STS)
        {

            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @" SELECT ORDERTYPECODE AS TABLECODE, ORDERTYPE AS TABLENAME FROM RMORDERTYPES WHERE  STS= @STS";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@STS", STS);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }
                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataTable getMenu(string COMPCODE, string LOCCODE, string RESTCODE, string MENUCATCODE, string STS)
        {

            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @" SELECT RM.MENUCATCODE AS MENUCATCODE,MENUCODE,MENUNAME,RATE,COLORCODE,IMAGEPATH,RM.STS AS STS,RM.ISTAXAPPLY AS ISTAXAPPLY FROM RMMENUS RM " +
                                       " INNER JOIN RMMENU_CATEGORY RC ON RM.MENUCATCODE = RC.MENUCATCODE WHERE RM.COMPCODE= @COMPCODE AND RM.LOCCODE = @LOCCODE AND RM.RESTCODE = @RESTCODE AND RC.STS = @STS AND  RM.STS= @STS AND RM.MENUCATCODE= @MENUCATCODE order by MENUCODE";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE);
                            SQLCommand.Parameters.AddWithValue("@LOCCODE", LOCCODE);
                            SQLCommand.Parameters.AddWithValue("@STS", STS);
                            SQLCommand.Parameters.AddWithValue("@RESTCODE", RESTCODE);
                            SQLCommand.Parameters.AddWithValue("@MENUCATCODE", MENUCATCODE);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }

                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataTable getMenubyOrder(string ORDERCODE, string STS)
        {

            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @" SELECT DET.MENUCATCODE, MNU.ISTAXAPPLY, DET.MENUCODE, MNU.MENUNAME, QTY,DET.RATE, CAST(AMOUNT as float) AMOUNT" +
                                " FROM RMORDER_DETAILS DET INNER JOIN RMMENU_CATEGORY CAT ON DET.MENUCATCODE = CAT.MENUCATCODE " +
                                " INNER JOIN RMMENUS MNU ON MNU.MENUCODE = DET.MENUCODE " +
                                " INNER JOIN RMORDERS ORD ON ORD.ORDERCODE=DET.ORDERCODE " +
                                " INNER JOIN RMTABLES TBL ON TBL.TABLECODE=ORD.TABLECODE " +
                                " WHERE DET.ORDERCODE = @ORDERCODE AND DET.STS != @STS " +
                                " ORDER BY DET.IDNO DESC ";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@ORDERCODE", ORDERCODE);
                            SQLCommand.Parameters.AddWithValue("@STS", STS);

                            sqlda.Fill(SQLDT);
                        }
                    }
                }

                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataTable getItemByBarcode(string COMPCODE, string LOCCODE, string RESTCODE, string MENUCODE, string STS)
        {

            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @" SELECT RM.MENUCATCODE AS MENUCATCODE,MENUCODE,MENUNAME,RATE,RM.ISTAXAPPLY AS ISTAXAPPLY FROM RMMENUS RM " +
                                   " INNER JOIN RMMENU_CATEGORY RC ON RM.MENUCATCODE = RC.MENUCATCODE WHERE RM.COMPCODE= @COMPCODE " +
                                   "AND RM.LOCCODE = @LOCCODE AND RM.RESTCODE = @RESTCODE AND RC.STS = @STS " +
                                   "AND  RM.STS= @STS AND RM.BARCODE = @MENUCODE order by MENUCODE";

                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE);
                            SQLCommand.Parameters.AddWithValue("@LOCCODE", LOCCODE);
                            SQLCommand.Parameters.AddWithValue("@STS", STS);
                            SQLCommand.Parameters.AddWithValue("@MENUCODE", MENUCODE);

                            sqlda.Fill(SQLDT);
                        }
                    }
                }

                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataTable getRmTables(string COMPCODE, string LOCCODE, string RESTCODE, string STS)
        {

            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @" SELECT COMPCODE,TABLECODE,TABLENAME,NOOFSEATS,STS,COLORCODE FROM RMTABLES WHERE COMPCODE= @COMPCODE AND LOCCODE = @LOCCODE AND RESTCODE = @RESTCODE AND  STS= @STS order by TABLECODE";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE);
                            SQLCommand.Parameters.AddWithValue("@LOCCODE", LOCCODE);
                            SQLCommand.Parameters.AddWithValue("@STS", STS);
                            SQLCommand.Parameters.AddWithValue("@RESTCODE", RESTCODE);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }

                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public string createOrder(DataTable dtOrders, DataTable dttaxbrkdn, string COMPCODE, string LOCCODE, string RESTCODE, string ORDERCODE, string PAYMENTCODE, string GUESTREFCODE, string TABLECODE, string ORDERENDATE, string LOCDATE, string ORDERSTS, string ORDERDETSTS, string PAYMENTSTS
            , string ENUSER, string BILLAMOUNT, string PAIDAMOUNT, string DUEAMOUNT, string REMARK, DataTable kotmenudt, string DISCOUNT, string ROOMNOCODE)
        {
            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    using (SqlCommand SQLCommand = new SqlCommand("sp_rmcreateorders", con))
                    {
                        SQLCommand.CommandType = CommandType.StoredProcedure;
                        SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@LOCCODE", LOCCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@RESTCODE", RESTCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@ORDERCODE", ORDERCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@PAYMENTCODE", PAYMENTCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@GUESTREFCODE", GUESTREFCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@TABLECODE", TABLECODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@ORDERENDATE", ORDERENDATE.Trim());
                        SQLCommand.Parameters.AddWithValue("@BILLAMOUNT", BILLAMOUNT.Trim());
                        SQLCommand.Parameters.AddWithValue("@DISCOUNT", DISCOUNT.Trim());
                        SQLCommand.Parameters.AddWithValue("@PAIDAMOUNT", PAIDAMOUNT.Trim());
                        SQLCommand.Parameters.AddWithValue("@DUEAMOUNT", DUEAMOUNT.Trim());
                        SQLCommand.Parameters.AddWithValue("@REMARK", REMARK.Trim());
                        SQLCommand.Parameters.AddWithValue("@param", SqlDbType.Structured).Value = dtOrders;
                        SQLCommand.Parameters.AddWithValue("@taxparam", SqlDbType.Structured).Value = dttaxbrkdn;
                        SQLCommand.Parameters.AddWithValue("@kotparam", SqlDbType.Structured).Value = kotmenudt;
                        SQLCommand.Parameters.AddWithValue("@ORDERSTS", ORDERSTS.Trim());
                        SQLCommand.Parameters.AddWithValue("@ORDERDETSTS", ORDERDETSTS.Trim());
                        SQLCommand.Parameters.AddWithValue("@PAYMENTSTS", PAYMENTSTS.Trim());
                        SQLCommand.Parameters.AddWithValue("@ENUSER", ENUSER.Trim());
                        SQLCommand.Parameters.AddWithValue("@LOCDATE", LOCDATE.Trim());
                        SQLCommand.Parameters.AddWithValue("@ROOMNOCODE", ROOMNOCODE.Trim());
                        SQLCommand.Parameters.Add("@RESORDERCODE", SqlDbType.VarChar, 20);
                        SQLCommand.Parameters["@RESORDERCODE"].Direction = ParameterDirection.Output;
                        SQLCommand.ExecuteNonQuery();

                        ORDERCODE = (string)SQLCommand.Parameters["@RESORDERCODE"].Value;
                        SQLCommand.Parameters.Clear();

                    }
                }

                return ORDERCODE;


            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataTable getpaymentoptions(string COMPCODE, string LOCCODE, string STS)
        {
            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @" SELECT CONCAT(RPO.PAYMENTCODE,'|',PAYMENTSTATUS) as PAYMENSTSTCODE ,PAYMENTMODE,ISNULL(ICONFILE,'') as ICONFILE from RESERVATION_PAYMENT_OPTIONS RPO " +
                                   " INNER JOIN MAP_LOCATION_PAYMENTOPTIONS MLP ON  RPO.PAYMENTCODE = MLP.PAYMENTCODE " +
                                   " WHERE MLP.COMPCODE = @COMPCODE AND MLP.LOCCODE = @LOCCODE AND RPO.STS= @STS AND  MLP.STS =  @STS order by MLP.PAYMENTCODE ";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE);
                            SQLCommand.Parameters.AddWithValue("@LOCCODE", LOCCODE);
                            SQLCommand.Parameters.AddWithValue("@STS", STS);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }

                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataTable getRoomNoCode(string COMPCODE, string LOCCODE, string LOCDATE)
        {

            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @"SELECT RD.ROOMNOCODE, LR.ROOMNO FROM RESERVATION_DETAILS RD LEFT JOIN LOCATION_ROOMS LR ON LR.ROOMNOCODE = RD.ROOMNOCODE WHERE RD.COMPCODE = @COMPCODE AND RD.LOCCODE = @LOCCODE' AND @LOCDATE BETWEEN RD.CHECKINDATE AND RD.CHECKOUTDATE ";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE);
                            SQLCommand.Parameters.AddWithValue("@LOCCODE", LOCCODE);
                            SQLCommand.Parameters.AddWithValue("@LOCDATE", LOCDATE);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }

                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public DataTable getPendingOrders(string COMPCODE, string LOCCODE, string RESTCODE, string STS)
        {

            DataTable SQLDT = new DataTable();

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    string SQLString = @"SELECT RMD.ORDERCODE,RMT.TABLENAME ,RMT.TABLECODE FROM RMORDERS RMD LEFT JOIN RMTABLES RMT on RMD.TABLECODE =RMT.TABLECODE WHERE RMD.STS = @STS AND RMD.COMPCODE = @COMPCODE AND RMD.RESTCODE = @RESTCODE AND RMD.LOCCODE = @LOCCODE";
                    using (SqlCommand SQLCommand = new SqlCommand(SQLString, con))
                    {
                        using (SqlDataAdapter sqlda = new SqlDataAdapter(SQLCommand))
                        {
                            SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE);
                            SQLCommand.Parameters.AddWithValue("@LOCCODE", LOCCODE);
                            SQLCommand.Parameters.AddWithValue("@RESTCODE", RESTCODE);
                            SQLCommand.Parameters.AddWithValue("@STS", STS);
                            sqlda.Fill(SQLDT);
                        }
                    }
                }
                return SQLDT;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }

        public Boolean updateOrderDetails(string COMPCODE, string LOCCODE, string RESTCODE, string ORDERCODE, string STS)
        {
            Boolean isupdate = false;

            try
            {

                using (SqlConnection con = new SqlConnection(DataAccess.GetConnectionString()))
                {
                    if (con.State != ConnectionState.Open) { con.Open(); }
                    using (SqlCommand SQLCommand = new SqlCommand("sp_rmupdateorders", con))
                    {
                        SQLCommand.CommandType = CommandType.StoredProcedure;
                        SQLCommand.Parameters.AddWithValue("@COMPCODE", COMPCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@LOCCODE", LOCCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@RESTCODE", RESTCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@ORDERCODE", ORDERCODE.Trim());
                        SQLCommand.Parameters.AddWithValue("@STS", STS.Trim());
                        SQLCommand.Parameters.AddWithValue("@param", SqlDbType.Structured).Value = "";
                        SQLCommand.ExecuteNonQuery();
                        SQLCommand.Parameters.Clear();

                        isupdate = true;
                    }
                }

                return isupdate;
            }
            catch (Exception ex)
            {
                CeegralogDataHandler.LogError(ex);
                throw ex;
            }
        }
    }
}