﻿using System;

namespace ConstantsDataHandler
{
    public class Constants
    {
        public const String CON_OWNER_NAME = "Secvision Private Limited. ";
        public const String CON_OWNER_SHORTNAME = "Secvision. ";
        public const String CON_SYSTEM_NAME = "Restaurent Management System. ";
        public const String CON_DEF_USER_PASSWORD = "0";
        public const String CON_DEF_USER_ADMIN = "ADMIN";
        public const String CON_STATUS_READY = "R";

        public static String SYS_SESSION_COMPCODE = "";
        public static String SYS_SESSION_LOCCODE = "";
        public static String SYS_SESSION_USERNAME = "";
        public static String SYS_SESSION_USERGROUP = "";
        public static String SYS_SESSION_COMPANYNAME = "";
        public static String SYS_SESSION_RESTCODE = "";
        public static String SYS_SESSION_ADDRESS = "";

        public const String CON_STATUS_YES = "Y";
        public const String CON_STATUS_NO = "N";

        public const String CON_ACTIVE_USER = "A";

        public const String CON_STATUS_ACTIVE = "A";
        public const String CON_STATUS_PENDING = "P";
        public const String CON_STATUS_SUBMITTED = "S";
        public const String CON_STATUS_REJECTED = "R";
        public const String CON_STATUS_DELETED = "D";
        public const String CON_STATUS_APPROVED = "A";

        public static String MESSAGE_WARNING = "3";
        public static String MESSAGE_INFO = "2";
        public static String MESSAGE_SUCCESS = "1";
        public static String MESSAGE_ERROR = "0";

        public static String CON_MESSAGE_STRING = "";

        public static String CON_NULL_TEXT = "";
        public static String CON_NULL_VALUE = "";

        public const Int32 CON_KILOBYTES = 1024;
        public const Int32 CON_MEGABYTES = 1048576;
        public const Int32 CON_MAX_FILE_SIZE = 209715200;

        public static String CON_TAX_RM = "RM";
        public static String CON_TAX_GROSS = "G";
        public static String CON_TAX_NETT = "N";
        public static String CON_TAX_GROSS_TYPE = "Gross";
        public static String CON_TT_TAX_AMOUNT = "Tax Amount";
        public static String CON_TAX_NETT_TYPE = "Nett";
        public static String CON_TAX_NETT_RW = "Nett Payable";
        public static String CON_STATUS_TXNOTAPPLIED = "N";
        public static String CON_STATUS_TXAPPLIED = "Y";

        public static String CON_CUS_TYP_CLIENT = "CLIENT";
        public static String CON_CUS_TYP_GUEST = "GUEST";

        public static String REPORT_CATEGORY_SALES = "SL";
        public static String REPORT_CATEGORY_ITEMS = "IT";
        public const String CON_ERRORLOG_PATH = "~/Resources/Errorlog/";

        public const String REPORT_SALES = "RPSL00001";
        public const String REPORT_MENUITEMS = "RPIT00003";
        public const String REPORT_MENUCATEGORIES = "RPIT00004";
        public const String CON_PASSWORD_SALT = "j@zon|u|k!!ied|me&mysch00l";
    }
}