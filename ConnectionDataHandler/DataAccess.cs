﻿using System.Configuration;
using System.Data.SqlClient;

namespace ConnectionDataHandler
{
        public sealed class DataAccess
        {

            public static string GetConnectionString()
            {
                string DBcon = ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString;
                return DBcon;
            }

        }
}